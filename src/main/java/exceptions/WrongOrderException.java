package exceptions;

public class WrongOrderException extends Exception {
    public WrongOrderException (String message) {
        super(message);
    }
}
