package exceptions;

public class WrongDeptVacationException extends Exception{
    public WrongDeptVacationException(String s) {
        super(s);
    }
}
