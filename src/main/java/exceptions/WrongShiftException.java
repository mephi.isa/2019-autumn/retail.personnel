package exceptions;

public class WrongShiftException extends Exception {
    public WrongShiftException(String s) {
        super(s);
    }
}
