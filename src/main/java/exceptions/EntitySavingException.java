package exceptions;

public class EntitySavingException extends Exception {
    public EntitySavingException(String message) {
        super(message);
    }
}
