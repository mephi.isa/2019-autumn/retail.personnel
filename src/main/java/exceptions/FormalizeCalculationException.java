package exceptions;

public class FormalizeCalculationException extends Exception {
    public FormalizeCalculationException(String message) {
        super(message);
    }
}
