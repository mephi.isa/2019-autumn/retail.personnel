package models;

import java.time.LocalDate;


public class Shift {

    private LocalDate date;
    private Integer limitOfShift;
    private Integer actualAmountEmployee;

    public Shift(LocalDate date, Integer limitOfShift) {
        this.date = date;
        this.limitOfShift = limitOfShift;
        this.actualAmountEmployee = 0;
    }

    public Shift(LocalDate date, Integer limitOfShift, Integer actualAmountEmployee) {
        this.date = date;
        this.limitOfShift = limitOfShift;
        this.actualAmountEmployee = actualAmountEmployee;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getActualAmountEmployee() {
        return actualAmountEmployee;
    }

    public Integer getLimitOfShift() {
        return limitOfShift;
    }

    public void addAmountEmployee() throws Exception {
        if (this.actualAmountEmployee < this.limitOfShift) {
            this.actualAmountEmployee++;
        } else {
            throw new Exception("Amount can't be under limit");
        }
    }

    public boolean isFull() {
        return this.actualAmountEmployee == this.limitOfShift;
    }
// private void reduceAmountEmployee() throws Exception {
   //     if (this.actualAmountEmployee > 0) {
   //         this.actualAmountEmployee--;
   //     } else {
   //         throw new Exception("Amount can't be negatve");
   //     }
   // }

}
