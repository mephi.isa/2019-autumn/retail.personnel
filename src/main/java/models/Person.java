package models;

import exceptions.WrongDateException;
import exceptions.WrongOrderException;
import service.mock.WorkCalendarService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

public class Person {

    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthday;
    private Integer idPerson;
    private Integer title;
    private String INN;
    private ShiftType shiftType;
    private LocalDate rentDate;
    private LocalDate fireDate;
    private List<Order> orders;
    private List<Shift> shifts;

    public Person(String firstName, String lastName, String middleName,
                  LocalDate birthday,
                  String INN, Integer title, LocalDate rentDate, Order order, ShiftType shiftType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthday;
        this.title = title;
        boolean isValidINN = Pattern.matches("\\d{12}",INN);
        if (isValidINN) {
            this.INN = INN;
        } else {
            throw  new IllegalArgumentException("wrong INN");
        }
        this.rentDate = rentDate;
        this.orders = new ArrayList<Order>();
        this.orders.add(order);
        this.shiftType = shiftType;
        this.shifts = new ArrayList<Shift>();
    }

    public Person(String firstName, String lastName, String middleName,
                  LocalDate birthday, Integer idPerson,
                  String INN, Integer title, LocalDate rentDate, Order order, ShiftType shiftType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthday;
        this.title = title;
        this.idPerson = idPerson;
        boolean isValidINN = Pattern.matches("\\d{12}",INN);
        if (isValidINN) {
            this.INN = INN;
        } else {
            throw  new IllegalArgumentException("wrong INN");
        }
        this.rentDate = rentDate;
        this.orders = new ArrayList<Order>();
        this.orders.add(order);
        this.shiftType = shiftType;
        this.shifts = new ArrayList<Shift>();
    }

   // List<Pair<ClassProperty, String>> propertyDataList

    public Person(String firstName, String lastName, String middleName,
                  LocalDate birthday, Integer idPerson,
                  String INN, Integer title, LocalDate rentDate, List<Order> orders, ShiftType shiftType,
                  LocalDate fireDate, List<Shift> shifts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthday;
        this.title = title;
        this.idPerson = idPerson;
        boolean isValidINN = Pattern.matches("\\d{12}",INN);
        if (isValidINN) {
            this.INN = INN;
        } else {
            throw  new IllegalArgumentException("wrong INN");
        }
        this.rentDate = rentDate;
        this.orders = orders;
        this.shiftType = shiftType;
        this.shifts = shifts;
        this.fireDate = fireDate;
    }



    public void firePerson (Order order, LocalDate fireDate) throws WrongOrderException, WrongDateException {
        if (order.getType() != Type.FIRE) {
            throw new WrongOrderException("Order must be a fire order");
        } else {
            this.orders.add(order);
            if (this.getRentDate().isBefore(fireDate)) {
                this.fireDate = fireDate;
            } else {
                throw new WrongDateException("Fire date must be after rent");
            }
        }
    }

    public void addOrder(Order order) {
        if (order != null)
            this.orders.add(order);
    }

    public boolean addShift (Shift shift) {
        if (this.shiftType == ShiftType.NORMAL) {
            try {
                    shift.addAmountEmployee();
                    this.shifts.add(shift);
                    return true;
                } catch (Exception e) {
                    return false;
                }

        } else {
            if (this.shifts.size() == 0) {
                try {
                    shift.addAmountEmployee();
                    this.shifts.add(shift);
                    return true;
                } catch (Exception e) {
                    return false;
                }
            } else {
                if (this.listDatesShifts().size() == 1 &&
                        this.listDatesShifts().contains(shift.getDate().minusDays(1))) {
                    try {
                        shift.addAmountEmployee();
                        this.shifts.add(shift);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } else {
                    if (this.listDatesShifts().size() == 2 &&
                            this.listDatesShifts().contains(shift.getDate().minusDays(1)) &&
                            this.listDatesShifts().contains(shift.getDate().minusDays(2))) {
                        try {
                            shift.addAmountEmployee();
                            this.shifts.add(shift);
                            return true;
                        } catch (Exception e) {
                            return false;
                        }
                    } else {
                        if (!this.listDatesShifts().contains(shift.getDate().minusDays(1)) &&
                                !this.listDatesShifts().contains(shift.getDate().minusDays(2)) &&
                                !this.listDatesShifts().contains(shift.getDate().minusDays(3))) {
                            try {
                                shift.addAmountEmployee();
                                this.shifts.add(shift);
                                return true;
                            } catch (Exception e) {
                                return false;
                            }
                        } else {
                            if (!this.listDatesShifts().contains(shift.getDate().minusDays(1)) &&
                                    !this.listDatesShifts().contains(shift.getDate().minusDays(2)) &&
                                    !this.listDatesShifts().contains(shift.getDate().minusDays(3))) {
                                try {
                                    shift.addAmountEmployee();
                                    this.shifts.add(shift);
                                    return true;
                                } catch (Exception e) {
                                    return false;
                                }
                            } else {
                                if (this.listDatesShifts().contains(shift.getDate().minusDays(1)) &&
                                        this.listDatesShifts().contains(shift.getDate().minusDays(2)) &&
                                        !this.listDatesShifts().contains(shift.getDate().minusDays(3))) {
                                    try {
                                        shift.addAmountEmployee();
                                        this.shifts.add(shift);
                                        return true;
                                    } catch (Exception e) {
                                        return false;
                                    }
                                } else {
                                    if (this.listDatesShifts().contains(shift.getDate().minusDays(1)) &&
                                            !this.listDatesShifts().contains(shift.getDate().minusDays(2)) &&
                                            !this.listDatesShifts().contains(shift.getDate().minusDays(3))) {
                                        try {
                                            shift.addAmountEmployee();
                                            this.shifts.add(shift);
                                            return true;
                                        } catch (Exception e) {
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private List<LocalDate> listDatesShifts () {
        List<LocalDate> res = new ArrayList<LocalDate>();
        for (int i=0; i < this.shifts.size(); i++) {
            res.add(this.shifts.get(i).getDate());
        }
        return res;
    }

    public String getFirstName() {
        return firstName;
    }

    public List<Shift> getShifts() {
        return shifts;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Integer getIdPerson() {
        return idPerson;
    }

    public Integer getTitle() {
        return title;
    }

    public String getINN() {
        return INN;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public LocalDate getFireDate() {
        return fireDate;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void changeTitle(Integer title, Order order) throws WrongOrderException {
        if (order.getType() != Type.TRANSFER) {
            throw new WrongOrderException("Order must be a transfer order");
        } else {
            this.orders.add(order);
            this.title = title;
        }
    }

    public void setIdPerson(Integer idPerson) {
        if (this.idPerson == null) {
            this.idPerson = idPerson;
        } else {
            throw new IllegalArgumentException("you can't change idPerson");
        }
    }

    public void setShiftType(ShiftType shiftType, Order order) {
        if (!this.shiftType.equals(shiftType)) {
            this.shiftType = shiftType;
            this.shifts.clear();
            this.orders.add(order);
        }
    }

    public ShiftType getShiftType() {
        return shiftType;
    }
}
