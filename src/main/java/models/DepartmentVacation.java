package models;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DepartmentVacation {

    private Integer departament;
    private List<Vacation> vacationList;
    private Integer year;
    private boolean approved;
    private Integer idHREmpoloyee;

    public DepartmentVacation(Integer departament, List<Vacation> vacationList, Integer year) {
        this.departament = departament;
        this.year = year;
        this.vacationList = new ArrayList<Vacation>();
        for (int i=0; i<vacationList.size(); i++ ) {
            if (vacationList.get(i).getStartDay().getYear()==this.year &&
                    vacationList.get(i).getEndDay().getYear()==this.year) {
                this.vacationList.add(vacationList.get(i));
            } else {
                throw new IllegalArgumentException("Vacation must be in another year");
            }
        }
        this.approved = false;
    }

    public DepartmentVacation(Integer departament, List<Vacation> vacationList, Integer year, boolean approved, Integer idHREmpoloyee) {
        this.departament = departament;
        this.vacationList = vacationList;
        this.year = year;
        this.approved = approved;
        this.idHREmpoloyee = idHREmpoloyee;
    }

    public void approve(Integer idHR) {
        this.approved=true;
        this.idHREmpoloyee=idHR;
    }

    public boolean checkVacationDays(Integer idPerson) {
        boolean result = false;
        Long amount = 0L;
        for (int i = 0; i < this.vacationList.size(); i++) {
            if (this.vacationList.get(i).getEmployee().getIdPerson() == idPerson) {
                amount += this.vacationList.get(i).getAmountDays();
                if (this.vacationList.get(i).getAmountDays() >= 14) {
                    result = true;
                }
            }
        }
        if (amount > 56) {
            result = false;
        }
        return result;
    }

    public Long getAmountDay (Integer idPerson) {
        Long res =0L;
        for (int i=0; i<this.vacationList.size(); i++) {
            if (this.vacationList.get(i).getEmployee().getIdPerson() == idPerson) {
                res += this.vacationList.get(i).getAmountDays();
            }
        }
        return  res;
    }

    public boolean checkDepartmentVacationDays(List<Integer> idPersons) {
        boolean result = true;
        for (int i = 0; i < idPersons.size(); i++) {
            result = result && this.checkVacationDays(idPersons.get(i));
        }
        return result;
    }

    public List<Vacation> getAllPersonVacation (Person person) {
        List<Vacation> res = new ArrayList<>();
        for (int i=0; i<this.vacationList.size();i++) {
            if (this.vacationList.get(i).getEmployee() == person) {
                res.add(this.vacationList.get(i));
            }
        }
        if (res.size() == 0) {
            return null;
        }
        return res;
    }

    public List<Person> getAllPerson () {
        List<Person> res = new ArrayList<>();
        for (int i =0; i<this.vacationList.size(); i++) {
            if (!res.contains(this.vacationList.get(i).getEmployee())) {
                res.add(this.vacationList.get(i).getEmployee());
            }
        }
        return res;
    }

    public Integer getDepartament() {
        return departament;
    }

    public List<Vacation> getVacationList() {
        return vacationList;
    }

    public Integer getYear() {
        return year;
    }

    public boolean isApproved() {
        return approved;
    }

    public Integer getIdHREmpoloyee() {
        return idHREmpoloyee;
    }
}
