package models;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

import static java.time.temporal.ChronoUnit.DAYS;


public class Vacation {

    private LocalDate startDay;
    private LocalDate endDay;
    private Person employee;

    public Vacation(LocalDate startDay, LocalDate endDay, Person person) {
        Period period = Period.between(startDay,endDay);
        Integer n = (period.getDays()) * (period.getMonths()|1) * (period.getYears()|1);
        if (n <= 56) {
            this.startDay = startDay;
            this.endDay = endDay;
        } else {
            throw new IllegalArgumentException("Vacation time in two year must be less than 56");
        }
        this.employee = person;
    }

    public Long getAmountDays() {
      //  Period period = Period.between(this.startDay,this.endDay);
      //  Duration duration = Duration.between(this.startDay,this.endDay);
        return DAYS.between(this.startDay,this.endDay)+1;
        //Math.abs(duration.toDays());
                //(period.getDays()) * (period.getMonths()|1) * (period.getYears()|1);
    }

    public LocalDate getStartDay() {
        return startDay;
    }

    public LocalDate getEndDay() {
        return endDay;
    }

    public Person getEmployee() {
        return employee;
    }
}
