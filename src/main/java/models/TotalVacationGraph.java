package models;

import java.time.LocalDate;
import java.util.List;

public class TotalVacationGraph {
    private Integer year;
    private List<DepartmentVacation> vacations;

    public TotalVacationGraph(Integer year, List<DepartmentVacation> vacations) {
        this.year = year;
        for (int i=0; i<vacations.size(); i++) {
            if (!vacations.get(i).isApproved()) {
                throw new IllegalArgumentException("you can't create total vacation greph if departmnet vacation graph is not approved");
            }
        }
        this.vacations = vacations;
    }


    public Integer getYear() {
        return year;
    }

    public List<DepartmentVacation> getVacations() {
        return vacations;
    }
}
