package models;

import java.time.LocalDate;
import java.util.IllegalFormatException;

public class Order {

    private Integer id;
    private Type type;
    private LocalDate date;

    public Order(Integer id, LocalDate date) {
        this.id = id;
        this.date = date;
    }

    public Order(Integer id, Type type, LocalDate date) {
        this.id = id;
        this.type = type;
        this.date = date;
    }

    public Order( LocalDate date) {
        this.date = date;
    }

    public static Order createHireOrder(Integer id, LocalDate date) {
        Order ord = new Order(id,date);
        ord.type=Type.RENT;
        return ord;
    }

    public static Order createHireOrder(LocalDate date) {
        Order ord = new Order(date);
        ord.type=Type.RENT;
        return ord;
    }

    public static Order createFireOrder(Integer id, LocalDate date) {
        Order ord = new Order(id,date);
        ord.type=Type.FIRE;
        return ord;
    }

    public static Order createTransferOrder(Integer id, LocalDate date) {
        Order ord = new Order(id,date);
        ord.type=Type.TRANSFER;
        return ord;
    }

    public static Order createFireOrder(LocalDate date) {
        Order ord = new Order(date);
        ord.type=Type.FIRE;
        return ord;
    }

    public static Order createTransferOrder(LocalDate date) {
        Order ord = new Order(date);
        ord.type=Type.TRANSFER;
        return ord;
    }

    public Integer getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public LocalDate getDate() {
        return date;
    }
}

