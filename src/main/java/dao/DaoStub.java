package dao;

import models.*;
import service.classes.Role;

import java.time.LocalDate;
import java.util.*;

public class DaoStub implements Dao {

    class User {
        String login;
        String password;
        Role role;
    }

    class Session {
        User user;
        String uuid;
        Date date;
    }

    private Map<String, User> userMap = new HashMap<>();
    private Map<String, Session> sessionMap = new HashMap<>();
    private Map<String, Person> personMap = new HashMap<>();
    private Map<String, Person> personInnMap = new HashMap<>();
    private Map<Integer, Person> personIdMap = new HashMap<>();
    private Map<Integer, Order> orderMap = new HashMap<>();
    private Map<Person, List<Vacation>> vacationMap = new HashMap<>();
    private Map<Integer, List<DepartmentVacation>> departmentVacationMap = new HashMap<>();
    private Map<Integer, TotalVacationGraph> totalVacationGraphMap = new HashMap<>();
    private Map<Person, List<Shift>> shiftMap = new HashMap<>();


 //   private Map<String, User> objectUserRef = new HashMap<>();
 //   private Map<String, User> classUserRef = new HashMap<>();

    public DaoStub() {
        User admin = new User();
        admin.login = "admin";
        admin.password = "5f4dcc3b5aa765d61d8327deb882cf99"; //md5 from "password"
        admin.role = Role.ADMIN;

        User firstHRManager = new User();
        firstHRManager.login = "firstHRManager";
        firstHRManager.password = "5f4dcc3b5aa765d61d8327deb882cf99"; //md5 from "password"
        firstHRManager.role = Role.HR;

        User secondHRManager = new User();
        secondHRManager.login = "secondHRManager";
        secondHRManager.password = "5f4dcc3b5aa765d61d8327deb882cf99"; //md5 from "password"
        secondHRManager.role = Role.HR;

        User chief = new User();
        chief.login = "chief";
        chief.password = "5f4dcc3b5aa765d61d8327deb882cf99"; //md5 from "password"
        chief.role = Role.CHIEF;

        User user = new User();
        user.login = "user";
        user.password = "5f4dcc3b5aa765d61d8327deb882cf99"; //md5 from "password"
        user.role = Role.USER;

        this.userMap.put(admin.login, admin);
        this.userMap.put(firstHRManager.login, firstHRManager);
        this.userMap.put(secondHRManager.login, secondHRManager);
        this.userMap.put(chief.login,chief);
        this.userMap.put(user.login,user);
    }



    @Override
    public List<Person> getPersonList() {
        return new ArrayList<>(this.personMap.values());
    }

    @Override
    public List<Person> getSubordinatePersonList(List<Integer> titles) {
        return null;
    }

    @Override
    public Person getPersonById(Integer idPerson) {
        return personIdMap.get(idPerson);
    }

    @Override
    public void addPerson(Person person) {
        String fio = person.getLastName();
        fio = fio.concat(person.getFirstName());
        if (person.getMiddleName() != null) {
            fio = fio.concat(person.getMiddleName());
        }
        this.addOrder(person.getOrders().get(0));
        person.setIdPerson(this.personMap.size());
        this.personIdMap.put(person.getIdPerson(),person);
        this.personMap.put(fio, person);
        this.personInnMap.put(person.getINN(),person);
    }

    @Override
    public void updatePerson(Person person, Order order) {
        String fio = person.getLastName();
        fio = fio.concat(person.getFirstName());
        if (person.getMiddleName() != null) {
            fio = fio.concat(person.getMiddleName());
        }
        if (order != null) {
            this.addOrder(order);
        }
       // person.addOrder(order);
        this.personMap.replace(fio, person);
        this.personIdMap.replace(personIdMap.size()+1,person);
        this.personInnMap.replace(person.getINN(),person);
    }

    @Override
    public void updatePerson(Person person) {
        String fio = person.getLastName();
        fio = fio.concat(person.getFirstName());
        if (person.getMiddleName() != null) {
            fio = fio.concat(person.getMiddleName());
        }
        this.personMap.replace(fio, person);
        this.personInnMap.replace(person.getINN(),person);
        this.personIdMap.replace(personIdMap.size()+1,person);
    }

    @Override
    public Person getPersonByINN (String INN) {
        return this.personInnMap.get(INN);
    }

    @Override
    public List<Order> getOrderList(Person person) {
        return this.personInnMap.get(person.getINN()).getOrders();
    }


    private void addOrder(Order order) {
        this.orderMap.put(order.getId(),order);
    }

    @Override
    public List<Vacation> getAllVacationOfPerson(Person person) {
        return this.vacationMap.get(person);
    }

    @Override
    public List<Vacation> getAllDepartmentVacationByYear(Integer year, Integer idDepartment) {
        List<DepartmentVacation> tmp = this.departmentVacationMap.get(idDepartment);
        //List<Vacation> res = new ArrayList<>();
        for (int i=0; i < tmp.size(); i++){
            if (tmp.get(i).getYear() == year) {
                return tmp.get(i).getVacationList();
            }
        }
        return null;
    }

    @Override
    public void addVacation(Vacation vacation) {

    }



 /*   @Override
    public void addVacation(Vacation vacation, DepartmentVacation departmentVacation) {
        List<DepartmentVacation> tmpDep = new ArrayList<>();
        List<Vacation> tmpVac = new ArrayList<>();
        if (this.vacationMap.get(vacation.getEmployee()) == null) {
            if (this.departmentVacationMap.get(departmentVacation.getDepartament()) == null) {
                tmpDep.add(departmentVacation);
                this.departmentVacationMap.put(departmentVacation.getDepartament(), tmpDep);
                tmpVac.add(vacation);
                this.vacationMap.put(vacation.getEmployee(), tmpVac);
            } else {
                tmpDep = this.departmentVacationMap.get(departmentVacation.getDepartament());
                tmpDep.add(departmentVacation);
                this.departmentVacationMap.replace(departmentVacation.getDepartament(), tmpDep);
                tmpVac.add(vacation);
                this.vacationMap.put(vacation.getEmployee(), tmpVac);
            }
        } else {
            if (this.departmentVacationMap.get(departmentVacation.getDepartament()) == null) {
                tmpDep.add(departmentVacation);
                this.departmentVacationMap.put(departmentVacation.getDepartament(), tmpDep);
                tmpVac = this.vacationMap.get(vacation.getEmployee());
                tmpVac.add(vacation);
                this.vacationMap.replace(vacation.getEmployee(), tmpVac);
            } else {
                tmpDep = this.departmentVacationMap.get(departmentVacation.getDepartament());
                tmpDep.add(departmentVacation);
                this.departmentVacationMap.replace(departmentVacation.getDepartament(), tmpDep);
                tmpVac = this.vacationMap.get(vacation.getEmployee());
                tmpVac.add(vacation);
                this.vacationMap.replace(vacation.getEmployee(), tmpVac);
            }
        }
    }

  */

    @Override
    public void addDepartmentVacation(DepartmentVacation departmentVacation) {
        List<DepartmentVacation> tmpDep = new ArrayList<>();
        List<Vacation> tmpVac = new ArrayList<>();
        if (this.departmentVacationMap.get(departmentVacation.getDepartament()) == null) {
            tmpDep.add(departmentVacation);
            this.departmentVacationMap.put(departmentVacation.getDepartament(), tmpDep);
            for (int i=0; i < departmentVacation.getVacationList().size(); i++) {
                if (this.vacationMap.get(departmentVacation.getVacationList().get(i).getEmployee()) == null) {
                    tmpVac.clear();
                    tmpVac.add(departmentVacation.getVacationList().get(i));
                    this.vacationMap.put(departmentVacation.getVacationList().get(i).getEmployee(),
                            tmpVac);
                } else {
                    tmpVac = this.vacationMap.get(departmentVacation.getVacationList().get(i).getEmployee());
                    tmpVac.add(departmentVacation.getVacationList().get(i));
                    this.vacationMap.replace(departmentVacation.getVacationList().get(i).getEmployee(),tmpVac);
                }
            }
        } else {
            tmpDep = this.departmentVacationMap.get(departmentVacation.getDepartament());
            tmpDep.add(departmentVacation);
            this.departmentVacationMap.replace(departmentVacation.getDepartament(), tmpDep);
        }
    }

    @Override
    public void updateDepartmentVacation(DepartmentVacation departmentVacation) {
        List<DepartmentVacation> tmp = this.departmentVacationMap.get(departmentVacation.getDepartament());
        List<DepartmentVacation> res = new ArrayList<>();
        for (int i =0; i<tmp.size();i++) {
            if (tmp.get(i).getYear().equals(departmentVacation.getYear())){
                res.add(departmentVacation);
            } else {
                res.add(tmp.get(i));
            }
        }
        this.departmentVacationMap.replace(departmentVacation.getDepartament(),res);
    }

    @Override
    public DepartmentVacation getDepartmentVacationByYearId(Integer idDepartment, Integer year) {
        List<DepartmentVacation> tmp = this.departmentVacationMap.get(idDepartment);
       // DepartmentVacation res = null;
        for (int i=0; i<tmp.size();i++) {
            if (tmp.get(i).getYear().equals(year)) {
                return tmp.get(i);
            }
        }
        return null;
    }

    @Override
    public List<DepartmentVacation> getDepartmentVacationsByYear(Integer year) {
        List<DepartmentVacation> res = new ArrayList<>();
        Set<Integer> dept = this.departmentVacationMap.keySet();
        int n = dept.size();
        List<Integer> tmp = new ArrayList<Integer>(n);
        tmp.addAll(dept);
        for (int i=0; i<tmp.size(); i++) {
            List<DepartmentVacation> elem = this.departmentVacationMap.get(tmp.get(i));
            for (int j=0; j<elem.size(); j++) {
                if (elem.get(j).getYear().equals(year)) {
                    res.add(elem.get(j));
                }
            }
        }
        return res;
    }

    @Override
    public List<TotalVacationGraph> getListTotalVacationGraph() {
        return new ArrayList<>(this.totalVacationGraphMap.values());
    }

    @Override
    public void addTotalVacationGraph(TotalVacationGraph totalVacationGraph) {
        this.totalVacationGraphMap.put(totalVacationGraph.getYear(),totalVacationGraph);
    }

    @Override
    public TotalVacationGraph getGraphByYear(Integer year) {
        return this.totalVacationGraphMap.get(year);
    }

    @Override
    public List<Shift> getPersonShift(Person person) {
        return this.shiftMap.get(person);
    }

    @Override
    public void addShift(Shift shift, Person person) {
        List<Shift> shiftList = new ArrayList<>();
        if (this.shiftMap.get(person) != null) {
            shiftList = this.shiftMap.get(person);
            shiftList.add(shift);
            this.shiftMap.replace(person,shiftList);
        } else {
            shiftList.add(shift);
            this.shiftMap.put(person,shiftList);
        }
    }

    @Override
    public void updateShift(Shift shift, Person person) {
        List<Shift> shiftList = new ArrayList<>();
        if (this.shiftMap.get(person) != null) {
            shiftList = this.shiftMap.get(person);
            shiftList.add(shift);
            this.shiftMap.replace(person,shiftList);
        } else {
            throw new NullPointerException("You cannot modify not exist shift");
        }
    }



}
