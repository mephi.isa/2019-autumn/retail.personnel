package dao;


import exceptions.EntitySavingException;
import models.*;
import service.classes.Role;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface Dao {

    List<Person> getPersonList();
    List<Person> getSubordinatePersonList(List<Integer> titles);

    Person getPersonById (Integer idPerson);
    Person getPersonByINN (String INN);

    void addPerson(Person person);
    void updatePerson(Person person, Order order);
    void updatePerson(Person person);

    List<Order> getOrderList(Person person);
   // void addOrder(Order order);

    List<Vacation> getAllVacationOfPerson(Person person);
    List<Vacation> getAllDepartmentVacationByYear(Integer year, Integer idDepartment);
    void addVacation(Vacation vacation) throws EntitySavingException;
    void addDepartmentVacation (DepartmentVacation departmentVacation) throws EntitySavingException;
    void updateDepartmentVacation (DepartmentVacation departmentVacation);
    DepartmentVacation getDepartmentVacationByYearId (Integer idDepartment, Integer year);
    List<DepartmentVacation> getDepartmentVacationsByYear (Integer year);

    List<TotalVacationGraph> getListTotalVacationGraph ();
    void addTotalVacationGraph(TotalVacationGraph totalVacationGraph) throws EntitySavingException;
    TotalVacationGraph getGraphByYear (Integer year);

    List<Shift> getPersonShift (Person person);
    void addShift (Shift shift, Person person) throws EntitySavingException;
    void updateShift (Shift shift, Person person) throws EntitySavingException;


//    String getClassOwnerLogin(Class aClass);
//    String getClassOwnerLogin(String className);

  //  List<ClassProperty> getClassPropertyList();
}
