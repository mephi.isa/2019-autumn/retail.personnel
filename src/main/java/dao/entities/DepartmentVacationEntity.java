package dao.entities;

import models.Vacation;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "department_vacation", schema = "rp")
public class DepartmentVacationEntity {

    @Id
    @Column(name = "department_vacation_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDeptVacation;

    @Column(name = "department", nullable = false)
    private Integer departament;

    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "approved", nullable = false)
    private boolean approved;

    @Column(name = "id_hr")
    private Integer idHREmpoloyee;

    @OneToMany(mappedBy="idVacationDept", cascade=CascadeType.ALL, orphanRemoval = true)
    Set<VacationEntity> vacations = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="total_vacation_graph")
    private TotalVacationGraphEntity vacationGraph;

    public DepartmentVacationEntity() {
        this.approved = false;
    }

    public DepartmentVacationEntity(Integer departament, Integer year) {
        this.departament = departament;
        this.year = year;
        this.approved = false;
    }

    public DepartmentVacationEntity(Integer departament, Integer year, boolean approved, Integer idHREmpoloyee, TotalVacationGraphEntity vacationGraph) {
        this.departament = departament;
        this.year = year;
        this.approved = approved;
        this.idHREmpoloyee = idHREmpoloyee;
        this.vacationGraph = vacationGraph;
    }


    public Integer getIdDeptVacation() {
        return idDeptVacation;
    }

    public void setIdDeptVacation(Integer idDeptVacation) {
        this.idDeptVacation = idDeptVacation;
    }

    public Integer getDepartament() {
        return departament;
    }

    public void setDepartament(Integer departament) {
        this.departament = departament;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {

        this.year = year;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public Integer getIdHREmpoloyee() {
        return idHREmpoloyee;
    }

    public void setIdHREmpoloyee(Integer idHREmpoloyee) {
        this.idHREmpoloyee = idHREmpoloyee;
    }

    public Set<VacationEntity> getVacations() {
        return vacations;
    }

    public void setVacations(Set<VacationEntity> vacations) {
        this.vacations = vacations;
    }

    public void addVacation (VacationEntity vacation) {
        this.vacations.add(vacation);
        vacation.setIdVacationDept(this);
    }

    public TotalVacationGraphEntity getVacationGraph() {
        return vacationGraph;
    }

    public void setVacationGraph(TotalVacationGraphEntity vacationGraph) {
        this.vacationGraph = vacationGraph;
    }
}
