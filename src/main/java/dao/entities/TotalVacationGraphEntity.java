package dao.entities;

import models.DepartmentVacation;
import models.Vacation;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "total_vacation_graph", schema = "rp")
public class TotalVacationGraphEntity {

    @Id
    @Column(name = "id_vacation_graph")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idGraph;

    @Column(name = "year", unique = true, nullable = false)
    private Integer year;

    @OneToMany(mappedBy="vacationGraph", cascade=CascadeType.ALL, orphanRemoval = true)
    Set<DepartmentVacationEntity> vacations = new HashSet<>();

    public TotalVacationGraphEntity() {
    }

    public TotalVacationGraphEntity(Integer year) {
        this.year = year;
    }

    public Integer getIdGraph() {
        return idGraph;
    }

    public void setIdGraph(Integer idGraph) {
        this.idGraph = idGraph;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Set<DepartmentVacationEntity> getVacations() {
        return vacations;
    }

    public void setVacations(Set<DepartmentVacationEntity> vacations) {
        this.vacations = vacations;
    }

    public void addVacations(DepartmentVacationEntity vacation) {
        this.vacations.add(vacation);
        vacation.setVacationGraph(this);
    }
}
