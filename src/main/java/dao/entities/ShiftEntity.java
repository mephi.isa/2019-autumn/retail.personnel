package dao.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table (name = "shift", schema = "rp")
public class ShiftEntity {

    @Id
    @Column(name = "shift_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idShift;

  //  @OneToMany (mappedBy="aClass", cascade=CascadeType.ALL, orphanRemoval = true)


    @ManyToMany(cascade=CascadeType.REMOVE)
    @JoinTable(name = "employee_shift",
            //foreign key for ShiftEntity in employee_shift table
            joinColumns = @JoinColumn(name = "shift_id"),
            //foreign key for other side - PersonEntity in employee_shift table
            inverseJoinColumns = @JoinColumn(name = "person_id"), schema = "rp")
    Set<PersonEntity> employeeSet = new HashSet<>();

  //  @ManyToOne
  //  @JoinColumn(name="person_id", nullable=false)
  //  private PersonEntity employee;

    @Column
    private LocalDate date;

    @Column(name = "limitOfShift")
    private Integer limitOfShift;

    @Column(name = "actualAmountEmployee")
    private Integer actualAmountEmployee;

    public ShiftEntity() {
    }

    public ShiftEntity(LocalDate date, Integer limitOfShift) {
        this.date = date;
        this.limitOfShift = limitOfShift;
        this.actualAmountEmployee = 0;
    }

    public ShiftEntity( LocalDate date, Integer limitOfShift, Integer actualAmountEmployee) {
        this.date = date;
        this.limitOfShift = limitOfShift;
        this.actualAmountEmployee = actualAmountEmployee;
    }

    public Integer getIdShift() {
        return idShift;
    }

    public void setIdShift(Integer idShift) {
        this.idShift = idShift;
    }

    public Set<PersonEntity> getEmployeeSet() {
        return employeeSet;
    }

    public void setEmployeeSet(Set<PersonEntity> employeeSet) {
        this.employeeSet = employeeSet;
    }

    public void addEmployee (PersonEntity person) {
        this.employeeSet.add(person);
       // this.actualAmountEmployee += 1;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getLimitOfShift() {
        return limitOfShift;
    }

    public void setLimitOfShift(Integer limitOfShift) {
        this.limitOfShift = limitOfShift;
    }

    public Integer getActualAmountEmployee() {
        return actualAmountEmployee;
    }

    public void setActualAmountEmployee(Integer actualAmountEmployee) {
        this.actualAmountEmployee = actualAmountEmployee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShiftEntity)) return false;
        ShiftEntity that = (ShiftEntity) o;
        return Objects.equals(idShift, that.idShift) &&
                Objects.equals(limitOfShift, that.limitOfShift);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idShift, limitOfShift);
    }

    @Override
    public String toString() {
        return "ShiftEntity{" +
                "idShift=" + idShift +
                ", employeeSet=" + employeeSet +
                ", date=" + date +
                ", limitOfShift=" + limitOfShift +
                ", actualAmountEmployee=" + actualAmountEmployee +
                '}';
    }
}
