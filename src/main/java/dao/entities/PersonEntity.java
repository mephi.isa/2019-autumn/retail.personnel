package dao.entities;

import models.Order;
import models.Shift;
import models.ShiftType;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "person", schema = "rp")
public class PersonEntity {

    @Id
    @Column(name = "person_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPerson;

    @OneToMany (mappedBy="employee", cascade=CascadeType.ALL, orphanRemoval = true)
    Set<OrderEntity> orderSet = new HashSet<>();

 //   @OneToMany (mappedBy="aClass", cascade=CascadeType.ALL, orphanRemoval = true)


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "employee_shift",
            //foreign key for PersonEntity in employee_shift table
            joinColumns = @JoinColumn(name = "person_id"),
            //foreign key for other side - ShiftEntity in employee_shift table
            inverseJoinColumns = @JoinColumn(name = "shift_id"), schema = "rp")
    Set<ShiftEntity> shifts = new HashSet<>();

    @OneToMany (mappedBy="idPerson", cascade=CascadeType.ALL, orphanRemoval = true)
    Set<VacationEntity> vacations = new HashSet<>();

    @Column(name = "shift_type")
    private ShiftType shiftType;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "title")
    private Integer title;

    @Column(name = "INN", unique = true, nullable = false)
    private String INN;

    @Column(name = "rent_date")
    private LocalDate rentDate;

    @Column(name = "fire_date")
    private LocalDate fireDate;

    public PersonEntity() {}

    public PersonEntity(OrderEntity order, ShiftType shiftType, String firstName,
                        String lastName, String middleName, LocalDate birthday, Integer title, String INN,
                        LocalDate rentDate) {
        this.orderSet = new HashSet<OrderEntity>();
        this.orderSet.add(order);
        order.setEmployee(this);
        this.shiftType = shiftType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthday;
        this.title = title;
        this.INN = INN;
        this.rentDate = rentDate;
        //this.shiftSet = new HashSet<ShiftEntity>();
    }

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public Set<OrderEntity> getOrderSet() {
        return orderSet;
    }

    public void setOrderSet(Set<OrderEntity> orderSet) {
        this.orderSet = orderSet;
    }

    public void addOrder(OrderEntity order) {
        this.orderSet.add(order);
        order.setEmployee(this);
    }

    public Set<VacationEntity> getVacations() {
        return vacations;
    }

    public void setVacations(Set<VacationEntity> vacations) {
        this.vacations = vacations;
    }

    public void addVacation(VacationEntity vacation) {
        this.vacations.add(vacation);
        vacation.setIdPerson(this);
    }
    public Set<ShiftEntity> getShifts() {
        return shifts;
    }

    public void setShifts(Set<ShiftEntity> shifts) {
        this.shifts = shifts;
    }

    public void addShift (ShiftEntity shift) {
        this.shifts.add(shift);
       // shift.addEmployee(this);
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Integer getTitle() {
        return title;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }


    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public LocalDate getFireDate() {
        return fireDate;
    }

    public void setFireDate(LocalDate fireDate) {
        this.fireDate = fireDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonEntity that = (PersonEntity) o;
        return Objects.equals(INN, that.INN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(INN);
    }

    @Override
    public String toString() {
        return "PersonEntity{" +
                "idPerson=" + idPerson +
                ", orderSet=" + orderSet +
                ", shifts=" + shifts +
                ", shiftType=" + shiftType +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthday=" + birthday +
                ", title=" + title +
                ", INN='" + INN + '\'' +
                ", rentDate=" + rentDate +
                ", fireDate=" + fireDate +
                '}';
    }
}
