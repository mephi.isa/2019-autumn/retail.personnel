package dao.entities;

import models.ShiftType;
import models.Type;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "order", schema = "rp")
public class OrderEntity {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="person_id", nullable=false)
    private PersonEntity employee;

    @Column(name = "type")
    private Type type;

    @Column(name = "date")
    private LocalDate date;

    public OrderEntity() {
    }

    public OrderEntity(Type type, LocalDate date) {
        this.type = type;
        this.date = date;
    }

    public OrderEntity(PersonEntity employee, Type type, LocalDate date) {
        this.employee = employee;
        this.type = type;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonEntity getEmployee() {
        return employee;
    }

    public void setEmployee(PersonEntity employee) {
        this.employee = employee;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderEntity)) return false;
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(employee, that.employee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employee);
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id=" + id +
                ", employee=" + employee +
                ", type=" + type +
                ", date=" + date +
                '}';
    }
}
