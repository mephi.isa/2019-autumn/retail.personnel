package dao.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "vacation", schema = "rp")
public class VacationEntity {

    @Id
    @Column(name = "vacation_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVacation;

    @Column(name = "startDay", nullable = false)
    private LocalDate startDay;

    @Column(name = "endDay", nullable = false)
    private LocalDate endDay;

    @ManyToOne
    @JoinColumn(name="person_id", nullable=false)
    private PersonEntity idPerson;

    @ManyToOne
    @JoinColumn(name="vacation_dept")
    private DepartmentVacationEntity idVacationDept;

    public VacationEntity() {
    }

    public VacationEntity(LocalDate startDay, LocalDate endDay, PersonEntity idPerson) {
        this.startDay = startDay;
        this.endDay = endDay;
        this.idPerson = idPerson;
    }

    public VacationEntity(PersonEntity idPerson, LocalDate startDay, LocalDate endDay,  DepartmentVacationEntity idVacationDept) {
        this.startDay = startDay;
        this.endDay = endDay;
        this.idPerson = idPerson;
        this.idVacationDept = idVacationDept;
    }



    public Integer getIdVacation() {
        return idVacation;
    }

    public DepartmentVacationEntity getIdVacationDept() {
        return idVacationDept;
    }

    public void setIdVacationDept(DepartmentVacationEntity idVacationDept) {
        this.idVacationDept = idVacationDept;
    }

    public void setIdVacation(Integer idVacation) {
        this.idVacation = idVacation;
    }

    public LocalDate getStartDay() {
        return startDay;
    }

    public void setStartDay(LocalDate startDay) {
        this.startDay = startDay;
    }

    public LocalDate getEndDay() {
        return endDay;
    }

    public void setEndDay(LocalDate endDay) {
        this.endDay = endDay;
    }

    public PersonEntity getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(PersonEntity idPerson) {
        this.idPerson = idPerson;
    }
}
