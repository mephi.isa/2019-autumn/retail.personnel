package dao;

import dao.entities.*;
import exceptions.EntitySavingException;
import models.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import service.classes.Role;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DaoImpl implements Dao {


    public DaoImpl() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from UserEntity where login=:login");
        query.setParameter("login", "admin");
        List userList = query.list();

        if(userList.size() > 0) {
            Query personQuery = session.createQuery("from PersonEntity where INN=:inn");
            personQuery.setParameter("inn", "-");
            List personList = personQuery.list();
            Query graphQuery = session.createQuery("from TotalVacationGraphEntity");
           // graphQuery.setParameter("year", "-");
            List graphList = graphQuery.list();

            if(personList.size() == 0 && graphList.size() ==0) {
                PersonEntity personEntity = new PersonEntity();
                personEntity.setFirstName("-");
                personEntity.setLastName("-");
                personEntity.setINN("-");
                TotalVacationGraphEntity totalVacationGraphEntity = new TotalVacationGraphEntity(1970);
                Transaction tx1 = session.beginTransaction();
            //    session.save(personEntity);
            //    session.save(totalVacationGraphEntity);
                tx1.commit();
                session.close();
            }
        }

}

    @Override
    public List<Person> getPersonList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

        //Маппим в бизнес-объекты
        List<Person> personList = new ArrayList<>();
        for(PersonEntity personEntity: personEntityList) {
            personList.add(new Person(personEntity.getFirstName(), personEntity.getLastName(), personEntity.getMiddleName(),
                    personEntity.getBirthday(),personEntity.getIdPerson(), personEntity.getINN(), personEntity.getTitle(),
                    personEntity.getRentDate(), new ArrayList<>(personEntity.getOrderSet().stream().map(ord ->
                            new Order(ord.getId(),ord.getType(),ord.getDate())).collect(Collectors.toList())),
                    personEntity.getShiftType(), personEntity.getFireDate(),
                    new ArrayList<>(personEntity.getShifts().stream().map(shift ->
                            new Shift(shift.getDate(),shift.getLimitOfShift(),shift.getActualAmountEmployee())).collect(Collectors.toList()))
                    ));
        }
        session.close();

        return personList;
    }

    @Override
    public List<Person> getSubordinatePersonList(List<Integer> titles) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from PersonEntity where title in :titles");
        query.setParameter("titles", titles);
        List<PersonEntity> personEntityList = (List<PersonEntity>) query.list();

        //Маппим в бизнес-объекты
        List<Person> personList = new ArrayList<>();
        for(PersonEntity personEntity: personEntityList) {
            personList.add(new Person(personEntity.getFirstName(), personEntity.getLastName(), personEntity.getMiddleName(),
                    personEntity.getBirthday(),personEntity.getIdPerson(), personEntity.getINN(), personEntity.getTitle(),
                    personEntity.getRentDate(), new ArrayList<>(personEntity.getOrderSet().stream().map(ord ->
                    new Order(ord.getId(),ord.getType(),ord.getDate())).collect(Collectors.toList())),
                    personEntity.getShiftType(), personEntity.getFireDate(),
                    new ArrayList<>(personEntity.getShifts().stream().map(shift ->
                            new Shift(shift.getDate(),shift.getLimitOfShift(),shift.getActualAmountEmployee())).collect(Collectors.toList()))
            ));
        }
        session.close();

        return personList;
    }

    @Override
    public Person getPersonById(Integer idPerson) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
        query.setParameter("idPerson",idPerson);
        List personEntityList = query.list();
        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

        if(personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            Person person = new Person(personEntity.getFirstName(), personEntity.getLastName(), personEntity.getMiddleName(),
                    personEntity.getBirthday(),personEntity.getIdPerson(), personEntity.getINN(), personEntity.getTitle(),
                    personEntity.getRentDate(), new ArrayList<>(personEntity.getOrderSet().stream().map(ord ->
                    new Order(ord.getId(),ord.getType(),ord.getDate())).collect(Collectors.toList())),
                    personEntity.getShiftType(), personEntity.getFireDate(),
                    new ArrayList<>(personEntity.getShifts().stream().map(shift ->
                            new Shift(shift.getDate(),shift.getLimitOfShift(),shift.getActualAmountEmployee())).collect(Collectors.toList())));
            session.close();
            return person;
        } else {
            session.close();
            return null;
        }
    }

    @Override
    public Person getPersonByINN(String INN) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from PersonEntity where INN=:INN");
        query.setParameter("INN", INN);
        List personEntityList = query.list();
        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

        if(personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            Person person = new Person(personEntity.getFirstName(), personEntity.getLastName(), personEntity.getMiddleName(),
                    personEntity.getBirthday(),personEntity.getIdPerson(), personEntity.getINN(), personEntity.getTitle(),
                    personEntity.getRentDate(), new ArrayList<>(personEntity.getOrderSet().stream().map(ord ->
                    new Order(ord.getId(),ord.getType(),ord.getDate())).collect(Collectors.toList())),
                    personEntity.getShiftType(), personEntity.getFireDate(),
                    new ArrayList<>(personEntity.getShifts().stream().map(shift ->
                            new Shift(shift.getDate(),shift.getLimitOfShift(),shift.getActualAmountEmployee())).collect(Collectors.toList())));
            session.close();
            return person;
        } else {
            session.close();
            return null;
        }
    }

    @Override
    public void addPerson(Person person) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        OrderEntity orderEntity = new OrderEntity(person.getOrders().get(0).getType(), person.getOrders().get(0).getDate());
        PersonEntity personEntity = new PersonEntity(orderEntity, person.getShiftType(), person.getFirstName(), person.getLastName(), person.getMiddleName(), person.getBirthday(), person.getTitle(), person.getINN(), person.getRentDate());

        Transaction tx1 = session.beginTransaction();
        session.save(personEntity);
        tx1.commit();

        session.close();
    }

    @Override
    public void updatePerson(Person person, Order order) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query personQuery = session.createQuery("from PersonEntity where idPerson=:idPerson");
        personQuery.setParameter("idPerson", person.getIdPerson());
        List personList = personQuery.list();

        List<ShiftEntity> res = null;
        if(personList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personList.get(0);

            personEntity.addOrder(new OrderEntity(order.getType(), order.getDate()));
            if (order.getType() == Type.FIRE) {
                personEntity.setFireDate(person.getFireDate());
            }
            if (order.getType() == Type.TRANSFER) {
                if (!person.getTitle().equals(personEntity.getTitle())) {
                    personEntity.setTitle(person.getTitle());
                } else {
                    if (person.getShiftType() != personEntity.getShiftType()) {
                        personEntity.setShiftType(person.getShiftType());
                        List<ShiftEntity> shiftEntityList = new ArrayList<>(personEntity.getShifts().stream().map(shift ->
                                shift).collect(Collectors.toList()));
                        res = shiftEntityList;
                        for (ShiftEntity shiftEntity: shiftEntityList) {
                            personEntity.getShifts().remove(shiftEntity);
                            //shiftEntityList.remove(shiftEntity);
                        }

                    }
                }
            }

            Transaction tx1 = session.beginTransaction();
            session.update(personEntity);
            tx1.commit();
        }

        session.close();

    }

    @Override
    public void updatePerson(Person person) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query personQuery = session.createQuery("from PersonEntity where idPerson=:idPerson");
        personQuery.setParameter("idPerson", person.getIdPerson());
        List personList = personQuery.list();

        if(personList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personList.get(0);

            personEntity.setFirstName(person.getFirstName());
            personEntity.setLastName(person.getLastName());
            personEntity.setMiddleName(person.getMiddleName());

            Transaction tx1 = session.beginTransaction();
            session.update(personEntity);
            tx1.commit();
        }

        session.close();
    }

    @Override
    public List<Order> getOrderList(Person person) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
        query.setParameter("idPerson",person.getIdPerson());
        List personEntityList = query.list();
        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

        if(personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);

            List<Order> orders = new ArrayList<>();
            for(OrderEntity orderEntity: personEntity.getOrderSet()) {
                orders.add(new Order(orderEntity.getId(),orderEntity.getType(),orderEntity.getDate()));
            }
            session.close();
            return orders;
        } else {
            session.close();
            return null;
        }
    }

    @Override
    public List<Vacation> getAllVacationOfPerson(Person person) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query pquery = session.createQuery("from PersonEntity where idPerson=:idPerson");
        pquery.setParameter("idPerson", person.getIdPerson());
        List personEntityList = pquery.list();
        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();
        List<Vacation> vacationList = new ArrayList<>();

        if(personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            Query query = session.createQuery("from VacationEntity where idPerson=:idPerson");
            query.setParameter("idPerson", personEntity);
            List<VacationEntity> vacationEntityList = (List<VacationEntity>) query.list();

            // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

            for (VacationEntity vacationEntity : vacationEntityList) {
                vacationList.add(new Vacation(vacationEntity.getStartDay(), vacationEntity.getEndDay(), person));
            }
        } else {
            session.close();
            return null;
        }
        session.close();

        return vacationList;
    }

    @Override
    public List<Vacation> getAllDepartmentVacationByYear(Integer year, Integer idDepartment) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from DepartmentVacationEntity where year=:year");
        query.setParameter("year",year);
        List<DepartmentVacationEntity> departmentVacationEntityList = (List<DepartmentVacationEntity>) query.list();

        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();
       // List<DepartmentVacation> departmentVacationList = new ArrayList<>();
        List<Vacation> vacationList = new ArrayList<>();
        for(DepartmentVacationEntity departmentVacationEntity: departmentVacationEntityList) {
            if (departmentVacationEntity.getDepartament() == idDepartment) {
                vacationList = new ArrayList<>(departmentVacationEntity.getVacations().stream().map(vac ->
                                new Vacation(vac.getStartDay(), vac.getEndDay(),
                                        this.getPersonById(vac.getIdPerson().getIdPerson()))).collect(Collectors.toList()));
                break;
            }
        }

        session.close();

        return vacationList;
    }

    @Override
    public void addVacation(Vacation vacation) throws EntitySavingException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
        query.setParameter("idPerson", vacation.getEmployee().getIdPerson());
        List personEntityList = query.list();

        if (personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            VacationEntity vacationEntity = new VacationEntity(vacation.getStartDay(), vacation.getEndDay(), personEntity);
            Transaction tx1 = session.beginTransaction();
            session.save(vacationEntity);
            tx1.commit();

            session.close();
        }  else {
            session.close();
            throw new EntitySavingException("Error saving vacation");
        }
    }

    @Override
    public void addDepartmentVacation(DepartmentVacation departmentVacation) throws EntitySavingException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        DepartmentVacationEntity departmentVacationEntity = new DepartmentVacationEntity(departmentVacation.getDepartament(),
                departmentVacation.getYear());

        List<VacationEntity> vacationEntities = new ArrayList<>();
        if (departmentVacation.getVacationList() != null) {
            for (Vacation vacation : departmentVacation.getVacationList()) {
                Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
                query.setParameter("idPerson", vacation.getEmployee().getIdPerson());
                List personEntityList = query.list();

                if (personEntityList.size() > 0) {
                    PersonEntity personEntity = (PersonEntity) personEntityList.get(0);

                    Query vacQuery = session.createQuery("from VacationEntity where idPerson=:idPerson");
                   // vacQuery.setParameter("startDate", vacation.getStartDay());
                   // vacQuery.setParameter("endDate", vacation.getEndDay());
                    vacQuery.setParameter("idPerson", personEntity);

                    List<VacationEntity> vacationEntityList = (List<VacationEntity>) vacQuery.list();
                    for (VacationEntity vacationEntity:vacationEntityList) {
                        if (vacationEntity.getStartDay().equals(vacation.getStartDay()) && vacation.getEndDay().equals(vacationEntity.getEndDay())) {
                            departmentVacationEntity.addVacation(vacationEntity);
                            vacationEntities.add(vacationEntity);
                        }
                    }
                } else {
                    session.close();
                    throw new EntitySavingException("There are no person while saving dep vac");
                }
            }
        }

        departmentVacationEntity.setApproved(departmentVacation.isApproved());

        Transaction tx1 = session.beginTransaction();
        session.save(departmentVacationEntity);
        tx1.commit();

        for (VacationEntity vacationEntity:vacationEntities) {
            tx1 = session.beginTransaction();
            session.update(vacationEntity);
            tx1.commit();
        }

        session.close();
    }

    @Override
    public void updateDepartmentVacation(DepartmentVacation departmentVacation) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query departmentVacationQuery = session.createQuery("from DepartmentVacationEntity where departament=:dept and year=:year");
        departmentVacationQuery.setParameter("dept", departmentVacation.getDepartament());
        departmentVacationQuery.setParameter("year", departmentVacation.getYear());
        List departmentVacationList = departmentVacationQuery.list();

        if(departmentVacationList.size() > 0) {
            DepartmentVacationEntity departmentVacation1 = (DepartmentVacationEntity) departmentVacationList.get(0);

            departmentVacation1.setApproved(departmentVacation.isApproved());
            departmentVacation1.setIdHREmpoloyee(departmentVacation.getIdHREmpoloyee());

            Transaction tx1 = session.beginTransaction();
            session.update(departmentVacation1);
            tx1.commit();
        }

        session.close();
    }

    @Override
    public DepartmentVacation getDepartmentVacationByYearId(Integer idDepartment, Integer year) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query departmentVacationQuery = session.createQuery("from DepartmentVacationEntity where departament=:dept and year=:year");
        departmentVacationQuery.setParameter("dept", idDepartment);
        departmentVacationQuery.setParameter("year", year);
        List departmentVacationEntityList = departmentVacationQuery.list();

        if (departmentVacationEntityList.size() > 0) {
            DepartmentVacationEntity departmentVacationEntity = (DepartmentVacationEntity) departmentVacationEntityList.get(0);
            DepartmentVacation departmentVacation = new DepartmentVacation(departmentVacationEntity.getDepartament(),
                    this.getAllDepartmentVacationByYear(departmentVacationEntity.getYear(),departmentVacationEntity.getDepartament()),
                    departmentVacationEntity.getYear(),departmentVacationEntity.isApproved(),departmentVacationEntity.getIdHREmpoloyee());
            session.close();
            return departmentVacation;
        } else {
            session.close();
            return null;
        }
    }

    @Override
    public List<DepartmentVacation> getDepartmentVacationsByYear(Integer year) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from DepartmentVacationEntity where year=:year");
        query.setParameter("year",year);
        List<DepartmentVacationEntity> departmentVacationEntityList = (List<DepartmentVacationEntity>) query.list();

        // List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();
        List<DepartmentVacation> departmentVacationList = new ArrayList<>();
        for(DepartmentVacationEntity departmentVacationEntity: departmentVacationEntityList) {

            departmentVacationList.add(new DepartmentVacation(departmentVacationEntity.getDepartament(),
                    new ArrayList<>(departmentVacationEntity.getVacations().stream().map(vac->
                            new Vacation(vac.getStartDay(),vac.getEndDay(),this.getPersonById(vac.getIdPerson().getIdPerson()))).collect(Collectors.toList())),
                    departmentVacationEntity.getYear(),departmentVacationEntity.isApproved(),departmentVacationEntity.getIdHREmpoloyee()));
        }
        session.close();

        return departmentVacationList;
    }

    @Override
    public List<TotalVacationGraph> getListTotalVacationGraph() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<TotalVacationGraphEntity> totalVacationGraphEntityList = (List<TotalVacationGraphEntity>) session.createQuery("from TotalVacationGraphEntity").list();

        //Маппим в бизнес-объекты
        List<TotalVacationGraph> totalVacationGraphList = new ArrayList<>();
        for(TotalVacationGraphEntity totalVacationGraphEntity: totalVacationGraphEntityList) {
            totalVacationGraphList.add(new TotalVacationGraph(totalVacationGraphEntity.getYear(),
                    this.getDepartmentVacationsByYear(totalVacationGraphEntity.getYear())));
        }
        session.close();

        return totalVacationGraphList;
    }

    @Override
    public void addTotalVacationGraph(TotalVacationGraph totalVacationGraph) throws EntitySavingException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        TotalVacationGraphEntity totalVacationGraphEntity = new TotalVacationGraphEntity(totalVacationGraph.getYear());
     //   List<DepartmentVacation> departmentVacationList = this.getDepartmentVacationsByYear(totalVacationGraphEntity.getYear());

        Query depvacQuery = session.createQuery("from DepartmentVacationEntity where year=:year");
        depvacQuery.setParameter("year", totalVacationGraph.getYear());
        List<DepartmentVacationEntity> departmentVacationEntityList = (List<DepartmentVacationEntity>) depvacQuery.list();

        //List<DepartmentVacationEntity> departmentVacationEntityList = new ArrayList<>();
        for (DepartmentVacationEntity departmentVacationEntity:departmentVacationEntityList) {
            totalVacationGraphEntity.addVacations(departmentVacationEntity);
        }
        Transaction tx1 = session.beginTransaction();
        session.save(totalVacationGraphEntity);
        tx1.commit();
       // tx1 = session.beginTransaction();
       // session.save(totalVacationGraphEntity);
       // tx1.commit();

        session.close();

    }

    @Override
    public TotalVacationGraph getGraphByYear(Integer year) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        // List<TotalVacationGraphEntity> totalVacationGraphEntityList = (List<TotalVacationGraphEntity>) session.createQuery("from TotalVacationGraphEntity").list();

        Query totalVacationGraphQuery = session.createQuery("from TotalVacationGraphEntity where year=:year");
        totalVacationGraphQuery.setParameter("year", year);
        List totalVacationGraphEntityList = totalVacationGraphQuery.list();

        //Маппим в бизнес-объекты
        if (totalVacationGraphEntityList.size() > 0) {
            //       PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            TotalVacationGraphEntity totalVacationGraphEntity = (TotalVacationGraphEntity) totalVacationGraphEntityList.get(0);
            TotalVacationGraph totalVacationGraph = new TotalVacationGraph(totalVacationGraphEntity.getYear(),
                    this.getDepartmentVacationsByYear(totalVacationGraphEntity.getYear()));
            session.close();
            return totalVacationGraph;
        } else {
            session.close();
            return null;

        }
    }

    @Override
    public List<Shift> getPersonShift(Person person) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query shiftQuery = session.createQuery("SELECT s FROM ShiftEntity s JOIN FETCH s.employeeSet");
       // shiftQuery.setParameter("idPerson", person.getIdPerson());
        List<ShiftEntity> shiftEntityList = (List<ShiftEntity>) shiftQuery.list();
        List<Shift> shiftList = new ArrayList<>();
        for(ShiftEntity shiftEntity: shiftEntityList) {
            if (shiftEntity.getEmployeeSet().iterator().next().getIdPerson().equals(person.getIdPerson())) {
                shiftList.add(new Shift(shiftEntity.getDate(),shiftEntity.getLimitOfShift(),shiftEntity.getActualAmountEmployee()));
            }
        }
        session.close();
        return shiftList;
    }

    @Override
    public void addShift(Shift shift, Person person) throws EntitySavingException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
        query.setParameter("idPerson", person.getIdPerson());
        List personEntityList = query.list();

        ShiftEntity shiftEntity = new ShiftEntity(shift.getDate(),shift.getLimitOfShift(),shift.getActualAmountEmployee());

        if (personEntityList.size() > 0) {
            PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
            shiftEntity.addEmployee(personEntity);
        } else {
            session.close();
            throw new EntitySavingException("Error saving shift");
        }

        Transaction tx1 = session.beginTransaction();
        session.save(shiftEntity);
        tx1.commit();

        session.close();

    }

    @Override
    public void updateShift(Shift shift, Person person) throws EntitySavingException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query shiftQuery = session.createQuery("from ShiftEntity where date=:date and limitOfShift=:limitOfShift");
        shiftQuery.setParameter("date", shift.getDate());
        shiftQuery.setParameter("limitOfShift", shift.getLimitOfShift());
        List shiftEntityList = shiftQuery.list();

        if (shiftEntityList.size() > 0) {
            ShiftEntity shiftEntity = (ShiftEntity) shiftEntityList.get(0);

            Query query = session.createQuery("from PersonEntity where idPerson=:idPerson");
            query.setParameter("idPerson", person.getIdPerson());
            List personEntityList = query.list();

            if (personEntityList.size() > 0) {
                PersonEntity personEntity = (PersonEntity) personEntityList.get(0);
                shiftEntity.addEmployee(personEntity);
            } else {
                session.close();
                throw new EntitySavingException("Error updating shift there no person");
            }
            Transaction tx1 = session.beginTransaction();
            session.save(shiftEntity);
            tx1.commit();
        } else {
            session.close();
            throw new EntitySavingException("Error saving updating shift there are no shift");
        }

        session.close();

    }

}
