package dao;

import dao.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private HibernateUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                configureFactory();
            } catch (Exception e) {
                System.out.println("Исключение!  " + e);
            }
        }
        return sessionFactory;
    }

    private static void configureFactory() {
        Configuration configuration = new Configuration();

        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "org.postgresql.Driver");
        settings.put(Environment.URL, "jdbc:postgresql://localhost:5432/retail_personnel");
        settings.put(Environment.USER, "lewue");
        settings.put(Environment.PASS, "lewue");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL9Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");

        configuration.setProperties(settings);
        configuration.addAnnotatedClass(UserEntity.class);
        configuration.addAnnotatedClass(ShiftEntity.class);
        configuration.addAnnotatedClass(OrderEntity.class);
        configuration.addAnnotatedClass(PersonEntity.class);
        configuration.addAnnotatedClass(VacationEntity.class);
        configuration.addAnnotatedClass(DepartmentVacationEntity.class);
        configuration.addAnnotatedClass(TotalVacationGraphEntity.class);

        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
    }

}
