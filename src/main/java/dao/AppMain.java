package dao;

import dao.entities.*;
import exceptions.WrongDateException;
import exceptions.WrongOrderException;
import models.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import service.classes.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AppMain {

    @PersistenceUnit
    static EntityManager emf;

    public static void main(String[] args) throws WrongOrderException, WrongDateException {
        System.out.println("Hibernate tutorial start");

       /* Session session = HibernateUtil.getSessionFactory().openSession();
        List<PersonEntity> personEntityList = (List<PersonEntity>) session.createQuery("from PersonEntity").list();

        //Маппим в бизнес-объекты
        List<Person> personList = new ArrayList<>();
        for(PersonEntity personEntity: personEntityList) {
            if (personEntity.getIdPerson() == 1) {
                personEntity.setINN("123456789012");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
            if (personEntity.getIdPerson() == 2) {
                personEntity.setINN("123456789013");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
            if (personEntity.getIdPerson() == 3) {
                personEntity.setINN("123456789014");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
            if (personEntity.getIdPerson() == 4) {
                personEntity.setINN("123456789015");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
            if (personEntity.getIdPerson() == 5) {
                personEntity.setINN("123456789017");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
            if (personEntity.getIdPerson() == 6) {
                personEntity.setINN("123456789912");
                Transaction tx = session.beginTransaction();
                session.update(personEntity);
                tx.commit();
            }
        }
        session.close();


      //  DaoImpl employeeDAO = new DaoImpl();
     //   employeeDAO.addSession("admin","1111", Date.valueOf(LocalDate.now()));

     /*   String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "INNBondar";
        Integer title = 1;
        LocalDate rentDate = LocalDate.now();
        Order order1 = Order.createHireOrder(rentDate);
        ShiftType type = ShiftType.NORMAL;
        Person person = new Person( firstName, lastName, middleName, birthday, INN, title, rentDate, order1, type);

        employeeDAO.addPerson(person);

        Person person1 = employeeDAO.getPersonByINN(INN);

        System.out.println("Order type: "+ person1.getOrders().get(0).getType().toString());

        Order order2 = Order.createTransferOrder(LocalDate.now());
        person1.changeTitle(2,order2);

        employeeDAO.updatePerson(person1,order2);

        person1 = employeeDAO.getPersonByINN(INN);
        person = employeeDAO.getPersonByINN(INN);
        System.out.println("Order2 type: "+ person.getOrders().get(1).getType().toString());

        Order order3 = Order.createFireOrder(LocalDate.now());
        person1.firePerson(order3,LocalDate.now().plusDays(3));

        employeeDAO.updatePerson(person1,order3);
        person = employeeDAO.getPersonByINN(INN);
        System.out.println("Fire date: " + person.getFireDate().toString());

        person1 = employeeDAO.getPersonByINN(INN);
        person1.setFirstName("Ivan");
        employeeDAO.updatePerson(person1);

        person = employeeDAO.getPersonByINN(INN);
        System.out.println("Person: " + person.getFirstName() + " " + person.getLastName());

        Transaction tx = session.beginTransaction();


       OrderEntity order = new OrderEntity(Type.RENT,LocalDate.of(2019,4,12));

       PersonEntity employee = new PersonEntity(order,ShiftType.NORMAL,"f","n","m",LocalDate.now(),1,"11",LocalDate.now());
       // employee.addOrder(order);
      //  employee.setFirstName("FirstName");
       // employee.setLastName("LastName");
       // employee.setShiftType(ShiftType.NORMAL);
       // employee.setINN("23");

        //add first car to employee
        ShiftEntity shift = new ShiftEntity();
        shift.setDate(LocalDate.of(2019,12,17));
        shift.setLimitOfShift(20);
        employee.addShift(shift);

        //add second car to employee
        shift = new ShiftEntity();
        shift.setDate(LocalDate.of(2019,12,18));
        shift.setLimitOfShift(21);
        employee.addShift(shift);

        VacationEntity vacation = new VacationEntity(LocalDate.of(2020,3,8),
                LocalDate.of(2020,3,10),employee);

        DepartmentVacationEntity vacDept = new DepartmentVacationEntity();
        vacDept.setDepartament(2);
        vacDept.setYear(2020);

        vacDept.addVacation(vacation);

        TotalVacationGraphEntity graph = new TotalVacationGraphEntity();
        graph.setYear(2020);
        graph.addVacations(vacDept);
     //   vacation.setIdPerson(employee.getIdPerson());
        employee.addVacation(vacation);

   /*     UserEntity admin = new UserEntity();
        admin.setLogin("admin");
        admin.setPassword("5f4dcc3b5aa765d61d8327deb882cf99"); //md5 from "password"
        admin.setRole("ADMIN");

        UserEntity firstHRManager = new UserEntity();
        firstHRManager.setLogin("firstHRManager");
        firstHRManager.setPassword("5f4dcc3b5aa765d61d8327deb882cf99"); //md5 from "password"
        firstHRManager.setRole("HR");

        UserEntity secondHRManager = new UserEntity();
        secondHRManager.setLogin("secondHRManager");
        secondHRManager.setPassword("5f4dcc3b5aa765d61d8327deb882cf99"); //md5 from "password"
        secondHRManager.setRole("HR");

        UserEntity chief = new UserEntity();
        chief.setLogin("chief");
        chief.setPassword("5f4dcc3b5aa765d61d8327deb882cf99"); //md5 from "password"
        chief.setRole("CHIEF");

        UserEntity user = new UserEntity();
        user.setLogin("user");
        user.setPassword("5f4dcc3b5aa765d61d8327deb882cf99"); //md5 from "password"
        user.setRole("USER");
*/

      //  session.save(employee);
    //    session.save(graph);
  //      session.save(admin);
  //      session.save(firstHRManager);
  //      session.save(secondHRManager);
  //      session.save(chief);
  //      session.save(user);
        // session.save(vacDept)

    //    tx.commit();
     //   session.close();

   /*     Order order4 = Order.createTransferOrder(LocalDate.now());
        List<Vacation> vacationList = employeeDAO.getAllDepartmentVacationByYear(2020,2);
        System.out.println(vacationList.get(0).getStartDay().toString() + "dataaaaaaaa");
        person = employeeDAO.getPersonByINN("11");
        person.setShiftType(ShiftType.IN_SHIFTS,order4);
        employeeDAO.updatePerson(person,order4);

    */
   /*     session = HibernateUtil.getSessionFactory().openSession();

        Query shiftQuery = session.createQuery("SELECT s FROM ShiftEntity s JOIN FETCH s.employeeSet where s.employeeSer=:idPerson");
        shiftQuery.setParameter("idPerson", person.getIdPerson());
        List shiftEntityList = shiftQuery.list();

        session.close();
*/
     ///   person = employeeDAO.getPersonById(2);
    //    List<Shift> shifts = employeeDAO.getPersonShift(person);
     //   System.out.println("Shift entity : "+ shifts.get(0).getDate().toString() + " " + shifts.get(1).getDate().toString());


    }

}