package PersonnelDao;

import exceptions.InvalidCredentialsException;

import java.util.List;

public interface PersonnelDao {
    User getUser(String Login, String password) throws InvalidCredentialsException;
    String getUserFIO(User user);
    String getUserByFIO(String fio);
    List<String> getUsersIDList();
    List<String> getUserIDList();
    List<String> getAdminIDList();
    List<String> getHRIDList();
    List<String> getChiefIDList();
    public Integer getIdByLogin(User user);
}