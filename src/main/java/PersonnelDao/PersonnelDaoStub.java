package PersonnelDao;

import exceptions.InvalidCredentialsException;
import service.classes.Role;

import java.util.ArrayList;
import java.util.List;

public class PersonnelDaoStub implements PersonnelDao {
    @Override
    public User getUser(String login, String password) throws InvalidCredentialsException {
        // User user = new User();
        if (login == null || password == null) {
            return null;
        }
        if (login.equals("ADMIN") && password.equals("ADMIN")) {
            User admin = new User();
            admin.login = "admin";
            admin.role = Role.ADMIN;
            return admin;
        }
        if (login.equals("FIRSTHRMANAGER") && password.equals("FIRSTHRMANAGER")) {
            User firstHRManager = new User();
            firstHRManager.login = "firstHRManager";
            firstHRManager.role = Role.HR;
            return firstHRManager;
        }
        if (login.equals("SECONDHRMANAGER") && password.equals("SECONDHRMANAGER")) {
            User secondHRManager = new User();
            secondHRManager.login = "secondHRManager";
            secondHRManager.role = Role.HR;
            return secondHRManager;
        }
        if (login.equals("CHIEF") && password.equals("CHIEF")) {
            User chief = new User();
            chief.login = "chief";
            chief.idDept = 1;
            chief.role = Role.CHIEF;
            return chief;
        }
        if (login.equals("USER") && password.equals("USER")) {
            User user = new User();
            user.login = "user";
            user.role = Role.USER;
            return user;
        }
        return null;
    }

    @Override
    public String getUserFIO(User user) {
        if (user.login == "admin") {
            return "Трушин Алексей Владимирович";
        } else if (user.login == "firstHRManager") {
            return "Денисов Дмитрий Валерьевич";
        } else if (user.login == "secondHRManager") {
            return "Антонов Никита Геннадьевич";
        } else if (user.login == "chief") {
            return "Бельдягин Вячеслав Андреевич";
        } else if (user.login == "user") {
            return "Субботин Олег Витальевич";
        }
        return "Некто";
    }

    @Override
    public List<String> getUsersIDList(){
        List<String> stringList = new ArrayList<>();
        stringList.add("admin");
        stringList.add("firstHRManager");
        stringList.add("secondHRManager");
        stringList.add("chief");
        stringList.add("user");
        return stringList;
    }

    @Override
    public List<String> getUserIDList(){
        List<String> stringList = new ArrayList<>();
        stringList.add("user");
        return stringList;
    }

    @Override
    public List<String> getAdminIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("admin");
        return stringList;
    }

    @Override
    public List<String> getHRIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("firstHRManager");
        stringList.add("secondHRManager");
        return stringList;
    }

    @Override
    public List<String> getChiefIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("chief");
        return stringList;
    }


    @Override
    public String getUserByFIO(String FIO){
        if (FIO.equals("Константинов Анатолий Владимирович")) {return "admin";
        } else if (FIO.equals("Денисов Дмитрий Валерьевич")) {return "firstHRManager";
        } else if (FIO.equals("Антонов Никита Геннадьевич")) {return "secondHRManager";
        } else if (FIO.equals("Долгих Максим Вадимович")) {return "chief";
        } else if (FIO.equals("Завгороднев Олег Вительевич")) {return "user";
        }
        return "Некто";
    }

    @Override
    public Integer getIdByLogin(User user) {
        if (user.login == "admin") {
            return 1;
        } else if (user.login == "firstHRManager") {
            return 2;
        } else if (user.login == "secondHRManager") {
            return 3;
        } else if (user.login == "chief") {
            return 4;
        } else if (user.login == "user") {
            return 5;
        }
        return null;
    }
}
