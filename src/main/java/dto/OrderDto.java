package dto;

import models.Type;

import java.time.LocalDate;

public class OrderDto {
    public Integer id;
    public Type type;
    public LocalDate date;
}
