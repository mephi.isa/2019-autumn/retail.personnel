package dto;

import java.time.LocalDate;

public class VacationDto {

    public LocalDate startDay;
    public LocalDate endDay;
    public Integer employee;
}
