package dto;

import java.util.List;

public class TotalVacationGraphDto {

    public Integer year;
    public List<DepartmentVacationDto> vacations;

}
