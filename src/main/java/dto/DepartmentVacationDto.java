package dto;

import java.util.List;

public class DepartmentVacationDto {


    public Integer departament;
    public List<VacationDto> vacationList;
    public Integer year;
    public boolean approved;
    public Integer idHREmpoloyee;

}
