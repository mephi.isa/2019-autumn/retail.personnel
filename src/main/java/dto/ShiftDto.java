package dto;

import java.time.LocalDate;

public class ShiftDto {

    public LocalDate date;
    public Integer limitOfShift;
    public Integer actualAmountEmployee;
}
