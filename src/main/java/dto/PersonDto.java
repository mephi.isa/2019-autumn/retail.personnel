package dto;

import models.ShiftType;

import java.time.LocalDate;
import java.util.List;

public class PersonDto {
    
    public String firstName;
    public String lastName;
    public String middleName;
    public LocalDate birthday;
    public Integer idPerson;
    public Integer title;
    public String INN;
    public ShiftType shiftType;
    public LocalDate rentDate;
    public LocalDate fireDate;
    public List<OrderDto> orders;
    public List<ShiftDto> shifts;
}
