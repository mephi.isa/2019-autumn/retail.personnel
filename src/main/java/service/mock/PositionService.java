package service.mock;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.*;

public interface PositionService {

    /* Получить все должности */
    /* Заглушка на время отсутствия БД */
    /* Номер title и соответствующий ему idDepartment*/
    static Map<Integer,Integer> findAll() {
        Map<Integer,Integer> positions = new HashMap<Integer, Integer>();
        positions.put(1,1);
        positions.put(2, 1);
        positions.put(3, 1);
        positions.put(4, 2);
        positions.put(5, 2);
        positions.put(6, 2);
        return positions;
    }

    static List<Integer> findAllDepartment() {
        List<Integer> idDepth = new ArrayList<>();
        idDepth.add(1);
        idDepth.add(2);
        return idDepth;
    }

    static List<Integer> getTitlesOfSubordinate (Integer idDept) {
        Map<Integer,List<Integer>> titles = new HashMap<Integer, List<Integer>>();
        List <Integer> tmp = new ArrayList<>();
        tmp.add(1);
        tmp.add(2);
        tmp.add(3);
        titles.put(1, tmp );
        List <Integer> tmp1 = new ArrayList<>();
        tmp1.add(4);
        tmp1.add(5);
        tmp1.add(6);
        titles.put(2,tmp1);
        return titles.get(idDept);
    }

}
