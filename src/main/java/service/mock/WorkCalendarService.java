package service.mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public interface WorkCalendarService {

    /* Получить производственный календарь */
    /* Заглушка на время отсутствия БД */
    static List<LocalDate> findAll() {
        List<LocalDate> calendar = new ArrayList<LocalDate>();
        calendar.add(LocalDate.of(2019,12,1));
        calendar.add(LocalDate.of(2019,12,7));
        calendar.add(LocalDate.of(2019,12,8));
        calendar.add(LocalDate.of(2019,12,14));
        calendar.add(LocalDate.of(2019,12,15));
        calendar.add(LocalDate.of(2019,12,21));
        calendar.add(LocalDate.of(2019,12,22));
        calendar.add(LocalDate.of(2019,12,28));
        calendar.add(LocalDate.of(2019,12,29));
        calendar.add(LocalDate.of(2019,12,31));
        calendar.add(LocalDate.of(2020,1,11));
        calendar.add(LocalDate.of(2020,1,12));
        calendar.add(LocalDate.of(2020,1,18));
        calendar.add(LocalDate.of(2020,1,19));
        calendar.add(LocalDate.of(2020,1,25));
        calendar.add(LocalDate.of(2020,1,26));
        calendar.add(LocalDate.of(2020,2,1));
        calendar.add(LocalDate.of(2020,2,2));
        return calendar;
    }
}
