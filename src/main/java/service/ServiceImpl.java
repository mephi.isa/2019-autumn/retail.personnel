package service;

import PersonnelDao.User;
import dao.Dao;
import dto.*;
import exceptions.*;
import models.*;
import service.classes.Action;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.List;

public class ServiceImpl implements Service {

    private AuthorizationService authorizationService;
    private PersonService personService;
    private VacationService vacationService;
    private ShiftService shiftService;
    private User user;

    public ServiceImpl(Dao dao) {
        this.authorizationService = new AuthorizationService(dao);
        this.personService = new PersonService(dao);
        this.vacationService = new VacationService(dao);
        this.shiftService = new ShiftService(dao);
    }

    public ServiceImpl(Dao dao, User user) {
        this.authorizationService = new AuthorizationService(dao);
        this.personService = new PersonService(dao);
        this.vacationService = new VacationService(dao);
        this.shiftService = new ShiftService(dao);
        this.user = user;
    }

    @Override
    public boolean checkAccessBySessionUuid(Action action, User user) throws AccessDeniedException, InvalidSessionException {
        return this.authorizationService.checkAccessBySessionUuid(action, user);
    }

    @Override
    public List<PersonDto> getSubordinatePersonList(User user, Integer idDep) throws AccessDeniedException, InvalidSessionException {
        return this.personService.getSubordinatePersonList(user, idDep);
    }

    @Override
    public boolean checkUserHasAccess(User user, Action action){
        return this.authorizationService.checkUserHasAccess(user, action);
    }

    @Override
    public List<PersonDto> getPersonList(User user) throws AccessDeniedException, InvalidSessionException {
        return this.personService.getPersonList(user);
    }

    @Override
    public PersonDto getPerson(User user, String INN) throws AccessDeniedException, InvalidSessionException {
        return this.personService.getPerson(user, INN);
    }

    @Override
    public PersonDto getPerson(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        return this.personService.getPerson(user, idPerson);
    }

    @Override
    public void rentPerson(User user, String firstName, String secondName,
                           String middleName, LocalDate birthday, String INN, Integer title,
                           LocalDate rentDate, OrderDto order, ShiftType type)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        this.personService.rentPerson(user,firstName,secondName,middleName,birthday,INN,title,rentDate,order,type);
    }

    @Override
    public void updateFirstName(User user, String firstName, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        this.personService.updateFirstName(user, firstName, idPerson);
    }

    @Override
    public void updateLastName(User user, String lastName, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        this.personService.updateLastName(user, lastName, idPerson);
    }

    @Override
    public void updateMiddleName(User user, String middleName, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        this.personService.updateMiddleName(user, middleName, idPerson);
    }

    @Override
    public void updateTitle(User user, Integer newTitle, OrderDto order, Integer idPerson) throws InvalidDataException, EntitySavingException, AccessDeniedException, InvalidSessionException, WrongOrderException {
        this.personService.updateTitle(user, newTitle, order, idPerson);
    }

    @Override
    public void updateShiftType(User user, ShiftType newType, OrderDto order, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        this.personService.updateShiftType(user, newType, order, idPerson);
    }

    @Override
    public void firePerson(User user, LocalDate fireDate, OrderDto order, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, WrongOrderException, WrongDateException {
        this.personService.firePerson(user, fireDate, order, idPerson);
    }

    @Override
    public List<OrderDto> getAllPersonOrders(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, Exception {
        return this.personService.getAllPersonOrders(user, idPerson);
    }


    @Override
    public List<VacationDto> getAllVacationOfPerson(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getAllVacationOfPerson(user,idPerson);
    }

    @Override
    public List<VacationDto> getAllDepartmentVacationByYear(User user, Integer year, Integer idDepartment) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getAllDepartmentVacationByYear(user, year, idDepartment);
    }

    @Override
    public DepartmentVacationDto getDepartmentVacationByYearId(User user, Integer idDepartment, Integer year) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getDepartmentVacationByYearId(user, idDepartment, year);
    }

 /*   @Override
    public void addVacation(User user, Vacation vacation, DepartmentVacation departmentVacation) throws AccessDeniedException, InvalidSessionException {
        this.vacationService.addVacation(user, vacation, departmentVacation);
    }

  */

    @Override
    public void updateDepartmentVacation(User user, DepartmentVacationDto departmentVacation, Integer idHR) throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        this.vacationService.updateDepartmentVacation(user, departmentVacation, idHR);
    }

    @Override
    public void addDepartmentVacation(User user, DepartmentVacationDto departmentVacation) throws AccessDeniedException, InvalidSessionException, EntitySavingException {
        this.vacationService.addDepartmentVacation(user, departmentVacation);
    }

    @Override
    public void addVacation(User user, VacationDto vacation) throws AccessDeniedException, EntitySavingException {
        this.vacationService.addVacation(user, vacation);
    }

    @Override
    public void createTotalVacationGraph(User user, List<Integer> idsDepartment, Integer year) throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException, EntitySavingException {
        this.vacationService.createTotalVacationGraph(user, idsDepartment, year);
    }

    @Override
    public TotalVacationGraphDto getTotalVacationGraphByYear(User user, Integer year) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getTotalVacationGraphByYear(user, year);
    }

    @Override
    public List<TotalVacationGraphDto> getAllTotalVacationGraph(User user) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getAllTotalVacationGraph(user);
    }

    @Override
    public List<DepartmentVacationDto> getDepartmentVacationsByYear(User user, Integer year) throws AccessDeniedException, InvalidSessionException {
        return this.vacationService.getDepartmentVacationsByYear(user, year);
    }

    @Override
    public List<ShiftDto> getAllPersonShift(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        return this.shiftService.getAllPersonShift(user,idPerson);
    }

    @Override
    public void addChiefShift(User user, Integer idPerson, ShiftDto shift, Integer idDept) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        this.shiftService.addChiefShift(user, idPerson, shift, idDept);
    }

    @Override
    public void addHrShift(User user, Integer idPerson, ShiftDto shift) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        this.shiftService.addHrShift(user, idPerson, shift);
    }


}
