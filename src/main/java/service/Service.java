package service;

import PersonnelDao.User;
import dto.*;
import exceptions.*;
import models.*;
import service.classes.Action;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public interface Service {

    public boolean checkUserHasAccess(User user, Action action);
    boolean checkAccessBySessionUuid(Action action, User user) throws AccessDeniedException, InvalidSessionException;

    List<PersonDto> getSubordinatePersonList(User user, Integer idDep) throws AccessDeniedException, InvalidSessionException;
        List<PersonDto> getPersonList(User user) throws AccessDeniedException, InvalidSessionException;
    PersonDto getPerson(User user, String INN) throws AccessDeniedException, InvalidSessionException;
    PersonDto getPerson(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException;
    void rentPerson(User user, String firstName, String secondName, String middleName,
                    LocalDate birthday, String INN, Integer title, LocalDate rentDate, OrderDto order, ShiftType type)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException;
  void updateFirstName(User user, String firstName, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException;
    void updateLastName (User user, String lastName, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException;
    void updateMiddleName (User user, String middleName,Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException;
    void updateTitle (User user, Integer newTitle, OrderDto order, Integer idPerson)
            throws InvalidDataException, EntitySavingException, AccessDeniedException, InvalidSessionException, WrongOrderException;
    void updateShiftType (User user, ShiftType newType, OrderDto order, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException;
    void firePerson (User user, LocalDate fireDate, OrderDto order, Integer idPerson) throws AccessDeniedException,
            InvalidSessionException, InvalidDataException, EntitySavingException, WrongOrderException, WrongDateException;
    List<OrderDto> getAllPersonOrders (User user , Integer idPerson)  throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, Exception;

    List<VacationDto> getAllVacationOfPerson(User user, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException;
    List<VacationDto> getAllDepartmentVacationByYear(User user, Integer year, Integer idDepartment )
            throws AccessDeniedException, InvalidSessionException;
    DepartmentVacationDto getDepartmentVacationByYearId (User user, Integer idDepartment, Integer year)
            throws AccessDeniedException, InvalidSessionException;
    void updateDepartmentVacation (User user, DepartmentVacationDto departmentVacation, Integer idHR)
            throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException;
    void addDepartmentVacation (User user, DepartmentVacationDto departmentVacation)
            throws AccessDeniedException, InvalidSessionException, EntitySavingException;
    void addVacation(User user, VacationDto vacation) throws AccessDeniedException, EntitySavingException;
    void createTotalVacationGraph(User user, List<Integer> idsDepartment, Integer year)
            throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException, EntitySavingException;
    TotalVacationGraphDto getTotalVacationGraphByYear(User user, Integer year)
            throws AccessDeniedException, InvalidSessionException;
    List<TotalVacationGraphDto> getAllTotalVacationGraph (User user)
            throws AccessDeniedException, InvalidSessionException;
    List<DepartmentVacationDto> getDepartmentVacationsByYear (User user, Integer year) throws AccessDeniedException, InvalidSessionException;

    List<ShiftDto> getAllPersonShift (User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException;
    void addChiefShift (User user, Integer idPerson, ShiftDto shift, Integer idDept) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException;
    void addHrShift (User user, Integer idPerson, ShiftDto shift) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException;


}
