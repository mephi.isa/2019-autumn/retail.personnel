package service;

import PersonnelDao.User;
import dao.Dao;
import dto.ShiftDto;
import exceptions.EntitySavingException;
import exceptions.InvalidSessionException;
import exceptions.WrongShiftException;
import models.Person;
import models.Shift;
import models.ShiftType;
import models.Vacation;
import service.classes.Action;
import service.mock.PositionService;
import service.mock.WorkCalendarService;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ShiftService {

    private AuthorizationService authorizationService;
    private Dao dao;

    ShiftService (Dao dao) {
        this.authorizationService = new AuthorizationService(dao);
        this.dao = dao;
    }


    List<ShiftDto> getAllPersonShift (User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        this.authorizationService.checkAccessBySessionUuid(Action.SHIFT_GET, user);
        List<Shift> shiftList= dao.getPersonShift(dao.getPersonById(idPerson));
        if (shiftList == null) {
            return null;
        } else {
            List<ShiftDto> shiftDtos = new ArrayList<>();
            for (Shift shift:shiftList) {
                shiftDtos.add(createShiftDto(shift));
            }
            return shiftDtos;
        }
    }

    private void addingShift ( Person person, Shift shift, List<LocalDate> weekends) throws WrongShiftException, EntitySavingException {
        if (person.getShiftType() == ShiftType.NORMAL) {
            if (!weekends.contains(shift.getDate())) {
                if (person.addShift(shift)) {
                    this.dao.updatePerson(person);
                    this.dao.addShift(shift, person);
                }
            } else {
                throw new WrongShiftException("This day is weekend");
            }
        } else {
            if (person.addShift(shift)) {
                this.dao.updatePerson(person);
                this.dao.addShift(shift, person);
            } else {
                throw new WrongShiftException("Wrong shift days");
            }
        }
    }

    void addChiefShift (User user, Integer idPerson, ShiftDto shift, Integer idDepartmnet) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        this.authorizationService.checkAccessBySessionUuid(Action.SHIFT_CHIEF_CREATE, user);
        Person person = dao.getPersonById(idPerson);
        Shift shift1 = createShift(shift);
        if (!PositionService.getTitlesOfSubordinate(idDepartmnet).contains(person.getTitle())) {
            throw new AccessDeniedException("You cannot add shift not yours subordinate");
        }
        if (this.checkVacation(person, shift1.getDate())) {
            addingShift(person,shift1, WorkCalendarService.findAll());
        } else {
            throw new WrongShiftException("Its vacation time!");
        }
    }

    void addHrShift (User user, Integer idPerson, ShiftDto shift) throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
       this.authorizationService.checkAccessBySessionUuid(Action.SHIFT_HR_CREATE, user);
       Person person = dao.getPersonById(idPerson);
       Shift shift1 = createShift(shift);
       if (this.checkVacation(person, shift1.getDate())) {
           addingShift(person,shift1,WorkCalendarService.findAll());
       } else {
           throw new WrongShiftException("Its vacation time!");
       }
    }

    private boolean checkVacation (Person person, LocalDate date) {
        boolean isEmpoyeeOnWorkAtDate = true;
        List<Vacation> vacations = this.dao.getAllVacationOfPerson(person);
        if (vacations!= null) {
            for (int i = 0; i < vacations.size(); i++) {
                if ((vacations.get(i).getStartDay().isBefore(date) && vacations.get(i).getEndDay().isAfter(date)) ||
                    vacations.get(i).getStartDay() == date || vacations.get(i).getEndDay() == date) {
                    isEmpoyeeOnWorkAtDate = false;
                }
            }
        }
        return isEmpoyeeOnWorkAtDate;
    }

    public ShiftDto createShiftDto (Shift shift) {
        ShiftDto shiftDto = new ShiftDto();
        shiftDto.date = shift.getDate();
        shiftDto.limitOfShift = shift.getLimitOfShift();
        shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
        return shiftDto;
    }

    public Shift createShift (ShiftDto shiftDto) {
        Shift shift = new Shift(shiftDto.date,shiftDto.limitOfShift,shiftDto.actualAmountEmployee);
        return shift;
    }
}
