package service;

import PersonnelDao.User;
import dao.Dao;
import dto.*;
import exceptions.EntitySavingException;
import exceptions.InvalidSessionException;
import exceptions.WrongDeptVacationException;
import models.*;
import net.bytebuddy.asm.Advice;
import org.hibernate.Session;
import service.classes.Action;

import javax.persistence.criteria.CriteriaBuilder;
import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VacationService {

    private AuthorizationService authorizationService;
    private Dao dao;

    VacationService(Dao dao) {
        this.dao = dao;
        this.authorizationService = new AuthorizationService(dao);
    }

    private boolean validVacationOfDepartment (Integer idDepartment, Integer year) {
        boolean res = true;
        DepartmentVacation departmentVacation = dao.getDepartmentVacationByYearId(idDepartment,year);
        List<Vacation> tmpVac = departmentVacation.getVacationList();
        List<Person> tmpPer = departmentVacation.getAllPerson();
        Long amount = 0L;
        for (int i=0; i<tmpPer.size(); i++) {
            tmpVac = departmentVacation.getAllPersonVacation(tmpPer.get(i));
            if (dao.getDepartmentVacationByYearId(idDepartment, year-1) != null) {
                amount = departmentVacation.getAmountDay(tmpPer.get(i).getIdPerson())
                        + dao.getDepartmentVacationByYearId(idDepartment, year-1).getAmountDay(tmpPer.get(i).getIdPerson());
                if (amount > 56) {
                    res = false;
                }
            } else {
                amount = departmentVacation.getAmountDay(tmpPer.get(i).getIdPerson());
                if (amount > 28) {
                    res = false;
                }
            }

        }
        return res;
    }

    List<VacationDto> getAllVacationOfPerson(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_GET, user);
        List<Vacation> vacationList =  dao.getAllVacationOfPerson(dao.getPersonById(idPerson));
        if (vacationList == null) {
            return null;
        } else {
            List<VacationDto> vacations = new ArrayList<>();
            for (Vacation vacation:vacationList) {
                vacations.add(this.createVacationDto(vacation));
            }
            return vacations;
        }
    }

    List<VacationDto> getAllDepartmentVacationByYear(User user, Integer year, Integer idDepartment ) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_GET, user);
        List<Vacation> vacationList = dao.getAllDepartmentVacationByYear(year,idDepartment);
        if (vacationList == null) {
            return null;
        } else {
            List<VacationDto> vacations = new ArrayList<>();
            for (Vacation vacation:vacationList) {
                vacations.add(createVacationDto(vacation));
            }
            return vacations;
        }
    }

    DepartmentVacationDto getDepartmentVacationByYearId (User user, Integer idDepartment, Integer year) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_GET, user);
        DepartmentVacation departmentVacation = dao.getDepartmentVacationByYearId(idDepartment, year);
        return createDepartmentVacationDto(departmentVacation);
    }

    void updateDepartmentVacation (User user, DepartmentVacationDto departmentVacation, Integer idHR) throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_UPDATE, user);
        if (this.validVacationOfDepartment(departmentVacation.departament,departmentVacation.year)) {
            DepartmentVacation departmentVacation1 = dao.getDepartmentVacationByYearId(departmentVacation.departament,departmentVacation.year);
            departmentVacation1.approve(idHR);
            dao.updateDepartmentVacation(departmentVacation1);
        } else {
            throw  new WrongDeptVacationException("Smth wrong in department vacation");
        }
    }

    void addDepartmentVacation (User user, DepartmentVacationDto departmentVacation) throws AccessDeniedException, InvalidSessionException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, user);
        dao.addDepartmentVacation(createDepartmentVacation(departmentVacation));
    }

    void addVacation (User user, VacationDto vacation) throws AccessDeniedException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, user);
        dao.addVacation(createVacation(vacation));
    }
    void createTotalVacationGraph(User user, List<Integer> idsDepartment, Integer year) throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.TOTAL_GRAPH_CREATE, user);
        List<DepartmentVacation> departmentVacationList = dao.getDepartmentVacationsByYear(year);
        boolean isAllDepthVacationApproved = true;
        List<Integer> deptIdsTemp = new ArrayList<>();
        for (int i = 0; i<departmentVacationList.size(); i++) {
            deptIdsTemp.add(departmentVacationList.get(i).getDepartament());
            isAllDepthVacationApproved = isAllDepthVacationApproved && departmentVacationList.get(i).isApproved();
        }
        if (!isAllDepthVacationApproved) {
            throw new WrongDeptVacationException("Not all department vacation graph approved");
        }
        for (int i=0; i< idsDepartment.size(); i++) {
            if (!deptIdsTemp.contains(idsDepartment.get(i))) {
              //  isAllDepthVacationExist = false;
                throw new WrongDeptVacationException("Not all department add vacation graph");
            }
        }
        TotalVacationGraph totalVacationGraph = new TotalVacationGraph(year, departmentVacationList);
        dao.addTotalVacationGraph(totalVacationGraph);
    }

    TotalVacationGraphDto getTotalVacationGraphByYear(User user, Integer year) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.TOTAL_GRAPH_GET, user);
        TotalVacationGraph totalVacationGraph = dao.getGraphByYear(year);
        return createTotalVacationGraphDto(totalVacationGraph);
    }

    List<TotalVacationGraphDto> getAllTotalVacationGraph (User user) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.TOTAL_GRAPH_GET, user);
        List<TotalVacationGraph> totalVacationGraphList = dao.getListTotalVacationGraph();
        if (totalVacationGraphList == null) {
            return null;
        } else {
            List<TotalVacationGraphDto> totalVacationGraphDtos = new ArrayList<>();
            for (TotalVacationGraph totalVacationGraph:totalVacationGraphList) {
                totalVacationGraphDtos.add(createTotalVacationGraphDto(totalVacationGraph));
            }
            return totalVacationGraphDtos;
        }
    }

    List<DepartmentVacationDto> getDepartmentVacationsByYear (User user, Integer year) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_GET, user);
        List<DepartmentVacation> departmentVacationList = dao.getDepartmentVacationsByYear(year);
        if (departmentVacationList == null) {
            return null;
        } else {
            List<DepartmentVacationDto> departmentVacationDtos = new ArrayList<>();
            for (DepartmentVacation departmentVacation:departmentVacationList) {
                departmentVacationDtos.add(createDepartmentVacationDto(departmentVacation));
            }
            return departmentVacationDtos;
        }
    }


    public VacationDto createVacationDto(Vacation vacation){
        VacationDto vacationDto = new VacationDto();
        vacationDto.startDay = vacation.getStartDay();
        vacationDto.endDay = vacation.getEndDay();
        vacationDto.employee = vacation.getEmployee().getIdPerson();
        return vacationDto;
    }

    public DepartmentVacationDto createDepartmentVacationDto(DepartmentVacation departmentVacation){
        DepartmentVacationDto departmentVacationDto = new DepartmentVacationDto();
        departmentVacationDto.departament = departmentVacation.getDepartament();
        departmentVacationDto.vacationList = new ArrayList<>();
        departmentVacationDto.year = departmentVacation.getYear();
        departmentVacationDto.approved = departmentVacation.isApproved();
        departmentVacationDto.idHREmpoloyee = departmentVacation.getIdHREmpoloyee();
        for (Vacation vacation:departmentVacation.getVacationList()) {
            departmentVacationDto.vacationList.add(createVacationDto(vacation));
        }
        return departmentVacationDto;
    }

    public Vacation createVacation (VacationDto vacationDto) {
        Vacation vacation = new Vacation(vacationDto.startDay,vacationDto.endDay,dao.getPersonById(vacationDto.employee));
        return vacation;
    }

    public DepartmentVacation createDepartmentVacation(DepartmentVacationDto departmentVacationDto){
        List<Vacation> vacationList = new ArrayList<>();
        for (VacationDto vacationDto:departmentVacationDto.vacationList) {
            vacationList.add(createVacation(vacationDto));
        }
        DepartmentVacation departmentVacation = new DepartmentVacation(departmentVacationDto.departament,
                vacationList,departmentVacationDto.year,
                departmentVacationDto.approved,departmentVacationDto.idHREmpoloyee);

        return departmentVacation;
    }

    public TotalVacationGraphDto createTotalVacationGraphDto (TotalVacationGraph totalVacationGraph) {
        TotalVacationGraphDto totalVacationGraphDto = new TotalVacationGraphDto();
        totalVacationGraphDto.year = totalVacationGraph.getYear();
        totalVacationGraphDto.vacations = new ArrayList<>();
        for (DepartmentVacation departmentVacation:totalVacationGraph.getVacations()) {
            totalVacationGraphDto.vacations.add(createDepartmentVacationDto(departmentVacation));
        }
        return totalVacationGraphDto;
    }
}
