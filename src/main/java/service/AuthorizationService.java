package service;

import PersonnelDao.User;
import dao.Dao;
import service.classes.Action;
import service.classes.Role;

import java.nio.file.AccessDeniedException;

class AuthorizationService {

   // private Long timeout = 10L;
    private Dao dao;

    AuthorizationService(Dao dao) {this.dao = dao;}

    public boolean checkUserHasAccess(User user, Action action) {
        Role userRole = user.role;
        if(userRole == null)
            return false;

        switch (userRole) {
            case ADMIN:
                return true;
            case HR:
                if(action == Action.SHIFT_CHIEF_CREATE || action == Action.SHIFT_CHIEF_UPDATE ||
                        action == Action.DEPARTMENT_GRAPH_CREATE || action == Action.DEPARTMENT_GRAPH_UPDATE)
                    return false;
                else
                    return true;
            case USER:
                if(action == Action.TOTAL_GRAPH_GET || action == Action.DEPARTMENT_GRAPH_GET
                        || action == Action.INFORMATION_GET || action == Action.SHIFT_GET)
                    return true;
                else
                    return false;
            case CHIEF:
                if (action == Action.SHIFT_CHIEF_CREATE || action == Action.SHIFT_CHIEF_UPDATE ||
                        action == Action.DEPARTMENT_GRAPH_CREATE || action == Action.DEPARTMENT_GRAPH_UPDATE ||
                        action == Action.TOTAL_GRAPH_GET || action == Action.DEPARTMENT_GRAPH_GET
                        || action == Action.INFORMATION_GET || action == Action.SHIFT_GET)
                    return true;
                else
                    return false;
        }
        return false;
    }

    boolean checkAccessBySessionUuid(Action action, User user) throws AccessDeniedException {
        boolean isRoleHasAccess = this.checkUserHasAccess(user, action);
        if (isRoleHasAccess)
            return true;
        else
            throw new AccessDeniedException(String.format("Access denied for operation %s", action));
    }

}
