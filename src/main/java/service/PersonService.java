package service;

import PersonnelDao.User;
import dao.Dao;
import dto.OrderDto;
import dto.PersonDto;
import dto.ShiftDto;
import exceptions.*;
import models.Order;
import models.Person;
import models.Shift;
import models.ShiftType;
import service.classes.Action;
import service.mock.PositionService;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonService {

    private AuthorizationService authorizationService;
    private Dao dao;

    PersonService(Dao dao) {
        this.dao = dao;
        this.authorizationService = new AuthorizationService(dao);
    }

    List<PersonDto> getPersonList(User user) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_GET,user);
        List<Person> personList = dao.getPersonList();
        if (personList==null) {
            return null;
        }
        List<PersonDto> personDtoList = new ArrayList<>();
        for (Person person: personList) {
            personDtoList.add(this.createPersonDto(person));
        }
        return personDtoList;

    }


    List<PersonDto> getSubordinatePersonList(User user, Integer idDep) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_GET,user);
        List<Person> personList = dao.getSubordinatePersonList(PositionService.getTitlesOfSubordinate(idDep));
        if (personList==null) {
            return null;
        }
        List<PersonDto> personDtoList = new ArrayList<>();
        for (Person person: personList) {
            personDtoList.add(this.createPersonDto(person));
        }
        return personDtoList;
    }

    PersonDto getPerson(User user, String INN) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_GET,user);
        return this.createPersonDto(dao.getPersonByINN(INN));
    }

    PersonDto getPerson(User user, Integer idPerson) throws AccessDeniedException, InvalidSessionException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_GET,user);
        return this.createPersonDto(dao.getPersonById(idPerson));
    }

    void rentPerson(User user, String firstName, String secondName, String middleName,
                   LocalDate birthday, String INN, Integer title, LocalDate rentDate, OrderDto order, ShiftType type)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_CREATE,user);

        boolean isPersonDateValid = firstName != null && !"".equals(firstName) &&
                secondName != null && !"".equals(secondName) && birthday != null && INN != null &&
                !"".equals(INN) && title != null && rentDate != null && order != null && type != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while adding new person");

        Person person = dao.getPersonByINN(INN);

        boolean isPersonAlreadyExist = person != null;
        if(isPersonAlreadyExist)
            throw new EntitySavingException("Can`t add person. Person with INN is already exist");

        dao.addPerson(new Person(firstName, secondName, middleName, birthday, INN, title,
                      rentDate, new Order(order.id, order.type, order.date), type));
    }

    void updateFirstName(User user, String firstName, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE,user);

        boolean isPersonDateValid = firstName != null && !"".equals(firstName) && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while updating first name person");

        Person person = this.dao.getPersonById(idPerson);

        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t update first name. Person with such id doesn`t exist");

        person.setFirstName(firstName);

        dao.updatePerson(person);
    }

    void updateLastName (User user, String lastName, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE,user);

        boolean isPersonDateValid = lastName != null && !"".equals(lastName) && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while updating last name");

        Person person = this.dao.getPersonById(idPerson);
        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t update las name. Person with such id doesn`t exist");

        person.setLastName(lastName);

        dao.updatePerson(person);
    }

    void updateMiddleName (User user, String middleName, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE,user);

        boolean isPersonDateValid = middleName != null && !"".equals(middleName) && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while updating middle name");

        Person person = this.dao.getPersonById(idPerson);
        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t update middle name. Person with such id doesn`t exist");

        person.setMiddleName(middleName);

        dao.updatePerson(person);
    }

    void updateTitle (User user, Integer newTitle, OrderDto order, Integer idPerson) throws InvalidDataException,
            EntitySavingException, AccessDeniedException, InvalidSessionException, WrongOrderException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE,user);

        boolean isPersonDateValid = newTitle != null && order != null && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while updating title");

        Person person = this.dao.getPersonById(idPerson);

        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t update title. Person with such id doesn`t exist");

        person.changeTitle(newTitle,new Order(order.id,order.type,order.date));

        dao.updatePerson(person, new Order(order.id,order.type,order.date));
    }

    void updateShiftType (User user, ShiftType newType, OrderDto order, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE,user);
        boolean isPersonDateValid = newType != null && order != null && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while updating shift type");

        Person person = this.dao.getPersonById(idPerson);

        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t update shift type. Person with such id doesn`t exist");

        person.setShiftType(newType, new Order (order.id,order.type, order.date));

        dao.updatePerson(person, new Order(order.id, order.type, order.date));
    }

    void firePerson (User user, LocalDate fireDate, OrderDto order, Integer idPerson)
            throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, WrongOrderException, WrongDateException {

        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, user);
        boolean isPersonDateValid = fireDate != null && order != null && idPerson != null;
        if(!isPersonDateValid)
            throw new InvalidDataException("Invalid data while firing person");

        Person person = this.dao.getPersonById(idPerson);

        boolean isPersonAlreadyExist = person != null;
        if(!isPersonAlreadyExist)
            throw new EntitySavingException("Can`t fire person. Person with such id doesn`t exist");

        person.firePerson(new Order(order.id,order.type, order.date),fireDate);

        dao.updatePerson(person, new Order(order.id,order.type, order.date));
    }

    List<OrderDto> getAllPersonOrders (User user, Integer idPerson)  throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, Exception {
        authorizationService.checkAccessBySessionUuid(Action.INFORMATION_GET,user);
        List<Order> orderList = dao.getOrderList(dao.getPersonById(idPerson));
        if (orderList == null) {
            return null;
        } else {
            List<OrderDto> orders = new ArrayList<>();
            for (Order order:orderList) {
                OrderDto orderDto = new OrderDto();
                orderDto.type = order.getType();
                orderDto.id = order.getId();
                orderDto.date = order.getDate();
                orders.add(orderDto);
            }
            return orders;
        }
    }

    public PersonDto createPersonDto(Person person){
        PersonDto personDto = new PersonDto();
        personDto.firstName = person.getFirstName();
        personDto.lastName = person.getLastName();
        personDto.middleName = person.getMiddleName();
        personDto.birthday = person.getBirthday();
        personDto.idPerson = person.getIdPerson();
        personDto.INN = person.getINN();
        personDto.title = person.getTitle();
        personDto.shiftType = person.getShiftType();
        personDto.rentDate = person.getRentDate();
        personDto.fireDate = person.getFireDate();
        List<OrderDto> orders = new ArrayList<>();
        for (Order order: person.getOrders()) {
            OrderDto orderDto = new OrderDto();
            orderDto.date = order.getDate();
            orderDto.id = order.getId();
            orderDto.type = order.getType();
            orders.add(orderDto);
        }
        personDto.orders = orders;
        if (person.getShifts() != null) {
            List<ShiftDto> shifts = new ArrayList<>();
            for (Shift shift : person.getShifts()) {
                ShiftDto shiftDto = new ShiftDto();
                shiftDto.date = shift.getDate();
                shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
                shiftDto.limitOfShift = shift.getLimitOfShift();
                shifts.add(shiftDto);
            }
            personDto.shifts = shifts;
        } else {
            personDto.shifts = null;
        }
        return personDto;
    }

    public OrderDto createOrderDto (Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.date = order.getDate();
        orderDto.id = order.getId();
        orderDto.type = order.getType();
        return orderDto;
    }
}
