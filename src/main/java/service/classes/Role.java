package service.classes;

public enum Role {
    HR,
    CHIEF,
    ADMIN,
    USER
}
