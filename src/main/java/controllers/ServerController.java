package controllers;

import PersonnelDao.PersonnelDao;
import PersonnelDao.User;
import dao.Dao;
import dao.entities.ShiftEntity;
import dao.entities.VacationEntity;
import dto.*;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import models.Order;
import models.ShiftType;
import models.Type;
import models.Vacation;
import service.Service;
import service.ServiceImpl;
import service.classes.Role;
import service.mock.PositionService;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

import java.time.LocalDate;
import java.util.*;

import static spark.Spark.*;

public class ServerController {
    private PersonnelDao authController;
    //private ModelController modelController;
    private Dao dao;

    public ServerController(Dao dao,PersonnelDao personnelDao){
        this.dao = dao;
        authController = personnelDao;
  //      modelController = new ModelController();
    }

    public void run() {
        org.apache.log4j.BasicConfigurator.configure();
        port(8080);

        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();
        Configuration freemarkerConfiguration = new Configuration();
        freemarkerConfiguration.setTemplateLoader(new ClassTemplateLoader(ServerController.class,
                "/templates/"));
        freeMarkerEngine.setConfiguration(freemarkerConfiguration);
        staticFileLocation("/static");

        get("/", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if(user == null)
                response.redirect("/auth");
            if(user.role == Role.ADMIN){
                response.redirect("/admin");
            }
            if(user.role == Role.HR){
                response.redirect("/hr");
            }
            if(user.role == Role.CHIEF){
                response.redirect("/chief");
            }
            if (user.role == Role.USER) {
                response.redirect("/user");
            }
            return "success";
        });

        get("/auth", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            String login, password;
            login = request.queryParams("login");
            password= request.queryParams("password");
            if(authController.getUser(login,password) == null){
                model.put("status", "Wrong login or password");
                return freeMarkerEngine.render(new ModelAndView(model, "auth.ftl"));
            }
            response.cookie("/", "login", login, 10 * 50, false);
            response.cookie("/", "password", password, 10 * 50, false);
            response.redirect("/");
            return "success";
        });

        get("/logout", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            String cookie = request.cookie(null);
            response.redirect("/auth");
            return "success";
        });


        get("/hr", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();

            try {

                String FIO = authController.getUserFIO(user);
                model.put("hrID",FIO);

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"hr.ftl"));
        });

        get("/admin", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();

            try {

                String FIO = authController.getUserFIO(user);
                model.put("adminID",FIO);

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"supervisor.ftl"));
        });

        get("/user", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();

            try {

                String FIO = authController.getUserFIO(user);
                model.put("userID",FIO);

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"user.ftl"));
        });

        get("/chief", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();

            try {

                String FIO = authController.getUserFIO(user);
                model.put("chiefID",FIO);
                model.put("idDept", user.idDept);

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"chief.ftl"));
        });

        get("/addperson", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String firstName = request.queryParams("firstName");
                String secondName = request.queryParams("secondName");
                String middleName = request.queryParams("middleName");
                String INN = request.queryParams("INN");
                String b_day = request.queryParams("b_day");
                String b_month = request.queryParams("b_month");
                String b_year = request.queryParams("b_year");
                int b_day_i = Integer.parseInt(b_day);
                int b_month_i = Integer.parseInt(b_month);
                int b_year_i = Integer.parseInt(b_year);
                LocalDate birthday = LocalDate.of(b_year_i,b_month_i,b_day_i);
                String title_s = request.queryParams("title");
                Integer title = Integer.parseInt(title_s);
                String r_day = request.queryParams("r_day");
                String r_month = request.queryParams("r_month");
                String r_year = request.queryParams("r_year");
                int r_day_i = Integer.parseInt(r_day);
                int r_month_i = Integer.parseInt(r_month);
                int r_year_i = Integer.parseInt(r_year);
                LocalDate rentDate = LocalDate.of(r_year_i,r_month_i,r_day_i);
                OrderDto orderDto = new OrderDto();
                String orderId = request.queryParams("orderid");
                orderDto.id = Integer.parseInt(orderId);
                orderDto.type = Type.RENT;
                String o_day = request.queryParams("o_day");
                String o_month = request.queryParams("o_month");
                String o_year = request.queryParams("o_year");
                int o_day_i = Integer.parseInt(o_day);
                int o_month_i = Integer.parseInt(o_month);
                int o_year_i = Integer.parseInt(o_year);
                orderDto.date = LocalDate.of(o_year_i,o_month_i,o_day_i);
                String shiftTyp = request.queryParams("shifttype");
                ShiftType type = null;
                if (shiftTyp.toUpperCase().equals("IN_SHIFTS")) {
                    type = ShiftType.IN_SHIFTS;
                } else {
                    type = ShiftType.NORMAL;
                }
                service.rentPerson(user,firstName,secondName,middleName,birthday,INN,title,rentDate,orderDto,type);
            }catch (Exception ignored){
            }
            response.redirect("/person");
            return "success";
        });

        get("/person", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");

            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");

            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            List<List> personList = new ArrayList<>();
            List<List> personSubList = new ArrayList<>();
            List<List> shortPersonList = new ArrayList<>();

            try {

                //   List<TotalVacationGraphDto> totalVacationGraphDtoList = service.getAllTotalVacationGraph(user);
                List<PersonDto> personDtoList = service.getPersonList(user);
                List<PersonDto> personSubDtoList = null;
                if (user.role == Role.CHIEF) {
                    personSubDtoList = service.getSubordinatePersonList(user, user.idDept);
                }
                String FIO = authController.getUserFIO(user);
                //model.put("adminID",FIO);
                model.put("user", user.role.toString());

                for (PersonDto personDto : personDtoList) {
                    if (personDto.fireDate == null) {
                        personList.add(Arrays.asList(personDto.idPerson,   personDto.lastName, personDto.firstName, personDto.middleName,
                                personDto.birthday, personDto.INN,
                                personDto.title, personDto.shiftType, personDto.rentDate));
                        shortPersonList.add(Arrays.asList(personDto.idPerson, personDto.lastName, personDto.firstName, personDto.middleName, personDto.title));
                    } else {
                        personList.add(Arrays.asList(personDto.idPerson,   personDto.lastName, personDto.firstName, personDto.middleName,
                                personDto.birthday, personDto.INN,
                                personDto.title, personDto.shiftType, personDto.rentDate, personDto.fireDate));
                       // shortPersonList.add(Arrays.asList(personDto.idPerson,   personDto.lastName, personDto.firstName, personDto.middleName, personDto.title));
                    }
                }
                model.put("shortpersons", shortPersonList);
                model.put("persons", personList);
                if (personSubDtoList != null) {
                    for (PersonDto personDto : personSubDtoList) {
                        if (personDto.fireDate == null) {
                            personSubList.add(Arrays.asList(personDto.idPerson, personDto.lastName, personDto.firstName, personDto.middleName,
                                    personDto.title, personDto.shiftType));
                        }
                    }
                }

                model.put("subpersons", personSubList);


            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"personal.ftl"));
        });


        get("/editperson", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);
            Map<String, Object> model = new HashMap<>();
            try {
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/person", "idPerson", idPerson1, 10 * 50, false);

                PersonDto personDto = service.getPerson(user, idPerson);
                List<List> personList = new ArrayList<>();
                if (personDto.fireDate == null) {
                    personList.add(Arrays.asList(personDto.idPerson,  personDto.lastName, personDto.firstName, personDto.middleName,
                            personDto.birthday, personDto.INN,
                            personDto.title, personDto.shiftType, personDto.rentDate));
                } else {
                    personList.add(Arrays.asList(personDto.idPerson,  personDto.lastName, personDto.firstName, personDto.middleName,
                            personDto.birthday, personDto.INN,
                            personDto.title, personDto.shiftType, personDto.rentDate, personDto.fireDate));
                }

                model.put("persons", personList);
                model.put("user", user.role.toString());
                //     model.put("manager", authController.getUserFIO(categoryDto.manager.toString()));
                model.put("idPerson", idPerson1);
            } catch (Exception e) {
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model, "editperson.ftl"));
        });

        get("/editfirstname", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);
                String firstName = request.queryParams("firstName");

                service.updateFirstName(user,firstName,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/editmiddlename", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);
                String middleName = request.queryParams("middleName");

                service.updateMiddleName(user,middleName,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/editlastname", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);
                String lastName = request.queryParams("lastName");

                service.updateLastName(user,lastName,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/edititle", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);
                String title_s = request.queryParams("title");
                Integer title = Integer.parseInt(title_s);
                OrderDto orderDto = new OrderDto();
                String orderId = request.queryParams("orderid");
                orderDto.id = Integer.parseInt(orderId);
                orderDto.type = Type.TRANSFER;
                String o_day = request.queryParams("o_day");
                String o_month = request.queryParams("o_month");
                String o_year = request.queryParams("o_year");
                int o_day_i = Integer.parseInt(o_day);
                int o_month_i = Integer.parseInt(o_month);
                int o_year_i = Integer.parseInt(o_year);
                orderDto.date = LocalDate.of(o_year_i,o_month_i,o_day_i);
                service.updateTitle(user,title,orderDto,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/fireperson", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);

                String r_day = request.queryParams("r_day");
                String r_month = request.queryParams("r_month");
                String r_year = request.queryParams("r_year");
                int r_day_i = Integer.parseInt(r_day);
                int r_month_i = Integer.parseInt(r_month);
                int r_year_i = Integer.parseInt(r_year);
                LocalDate fireDate = LocalDate.of(r_year_i,r_month_i,r_day_i);
                OrderDto orderDto = new OrderDto();
                String orderId = request.queryParams("orderid");
                orderDto.id = Integer.parseInt(orderId);
                orderDto.type = Type.FIRE;
                String o_day = request.queryParams("o_day");
                String o_month = request.queryParams("o_month");
                String o_year = request.queryParams("o_year");
                int o_day_i = Integer.parseInt(o_day);
                int o_month_i = Integer.parseInt(o_month);
                int o_year_i = Integer.parseInt(o_year);
                orderDto.date = LocalDate.of(o_year_i,o_month_i,o_day_i);
                service.firePerson(user,fireDate,orderDto,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/editshifttype", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                Integer idPerson = Integer.parseInt(idPerson1);
                response.cookie("/editperson", "idPerson", idPerson1, 10 * 50, false);

                String shiftTyp = request.queryParams("shifttype");
                ShiftType type = null;
                if (shiftTyp.toUpperCase().equals("IN_SHIFTS")) {
                    type = ShiftType.IN_SHIFTS;
                } else {
                    type = ShiftType.NORMAL;
                }
                OrderDto orderDto = new OrderDto();
                String orderId = request.queryParams("orderid");
                orderDto.id = Integer.parseInt(orderId);
                orderDto.type = Type.TRANSFER;
                String o_day = request.queryParams("o_day");
                String o_month = request.queryParams("o_month");
                String o_year = request.queryParams("o_year");
                int o_day_i = Integer.parseInt(o_day);
                int o_month_i = Integer.parseInt(o_month);
                int o_year_i = Integer.parseInt(o_year);
                orderDto.date = LocalDate.of(o_year_i,o_month_i,o_day_i);
                service.updateShiftType(user,type,orderDto,idPerson);
            }catch (Exception ignored){
            }
            response.redirect("/editperson");
            return "success";
        });

        get("/vacation", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");

            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");

            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();

            try {

                model.put("user", user.role.toString());
                if (user.role == Role.CHIEF) {
                    model.put("idDept", user.idDept);
                }

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"vacation.ftl"));
        });

        get("/gettotalgraph", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");

            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");

            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            List<List> totalgraph = new ArrayList<>();

            try {

                model.put("user", user.role.toString());
                if (user.role == Role.CHIEF) {
                    model.put("idDept", user.idDept);
                }

                String year1 = request.queryParams("year");
                Integer year = Integer.parseInt(year1);
                model.put("year",year1);
              //  List<TotalVacationGraphDto> totalVacationGraphDtoList = service.getAllTotalVacationGraph(user);
                TotalVacationGraphDto totalVacationGraphDto = service.getTotalVacationGraphByYear(user,year);
             //   for (TotalVacationGraphDto totalVacationGraphDto:totalVacationGraphDtoList) {
                    for (DepartmentVacationDto departmentVacationDto:totalVacationGraphDto.vacations) {
                        for (VacationDto vacationDto:departmentVacationDto.vacationList) {
                            PersonDto personDto = service.getPerson(user,vacationDto.employee);
                            totalgraph.add(Arrays.asList(totalVacationGraphDto.year.toString(),departmentVacationDto.departament.toString(),
                                    personDto.lastName,personDto.firstName,personDto.middleName,
                                    personDto.idPerson,vacationDto.startDay,vacationDto.endDay));
                        }
                    }

                model.put("totalgraph",totalgraph);


            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"totalvacgraph.ftl"));
        });

        get("/createdepvac", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");

            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");

            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            List<List> personList = new ArrayList<>();
            List<List> personsvacList = new ArrayList<>();

            try {

                List<PersonDto> personDtoList = null;
                List<VacationDto> vacationDtoList = null;
                String idDept = request.queryParams("idDept");
                if (idDept == null) {
                    idDept = request.cookie("idDept");
                }
                model.put("idDept",idDept);
                if (user.role == Role.CHIEF) {
                    personDtoList = service.getSubordinatePersonList(user, user.idDept);
                } else {
                   Integer idDept1 = Integer.parseInt(idDept);
                   personDtoList = service.getSubordinatePersonList(user, idDept1);
                }
                model.put("user", user.role.toString());
                String year = request.queryParams("year");
                if (year == null) {
                    year = request.cookie("year");
                }
                model.put("year",year);
                response.cookie("/addvacation", "year", year, 10 * 50, false);
                response.cookie("/addvacation", "idDept", idDept, 10 * 50, false);
                for (PersonDto personDto : personDtoList) {
                    if (personDto.fireDate == null) {
                        personList.add(Arrays.asList(personDto.idPerson,   personDto.lastName, personDto.firstName, personDto.middleName,
                                personDto.title));
                        vacationDtoList = service.getAllVacationOfPerson(user,personDto.idPerson);
                        for (VacationDto vacationDto: vacationDtoList) {
                            if (vacationDto.endDay.getYear() == Integer.parseInt(year)) {
                                personsvacList.add(Arrays.asList(personDto.idPerson, personDto.lastName, personDto.firstName, personDto.middleName,
                                        vacationDto.startDay, vacationDto.endDay));
                            }
                        }
                    }
                }
                model.put("persons", personList);
                model.put("personsvac",personsvacList);
                boolean isapproved = false;
                boolean depvacex = false;
                DepartmentVacationDto departmentVacationDto = service.getDepartmentVacationByYearId(user,Integer.parseInt(idDept),Integer.parseInt(year));
               // List<List> depvac = new ArrayList<>();
                if (departmentVacationDto!=null) {
                    depvacex = true;
                    if (departmentVacationDto.approved) {
                        isapproved=true;
                    } else {
                        isapproved = false;
                    }
                } else {
                    depvacex = false;
                    isapproved = false;
                }
                model.put("depvacex",depvacex);
                model.put("isapproved", isapproved);
                //model.put("depvac", depvac);


            }catch (Exception ignored){

            }
            return freeMarkerEngine.render(new ModelAndView(model,"createdepvac.ftl"));
        });

        get("/addingvacation", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                Integer idPerson = Integer.parseInt(idPerson1);
                String s_day = request.queryParams("s_day");
                String s_month = request.queryParams("s_month");
                String year = request.cookie("year");
                int s_day_i = Integer.parseInt(s_day);
                int s_month_i = Integer.parseInt(s_month);
                int year_i = Integer.parseInt(year);
                LocalDate startDate = LocalDate.of(year_i,s_month_i,s_day_i);
                String e_day = request.queryParams("e_day");
                String e_month = request.queryParams("e_month");
                int e_day_i = Integer.parseInt(e_day);
                int e_month_i = Integer.parseInt(e_month);
                LocalDate endDate = LocalDate.of(year_i,e_month_i,e_day_i);
                VacationDto vacationDto = new VacationDto();
                vacationDto.employee = idPerson;
                vacationDto.startDay = startDate;
                vacationDto.endDay = endDate;
               // response.cookie("/addvacation","year", request.cookie("year"), 10*50,false);
               // response.cookie("/addvacation","idDept", request.cookie("idDept"), 10*50,false);
                response.cookie("/addingvacation", "year", request.cookie("year"), 10 * 50, false);

                service.addVacation(user,vacationDto);
            }catch (Exception ignored){
            }
            response.redirect("/addvacation");
            return "success";
        });

        get("/addvacation", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            try {
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                List<List> vacationList = new ArrayList<>();
                List<VacationDto> vacationDtoList = service.getAllVacationOfPerson(user,Integer.parseInt(idPerson1));
                PersonDto personDto = service.getPerson(user, Integer.parseInt(idPerson1));
                for (VacationDto vacationDto : vacationDtoList) {
                    if (vacationDto.startDay.getYear() == Integer.parseInt(request.cookie("year"))) {
                        vacationList.add(Arrays.asList(personDto.idPerson, personDto.lastName, personDto.firstName, personDto.middleName,
                                vacationDto.startDay, vacationDto.endDay));
                    }
                }
                model.put("vacations",vacationList);
                model.put("user", user.role.toString());
                model.put("idPerson", idPerson1);
                model.put("year",request.cookie("year"));
                model.put("idDept",request.cookie("idDept"));
                response.cookie("/addvacation", "idPerson", idPerson1, 10 * 50, false, true);
                response.cookie("/addvacation", "year", request.cookie("year"), 10 * 50, false);
                response.cookie("/addvacation", "idDept", request.cookie("idDept"), 10 * 50, false);
                response.cookie("/addingvacation", "year", request.cookie("year"), 10 * 50, false);


            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"addvacation.ftl"));
        });

        get("/adddepvac", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String year = request.queryParams("year");
                String idDept = request.queryParams("idDept");

                DepartmentVacationDto departmentVacationDto = new DepartmentVacationDto();
                departmentVacationDto.departament = Integer.parseInt(idDept);
                departmentVacationDto.year = Integer.parseInt(year);
                List <VacationDto> vacations = new ArrayList<>();
                List<PersonDto> personDtoList = service.getSubordinatePersonList(user, Integer.parseInt(idDept));
                for (PersonDto personDto : personDtoList) {
                    List<VacationDto> vacationDtoList = service.getAllVacationOfPerson(user, personDto.idPerson);
                    for (VacationDto vacationDto : vacationDtoList) {
                        if (vacationDto.startDay.getYear() == Integer.parseInt(year)) {
                            vacations.add(vacationDto);
                        }
                    }
                }
                departmentVacationDto.vacationList=vacations;
                service.addDepartmentVacation(user,departmentVacationDto);

                response.cookie("/createdepvac", "idDept", idDept, 10 * 50, false);
                response.cookie("/createdepvac", "year", year, 10 * 50, false);
            }catch (Exception e){
                e.printStackTrace();
            }
            response.redirect("/createdepvac");
            return "success";
        });

        get("/approve", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String year = request.queryParams("year");
                String idDept = request.queryParams("idDept");

                DepartmentVacationDto departmentVacationDto = service.getDepartmentVacationByYearId(user,Integer.parseInt(idDept),Integer.parseInt(year));
                service.updateDepartmentVacation(user,departmentVacationDto,authController.getIdByLogin(user));

                response.cookie("/createdepvac", "idDept", idDept, 10 * 50, false);
                response.cookie("/createdepvac", "year", year, 10 * 50, false);

            }catch (Exception e){
                e.printStackTrace();
            }
            response.redirect("/createdepvac");
            return "success";
        });

        get("/createtotalgraph", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String year = request.queryParams("year");

                service.createTotalVacationGraph(user, PositionService.findAllDepartment(),Integer.parseInt(year));

            }catch (Exception e){
                e.printStackTrace();
            }
            response.redirect("/vacation");
            return "success";
        });

        get("/shifts", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            List<List> personList = new ArrayList<>();
            try {
                List<PersonDto> personDtoList = null;
                if (user.role == Role.CHIEF) {
                    personDtoList = service.getSubordinatePersonList(user, user.idDept);
                } else {
                    personDtoList = service.getPersonList(user);
                }
                model.put("user", user.role.toString());

                for (PersonDto personDto : personDtoList) {
                    if (personDto.fireDate == null) {
                        personList.add(Arrays.asList(personDto.idPerson,   personDto.lastName, personDto.firstName, personDto.middleName,
                                personDto.title,personDto.shiftType));
                    }
                }
                model.put("persons", personList);
            }catch (Exception ignored){
            }
            return freeMarkerEngine.render(new ModelAndView(model,"shifts.ftl"));
        });

        get("/addshift", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            try {
                String idPerson1 = request.queryParams("idPerson");
                if (idPerson1 == null) {
                    idPerson1 = request.cookie("idPerson");
                }
                model.put("user", user.role.toString());
                model.put("idPerson", idPerson1);
                List<List> shiftList = new ArrayList<>();
                PersonDto personDto = service.getPerson(user, Integer.parseInt(idPerson1));
                List<ShiftDto> shiftDtoList = service.getAllPersonShift(user,Integer.parseInt(idPerson1));
                for (ShiftDto shiftDto : shiftDtoList) {
                    shiftList.add(Arrays.asList(personDto.idPerson,personDto.lastName,personDto.firstName,personDto.middleName,shiftDto.date));
                }
                model.put("shifts",shiftList);
                response.cookie("/addshift", "idPerson", idPerson1, 10 * 50, false, true);

            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"addshift.ftl"));
        });

        get("/addingshift", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
                Service service = new ServiceImpl(dao,user);
                String idPerson1 = request.queryParams("idPerson");
                Integer idPerson = Integer.parseInt(idPerson1);
                String day = request.queryParams("day");
                String month = request.queryParams("month");
                String year = request.queryParams("year");

                int day_i = Integer.parseInt(day);
                int month_i = Integer.parseInt(month);
                int year_i = Integer.parseInt(year);
                LocalDate date = LocalDate.of(year_i,month_i,day_i);
                ShiftDto shiftDto = new ShiftDto();
                shiftDto.date = date;
                shiftDto.actualAmountEmployee=0;
                shiftDto.limitOfShift = 1;
                if (user.role == Role.CHIEF) {
                    service.addChiefShift(user,idPerson,shiftDto,user.idDept);
                } else {
                    service.addHrShift(user,idPerson,shiftDto);
                }
            }catch (Exception ignored){
            }
            response.redirect("/addshift");
            return "success";
        });

    }

}
