import PersonnelDao.PersonnelDao;
import PersonnelDao.PersonnelDaoStub;
import controllers.ServerController;
import dao.Dao;
import dao.DaoImpl;
import dao.DaoStub;


public class RestApplication {

   // private static ALPNProcessor BasicConfigurator;
    private static Object Hashing;

    public static void main(String[] args) {
        startServer();
    }

    public static void startServer() {
        Dao dao = new DaoImpl();
        PersonnelDao personnelDao = new PersonnelDaoStub();
        ServerController serverController = new ServerController(dao,personnelDao);
        serverController.run();
    }
}

