package integration_tests;

import PersonnelDao.User;
import dao.Dao;
import dao.DaoStub;
import dto.OrderDto;
import dto.PersonDto;
import exceptions.EntitySavingException;
import exceptions.InvalidCredentialsException;
import exceptions.InvalidDataException;
import exceptions.InvalidSessionException;
import javafx.util.Pair;
import models.Order;
import models.Person;
import models.ShiftType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.PersonService;
import service.Service;
import service.ServiceImpl;
import service.classes.Action;
import service.classes.Role;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PersonAPITest {
    private Service service;

    private User managerHRUuid = new User();
    private User secondHRManagerUuid = new User();
    private User chiefUuid = new User();
    private User userUuid = new User();
    private User adminUuid = new User();

    @BeforeEach
    public void init() throws InvalidCredentialsException {
        Dao dao = new DaoStub();
        this.service = new ServiceImpl(dao);

        String managerHRLogin = "firstHRManager";
        //  String secondHRManagerLogin = "secondHRManager";
        String chiefLogin = "chief";
        String userLogin = "user";
        String adminLogin = "admin";
        String password = "password";

        this.managerHRUuid.login = managerHRLogin;
        this.managerHRUuid.role = Role.HR;

        this.userUuid.login = userLogin;
        this.userUuid.role = Role.USER;

        this.chiefUuid.login = chiefLogin;
        this.chiefUuid.role = Role.CHIEF;

        this.adminUuid.login = adminLogin;
        this.adminUuid.role = Role.ADMIN;
    }

    @Test
    public void testRentPersonSuccess() throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException, InvalidCredentialsException {

        //  this.init();
      //  this.service.checkAccessBySessionUuid(Action.INFORMATION_CREATE, managerHRUuid);

        String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "123456123456";
        Integer title = 1;
        LocalDate rentDate = LocalDate.now();
        Order order1 = Order.createHireOrder(1, rentDate);
        OrderDto order = new OrderDto();
        order.date = order1.getDate();
        order.type = order1.getType();
        order.id = order1.getId();
        ShiftType type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
        PersonDto chosenPerson = personList.get(0);

        assertNotNull(this.service.getPerson(managerHRUuid, INN));
        assertEquals(this.service.getPerson(managerHRUuid, INN).lastName, lastName);
    }

    @Test
    public void testUpdateInfoPersonSuccess() throws Exception {

        //     this.init();
    //    this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

        String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "123456123456";
        Integer title = 1;
        LocalDate rentDate = LocalDate.now();
        Order order11 = Order.createHireOrder(1, rentDate);
        OrderDto order = new OrderDto();
        order.date = order11.getDate();
        order.type = order11.getType();
        order.id = order11.getId();
        ShiftType type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        //List<Person> personList = this.service.getPersonList(managerHRUuid);
        //Person chosenPerson = personList.get(0);

        firstName = "Bondarev1";
        lastName = "Alexander1";
        middleName = "Vladimirovich1";
        //  LocalDate birthday = LocalDate.of(1999,5,6);
        title = 2;
        rentDate = LocalDate.now();
        Order order12 = Order.createTransferOrder(2, rentDate);
        OrderDto order1 = new OrderDto();
        order1.date = order12.getDate();
        order1.type = order12.getType();
        order1.id = order12.getId();
        Order order21 = Order.createTransferOrder(3, rentDate);
        OrderDto order2 = new OrderDto();
        order2.date = order21.getDate();
        order2.type = order21.getType();
        order2.id = order21.getId();
        type = ShiftType.IN_SHIFTS;
        //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
        List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
        PersonDto chosenPerson = personList.get(0);
        chosenPerson.idPerson=0;

        this.service.updateFirstName(managerHRUuid, firstName, chosenPerson.idPerson);
        this.service.updateLastName(managerHRUuid, lastName, chosenPerson.idPerson);
        this.service.updateMiddleName(managerHRUuid, middleName, chosenPerson.idPerson);
        this.service.updateShiftType(managerHRUuid, type, order1, chosenPerson.idPerson);
        this.service.updateTitle(managerHRUuid, title, order2, chosenPerson.idPerson);

        assertNotNull(this.service.getPerson(managerHRUuid, INN));
        assertEquals(this.service.getPerson(managerHRUuid, INN).firstName, firstName);
        assertEquals(this.service.getPerson(managerHRUuid, INN).lastName, lastName);
        assertEquals(this.service.getPerson(managerHRUuid, INN).middleName, middleName);
        List<OrderDto> ord = this.service.getAllPersonOrders(managerHRUuid,this.service.getPerson(managerHRUuid, INN).idPerson);
        assertEquals(this.service.getAllPersonOrders(managerHRUuid,this.service.getPerson(managerHRUuid, INN).idPerson).get(1).type, order1.type);
        assertEquals(this.service.getAllPersonOrders(managerHRUuid,this.service.getPerson(managerHRUuid, INN).idPerson).get(2).type, order2.type);
        assertEquals(this.service.getPerson(managerHRUuid, INN).title, title);
    }

    @Test
    public void testFirePersonSuccess() throws Exception {

        //     this.init();
        this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

        String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "123456123456";
        Integer title = 1;
        LocalDate rentDate = LocalDate.now();
        Order order11 = Order.createHireOrder(1, rentDate);
        OrderDto order = new OrderDto();
        order.date = order11.getDate();
        order.type = order11.getType();
        order.id = order11.getId();
        ShiftType type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
        PersonDto chosenPerson = personList.get(0);

        Order order21 = Order.createFireOrder(2, rentDate.plusDays(20));
        OrderDto order1 = new OrderDto();
        order1.date = order21.getDate();
        order1.type = order21.getType();
        order1.id = order21.getId();
        this.service.firePerson(managerHRUuid, rentDate.plusDays(20), order1, chosenPerson.idPerson);

        assertNotNull(this.service.getPerson(managerHRUuid, INN));
        assertEquals(this.service.getPerson(managerHRUuid, INN).fireDate, rentDate.plusDays(20));
        assertEquals(this.service.getAllPersonOrders(managerHRUuid,this.service.getPerson(managerHRUuid, INN).idPerson).get(1).type, order1.type);
    }

    @Test
    public void testRentPersonFailedCheckAccess() throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        assertThrows(AccessDeniedException.class, () -> {
            User invalidUuid = new User();
            invalidUuid.role = null;
           // this.service.checkAccessBySessionUuid(Action.INFORMATION_CREATE, invalidUuid);

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order1 = Order.createHireOrder(1, rentDate);
            OrderDto order = new OrderDto();
            order.date = order1.getDate();
            order.type = order1.getType();
            order.id = order1.getId();
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(invalidUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        });
    }


    @Test
    public void testRentPersonWithAccessDeniedWhenSave() throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        assertThrows(AccessDeniedException.class, () -> {
           // this.service.checkAccessBySessionUuid(Action.INFORMATION_CREATE, managerHRUuid);

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order1 = Order.createHireOrder(1, rentDate);
            OrderDto order = new OrderDto();
            order.date = order1.getDate();
            order.type = order1.getType();
            order.id = order1.getId();
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(userUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        });
    }


    @Test
    public void testRentPersonWithInvalidDataWhenSave() throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        assertThrows(InvalidDataException.class, () -> {
            //this.service.checkAccessBySessionUuid(Action.INFORMATION_CREATE, managerHRUuid);

            String firstName = "";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order1 = Order.createHireOrder(1, rentDate);
            OrderDto order = new OrderDto();
            order.date = order1.getDate();
            order.type = order1.getType();
            order.id = order1.getId();
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        });
    }

    @Test
    public void testRentPersonWhoAlreadyRented() throws AccessDeniedException, InvalidSessionException, InvalidDataException, EntitySavingException {
        assertThrows(EntitySavingException.class, () -> {
         //   this.service.checkAccessBySessionUuid(Action.INFORMATION_CREATE, managerHRUuid);

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order1 = Order.createHireOrder(1, rentDate);
            OrderDto order = new OrderDto();
            order.date = order1.getDate();
            order.type = order1.getType();
            order.id = order1.getId();
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);

            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
        });
    }

    @Test
    public void testFireFailedCheckAccess() throws InvalidSessionException, AccessDeniedException, InvalidDataException, EntitySavingException {
        assertThrows(AccessDeniedException.class, () -> {
            User invalidUuid = new User();
            invalidUuid.role = null;

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order11 = Order.createHireOrder(1, rentDate);
            OrderDto order = new OrderDto();
            order.date = order11.getDate();
            order.type = order11.getType();
            order.id = order11.getId();
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, order, type);
            LocalDate fireDate = rentDate.plusDays(20);
            Order order21 = Order.createFireOrder(2, fireDate);
            OrderDto order1 = new OrderDto();
            order1.date = order21.getDate();
            order1.type = order21.getType();
            order1.id = order21.getId();
            //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
            List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
            PersonDto chosenPerson = personList.get(0);
           // this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, invalidUuid);

            this.service.firePerson(invalidUuid, fireDate, order1, chosenPerson.idPerson);
        });
    }


    private OrderDto createOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.date = order.getDate();
        orderDto.id = order.getId();
        orderDto.type = order.getType();
        return orderDto;
    }

    @Test
    public void testFireAccessDeniedWhenUpdate() throws InvalidSessionException, AccessDeniedException, InvalidDataException, EntitySavingException {
        assertThrows(AccessDeniedException.class, () -> {
            User invalidUuid = new User();
            invalidUuid.role = null;

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order = Order.createHireOrder(1, rentDate);
            OrderDto hireOrder = createOrderDto(order);
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, hireOrder, type);
            LocalDate fireDate = rentDate.plusDays(20);
            Order order1 = Order.createFireOrder(2, fireDate);
            OrderDto fireOrder = createOrderDto(order1);
            //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
            List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
            PersonDto chosenPerson = personList.get(0);
            this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

            this.service.firePerson(userUuid, fireDate, fireOrder, chosenPerson.idPerson);
        });
    }

    @Test
    public void testFireInvalidDataWhenSave() throws InvalidSessionException, AccessDeniedException, InvalidDataException, EntitySavingException {
        assertThrows(InvalidDataException.class, () -> {
            User invalidUuid = new User();
            invalidUuid.role = null;

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order = Order.createHireOrder(1, rentDate);
            OrderDto hireOrder = createOrderDto(order);
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title,
                    rentDate, hireOrder, type);
            LocalDate fireDate = rentDate.plusDays(20);
            Order order1 = Order.createFireOrder(2, fireDate);
            OrderDto fireOrder = createOrderDto(order1);
            //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
            List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
            PersonDto chosenPerson = personList.get(0);
            this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

            this.service.firePerson(managerHRUuid, null, fireOrder, chosenPerson.idPerson);
        });
    }


    @Test
    public void testUpdateInfoAccessDeniedWhenUpdate() throws InvalidSessionException, AccessDeniedException, InvalidDataException, EntitySavingException {
        assertThrows(AccessDeniedException.class, () -> {
            String invalidUuid = "invalidUuid";

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order = Order.createHireOrder(1, rentDate);
            OrderDto hireOrder = createOrderDto(order);
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title,
                    rentDate, hireOrder, type);
            firstName = "Bondarev1";
            lastName = "Alexander1";
            middleName = "Vladimirovich1";
            //  LocalDate birthday = LocalDate.of(1999,5,6);
            title = 2;
            rentDate = LocalDate.now();
            type = ShiftType.IN_SHIFTS;
            //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
            List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
            PersonDto chosenPerson = personList.get(0);
            this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

            this.service.updateFirstName(userUuid, firstName, chosenPerson.idPerson);
        });
    }

    @Test
    public void testUpdateInfoInvalidDataWhenSave() throws InvalidSessionException, AccessDeniedException, InvalidDataException, EntitySavingException {
        assertThrows(InvalidDataException.class, () -> {
            String invalidUuid = "invalidUuid";

            String firstName = "Bondarev";
            String lastName = "Alexander";
            String middleName = "Vladimirovich";
            LocalDate birthday = LocalDate.of(1999, 5, 6);
            String INN = "123456123456";
            Integer title = 1;
            LocalDate rentDate = LocalDate.now();
            Order order = Order.createHireOrder(1, rentDate);
            OrderDto orderDto = createOrderDto(order);
            ShiftType type = ShiftType.NORMAL;
            this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title,
                    rentDate, orderDto, type);
            firstName = "";
            lastName = "Alexander1";
            middleName = "Vladimirovich1";
            //  LocalDate birthday = LocalDate.of(1999,5,6);
            title = 2;
            rentDate = LocalDate.now();

            type = ShiftType.IN_SHIFTS;
            //this.service.rentPerson(managerHRUuid,firstName,lastName,middleName,birthday, INN, title, rentDate, order, type);
            List<PersonDto> personList = this.service.getPersonList(managerHRUuid);
            PersonDto chosenPerson = personList.get(0);
            this.service.checkAccessBySessionUuid(Action.INFORMATION_UPDATE, managerHRUuid);

            this.service.updateFirstName(managerHRUuid, firstName, chosenPerson.idPerson);
        });
    }


}
