package integration_tests;

import PersonnelDao.User;
import dao.Dao;
import dao.DaoStub;
import dto.*;
import exceptions.*;
import models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.Service;
import service.ServiceImpl;
import service.classes.Action;
import service.classes.Role;
import service.mock.PositionService;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class VacationAPITest {

    private Service service;

    private Dao dao = new DaoStub();
    private User managerHRUuid = new User();
    private User secondHRManagerUuid = new User();
    private User chiefUuid = new User();
    private User userUuid = new User();
    private User adminUuid = new User();

    @BeforeEach
    public void init() throws InvalidCredentialsException, AccessDeniedException, InvalidDataException, InvalidSessionException, EntitySavingException {
        Dao dao = new DaoStub();
        this.service = new ServiceImpl(dao);

        String managerHRLogin = "firstHRManager";
        //  String secondHRManagerLogin = "secondHRManager";
        String chiefLogin = "chief";
        String userLogin = "user";
        String adminLogin = "admin";
        String password = "password";

        this.managerHRUuid.login = managerHRLogin;
        this.managerHRUuid.role = Role.HR;

        this.userUuid.login = userLogin;
        this.userUuid.role = Role.USER;

        this.chiefUuid.login = chiefLogin;
        this.chiefUuid.role = Role.CHIEF;

        this.adminUuid.login = adminLogin;
        this.adminUuid.role = Role.ADMIN;

        String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "123456123451";
        Integer title = 1;
        LocalDate rentDate = LocalDate.of(2017,10,10);
        Order order = Order.createHireOrder(1, rentDate);
        OrderDto orderDto = createOrderDto(order);
        ShiftType type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title, rentDate, orderDto, type);

        firstName = "Bondarev1";
        lastName = "Alexander1";
        middleName = "Vladimirovich1";
        birthday = LocalDate.of(1999, 5, 6);
        String INN1 = "123456123456";
        title = 4;

        order = Order.createHireOrder(2, rentDate);
        orderDto = createOrderDto(order);
        type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN1, title, rentDate, orderDto, type);

    }

    @Test
    public void testPositiveAddVacation () throws AccessDeniedException, InvalidSessionException, EntitySavingException {
        this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
        Vacation vac1 = new Vacation(LocalDate.of(2020,1,13), LocalDate.of(2020,1,26),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac2 = new Vacation(LocalDate.of(2020,2,17), LocalDate.of(2020,2,23),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac3 = new Vacation(LocalDate.of(2020,3,23), LocalDate.of(2020,3,29),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        List<Vacation> depVac = new ArrayList<>();
        depVac.add(vac1);
        depVac.add(vac2);
        depVac.add(vac3);
        DepartmentVacation departmentVacation = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                depVac,2020);

        DepartmentVacationDto departmentVacationDto = createDepartmentVacationDto(departmentVacation);

        this.service.addDepartmentVacation(chiefUuid, departmentVacationDto);

        DepartmentVacationDto res = this.service.getDepartmentVacationByYearId(chiefUuid,1,
               2020);

        assertNotNull(res);
        assertEquals(res.vacationList.get(0).employee,depVac.get(0).getEmployee().getIdPerson());
        assertEquals(res.vacationList.get(0).startDay,depVac.get(0).getStartDay());

        List<VacationDto> tmp = this.service.getAllVacationOfPerson(chiefUuid, this.service.getPerson(chiefUuid, "123456123451").idPerson);
        assertEquals(tmp.get(0).startDay,vac1.getStartDay());
    }

    @Test
    public void testPositiveCreateTotalVacationGraph () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException, EntitySavingException {
        this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
        Vacation vac1 = new Vacation(LocalDate.of(2020,1,13), LocalDate.of(2020,1,26),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac2 = new Vacation(LocalDate.of(2020,2,17), LocalDate.of(2020,2,23),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac3 = new Vacation(LocalDate.of(2020,3,23), LocalDate.of(2020,3,29),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac21 = new Vacation(LocalDate.of(2020,1,13), LocalDate.of(2020,1,26),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac22 = new Vacation(LocalDate.of(2020,2,17), LocalDate.of(2020,2,23),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac23 = new Vacation(LocalDate.of(2020,3,23), LocalDate.of(2020,3,29),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        List<Vacation> depVac = new ArrayList<>();
        depVac.add(vac1);
        depVac.add(vac2);
        depVac.add(vac3);
        DepartmentVacation departmentVacation = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                depVac,2020);
        List<Vacation> depVac1 = new ArrayList<>();
        depVac1.add(vac21);
        depVac1.add(vac22);
        depVac1.add(vac23);
        DepartmentVacation departmentVacation1 = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                depVac1,2020);

        DepartmentVacationDto depVacDto1 = createDepartmentVacationDto(departmentVacation);
        DepartmentVacationDto depVacDto2 = createDepartmentVacationDto(departmentVacation1);
        this.service.addDepartmentVacation(chiefUuid, depVacDto1);
        this.service.addDepartmentVacation(chiefUuid, depVacDto2);

   //     departmentVacation.approve(1);
   //     departmentVacation1.approve(1);

        this.service.updateDepartmentVacation(chiefUuid,depVacDto1,1);
        this.service.updateDepartmentVacation(chiefUuid,depVacDto2,1);

        DepartmentVacationDto res = this.service.getDepartmentVacationByYearId(chiefUuid,1,
                2020);

        assertNotNull(res);
        assertEquals(res.vacationList.get(0).employee,depVac.get(0).getEmployee().getIdPerson());
        assertEquals(res.vacationList.get(0).startDay,depVac.get(0).getStartDay());

        res = this.service.getDepartmentVacationByYearId(chiefUuid,2,
                2020);

        assertNotNull(res);
        assertEquals(res.vacationList.get(0).employee,depVac1.get(0).getEmployee().getIdPerson());
        assertEquals(res.vacationList.get(0).startDay,depVac1.get(0).getStartDay());


        List<DepartmentVacation> depVacList = new ArrayList<>();
        for (DepartmentVacationDto departmentVacationDto:this.service.getDepartmentVacationsByYear(chiefUuid, 2020)) {
            depVacList.add(createDepartmentVacation(departmentVacationDto));
        }
        TotalVacationGraph totalVacationGraph = new TotalVacationGraph(2020,
                depVacList);

        this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2020);

        assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid,2020));
        assertEquals(this.service.getTotalVacationGraphByYear(chiefUuid,2020).vacations.get(0).idHREmpoloyee,
                totalVacationGraph.getVacations().get(0).getIdHREmpoloyee());

    }

    @Test
    public void testNegativeCreateTotalVacationGraphWrongDays () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        assertThrows(WrongDeptVacationException.class, () ->
        {this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
        Vacation vac1 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 28),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac2 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac3 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac21 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 28),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac22 = new Vacation(LocalDate.of(2020, 2, 10), LocalDate.of(2020, 2, 23),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac23 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        List<Vacation> depVac = new ArrayList<>();
        depVac.add(vac1);
        depVac.add(vac2);
        depVac.add(vac3);
        DepartmentVacationDto departmentVacation = createDepartmentVacationDto( new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                depVac, 2020));
        List<Vacation> depVac1 = new ArrayList<>();
        depVac1.add(vac21);
        depVac1.add(vac22);
        depVac1.add(vac23);
        DepartmentVacationDto departmentVacation1 = createDepartmentVacationDto(new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                depVac1, 2020));

        this.service.addDepartmentVacation(chiefUuid, departmentVacation);
        this.service.addDepartmentVacation(chiefUuid, departmentVacation1);

        //   departmentVacation.approve(1);
        //   departmentVacation1.approve(1);

        this.service.updateDepartmentVacation(chiefUuid, departmentVacation, 1);
        this.service.updateDepartmentVacation(chiefUuid, departmentVacation1, 1);

        DepartmentVacationDto res = this.service.getDepartmentVacationByYearId(chiefUuid, 1,
                2020);

            assertNotNull(res);
            assertEquals(res.vacationList.get(0).employee,depVac.get(0).getEmployee().getIdPerson());
            assertEquals(res.vacationList.get(0).startDay,depVac.get(0).getStartDay());


        res = this.service.getDepartmentVacationByYearId(chiefUuid, 2,
                2020);

            assertNotNull(res);
            assertEquals(res.vacationList.get(0).employee,depVac1.get(0).getEmployee().getIdPerson());
            assertEquals(res.vacationList.get(0).startDay,depVac1.get(0).getStartDay());

            List<DepartmentVacation> depVacList = new ArrayList<>();
            for (DepartmentVacationDto departmentVacationDto:this.service.getDepartmentVacationsByYear(chiefUuid, 2020)) {
                depVacList.add(createDepartmentVacation(departmentVacationDto));
            }
            TotalVacationGraph totalVacationGraph = new TotalVacationGraph(2020,
                    depVacList);

        this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2020);

            assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid,2020));
            assertEquals(this.service.getTotalVacationGraphByYear(chiefUuid,2020).vacations.get(0).idHREmpoloyee,
                    totalVacationGraph.getVacations().get(0).getIdHREmpoloyee());
    });
    }

    @Test
    public void testNegativeCreateTotalVacationGraph () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        assertThrows(WrongDeptVacationException.class, () ->
        {
            this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
            Vacation vac1 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 26),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac2 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac3 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac21 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 26),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac22 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac23 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            List<Vacation> depVac = new ArrayList<>();
            depVac.add(vac1);
            depVac.add(vac2);
            depVac.add(vac3);
            DepartmentVacationDto departmentVacation = createDepartmentVacationDto(new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                    depVac, 2020));
            List<Vacation> depVac1 = new ArrayList<>();
            depVac1.add(vac21);
            depVac1.add(vac22);
            depVac1.add(vac23);
            DepartmentVacationDto departmentVacation1 = createDepartmentVacationDto(new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                    depVac1, 2020));

            this.service.addDepartmentVacation(chiefUuid, departmentVacation);
            this.service.addDepartmentVacation(chiefUuid, departmentVacation1);

            this.service.updateDepartmentVacation(chiefUuid, departmentVacation, 1);
            //  this.service.updateDepartmentVacation(chiefUuid,departmentVacation1,1);

            DepartmentVacationDto res = this.service.getDepartmentVacationByYearId(chiefUuid, 1,
                    2020);

            res = this.service.getDepartmentVacationByYearId(chiefUuid, 2,
                    2020);

            this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(),
                    2020);
        });
    }


    @Test
    public void testNegativeCreateTotalVacationGraphNotAllDepartment () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        assertThrows(WrongDeptVacationException.class, () ->
        {
            this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
            Vacation vac1 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 26),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac2 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac3 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            List<Vacation> depVac = new ArrayList<>();
            depVac.add(vac1);
            depVac.add(vac2);
            depVac.add(vac3);
            DepartmentVacation departmentVacation = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                    depVac, 2020);
            DepartmentVacationDto departmentVacationDto = createDepartmentVacationDto(departmentVacation);
            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto);
          //  this.service.addDepartmentVacation(chiefUuid, departmentVacation1);

            departmentVacation.approve(1);
            // departmentVacation1.approve(1);

            this.service.updateDepartmentVacation(chiefUuid, departmentVacationDto, 1);
            //  this.service.updateDepartmentVacation(chiefUuid,departmentVacation1,1);

            this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2020);
        });
    }

    @Test
    public void testPositiveCreateTotalVacationGraphInTwoYears () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException, EntitySavingException {
        this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
        Vacation vac1 = new Vacation(LocalDate.of(2020,1,13), LocalDate.of(2020,1,25),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac2 = new Vacation(LocalDate.of(2020,2,17), LocalDate.of(2020,2,23),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac3 = new Vacation(LocalDate.of(2020,3,23), LocalDate.of(2020,3,29),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        Vacation vac21 = new Vacation(LocalDate.of(2020,1,13), LocalDate.of(2020,1,25),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac22 = new Vacation(LocalDate.of(2020,2,17), LocalDate.of(2020,2,23),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        Vacation vac23 = new Vacation(LocalDate.of(2020,3,23), LocalDate.of(2020,3,29),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        List<Vacation> depVac = new ArrayList<>();
        depVac.add(vac1);
        depVac.add(vac2);
        depVac.add(vac3);
        DepartmentVacation departmentVacation = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                depVac,2020);
        DepartmentVacationDto departmentVacationDto = createDepartmentVacationDto(departmentVacation);
        List<Vacation> depVac1 = new ArrayList<>();
        depVac1.add(vac21);
        depVac1.add(vac22);
        depVac1.add(vac23);
        DepartmentVacation departmentVacation1 = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                depVac1,2020);
        DepartmentVacationDto departmentVacationDto1 = createDepartmentVacationDto(departmentVacation1);
        List<Vacation> depVac1_2019 = new ArrayList<>();
        Vacation vac1_2019 = new Vacation(LocalDate.of(2019,1,1), LocalDate.of(2019,1,2),
                createPerson(this.service.getPerson(chiefUuid, "123456123451")));
        depVac1_2019.add(vac1_2019);

        List<Vacation> depVac2_2019 = new ArrayList<>();
        Vacation vac2_2019 = new Vacation(LocalDate.of(2019,1,1), LocalDate.of(2019,1,2),
                createPerson(this.service.getPerson(chiefUuid, "123456123456")));
        depVac2_2019.add(vac2_2019);

        DepartmentVacation departmentVacation2_2019 = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                depVac2_2019,2019);
        DepartmentVacationDto departmentVacationDto2_2019 = createDepartmentVacationDto(departmentVacation2_2019);
        DepartmentVacation departmentVacation1_2019 = new DepartmentVacation(
                PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                depVac1_2019,2019);
        DepartmentVacationDto departmentVacationDto1_2019 = createDepartmentVacationDto(departmentVacation1_2019);
        //     departmentVacation.approve(1);
        //     departmentVacation1.approve(1);

        this.service.addDepartmentVacation(chiefUuid, departmentVacationDto1_2019);
        this.service.addDepartmentVacation(chiefUuid, departmentVacationDto2_2019);

        this.service.addDepartmentVacation(chiefUuid, departmentVacationDto);
        this.service.addDepartmentVacation(chiefUuid,departmentVacationDto1);


        this.service.updateDepartmentVacation(chiefUuid,departmentVacationDto,1);
        this.service.updateDepartmentVacation(chiefUuid,departmentVacationDto1,1);
        this.service.updateDepartmentVacation(chiefUuid,departmentVacationDto1_2019,1);
        this.service.updateDepartmentVacation(chiefUuid,departmentVacationDto2_2019,1);

        DepartmentVacationDto res = this.service.getDepartmentVacationByYearId(chiefUuid,1,
                2020);

        assertNotNull(res);
        assertEquals(res.vacationList.get(0).employee,depVac.get(0).getEmployee().getIdPerson());
        assertEquals(res.vacationList.get(0).startDay,depVac.get(0).getStartDay());


        res = this.service.getDepartmentVacationByYearId(chiefUuid,2,
                2020);

        assertNotNull(res);
        assertEquals(res.vacationList.get(0).employee,depVac1.get(0).getEmployee().getIdPerson());
        assertEquals(res.vacationList.get(0).startDay,depVac1.get(0).getStartDay());


        List<DepartmentVacation> depVacList = new ArrayList<>();
        for (DepartmentVacationDto departmentVacationDt:this.service.getDepartmentVacationsByYear(chiefUuid, 2020)) {
            depVacList.add(createDepartmentVacation(departmentVacationDt));
        }
        TotalVacationGraph totalVacationGraph = new TotalVacationGraph(2020,
                depVacList);

        this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2019);

        this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2020);

        assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid,2020));
        assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid,2019));
        assertEquals(this.service.getTotalVacationGraphByYear(chiefUuid,2020).vacations.get(0).vacationList.get(0).startDay,
                totalVacationGraph.getVacations().get(0).getVacationList().get(0).getStartDay());
    }


    @Test
    public void testNegativeCreateTotalVacationGraphInTwoYears () throws AccessDeniedException, InvalidSessionException, WrongDeptVacationException {
        assertThrows(WrongDeptVacationException.class, () -> {
            this.service.checkAccessBySessionUuid(Action.DEPARTMENT_GRAPH_CREATE, chiefUuid);
            Vacation vac1 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 20),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac2 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac3 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac21 = new Vacation(LocalDate.of(2020, 1, 13), LocalDate.of(2020, 1, 20),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac22 = new Vacation(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac23 = new Vacation(LocalDate.of(2020, 3, 23), LocalDate.of(2020, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            List<Vacation> depVac = new ArrayList<>();
            depVac.add(vac1);
            depVac.add(vac2);
            depVac.add(vac3);
            DepartmentVacation departmentVacation = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                    depVac, 2020);
            List<Vacation> depVac1 = new ArrayList<>();
            depVac1.add(vac21);
            depVac1.add(vac22);
            depVac1.add(vac23);
            DepartmentVacation departmentVacation1 = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                    depVac1, 2020);
            DepartmentVacationDto departmentVacationDto = createDepartmentVacationDto(departmentVacation);
            DepartmentVacationDto departmentVacationDto1 = createDepartmentVacationDto(departmentVacation1);

            List<Vacation> depVac1_2019 = new ArrayList<>();
            Vacation vac1_2019 = new Vacation(LocalDate.of(2019, 1, 13), LocalDate.of(2019, 1, 30),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac2_2019 = new Vacation(LocalDate.of(2019, 2, 17), LocalDate.of(2019, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac3_2019 = new Vacation(LocalDate.of(2019, 3, 23), LocalDate.of(2019, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123451")));
            Vacation vac21_2019 = new Vacation(LocalDate.of(2019, 1, 13), LocalDate.of(2019, 1, 30),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac22_2019 = new Vacation(LocalDate.of(2019, 2, 17), LocalDate.of(2019, 2, 23),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            Vacation vac23_2019 = new Vacation(LocalDate.of(2019, 3, 23), LocalDate.of(2019, 3, 29),
                    createPerson(this.service.getPerson(chiefUuid, "123456123456")));
            depVac1_2019.add(vac1_2019);
            depVac1_2019.add(vac2_2019);
            depVac1_2019.add(vac3_2019);

            List<Vacation> depVac2_2019 = new ArrayList<>();

            depVac2_2019.add(vac21_2019);
            depVac2_2019.add(vac22_2019);
            depVac2_2019.add(vac23_2019);

            DepartmentVacation departmentVacation2_2019 = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123456").title),
                    depVac2_2019, 2019);

            DepartmentVacation departmentVacation1_2019 = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                    depVac1_2019, 2019);

            DepartmentVacationDto departmentVacationDto2_2019 = createDepartmentVacationDto(departmentVacation2_2019);
            DepartmentVacationDto departmentVacationDto1_2019 = createDepartmentVacationDto(departmentVacation1_2019);
            //     departmentVacation.approve(1);
            //     departmentVacation1.approve(1);

            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto1_2019);
            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto2_2019);

            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto);
            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto1);


            this.service.updateDepartmentVacation(chiefUuid, departmentVacationDto, 1);
            this.service.updateDepartmentVacation(chiefUuid, departmentVacationDto1, 1);
            this.service.updateDepartmentVacation(chiefUuid, departmentVacationDto1_2019, 1);
            this.service.updateDepartmentVacation(chiefUuid, departmentVacationDto2_2019, 1);

            this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2019);

            this.service.createTotalVacationGraph(managerHRUuid, PositionService.findAllDepartment(), 2020);

            assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid, 2020));
            assertNotNull(this.service.getTotalVacationGraphByYear(chiefUuid, 2019));
        });
    }



    public VacationDto createVacationDto(Vacation vacation){
        VacationDto vacationDto = new VacationDto();
        vacationDto.startDay = vacation.getStartDay();
        vacationDto.endDay = vacation.getEndDay();
        vacationDto.employee = vacation.getEmployee().getIdPerson();
        return vacationDto;
    }

    public DepartmentVacationDto createDepartmentVacationDto(DepartmentVacation departmentVacation){
        DepartmentVacationDto departmentVacationDto = new DepartmentVacationDto();
        departmentVacationDto.departament = departmentVacation.getDepartament();
        departmentVacationDto.vacationList = new ArrayList<>();
        departmentVacationDto.year = departmentVacation.getYear();
        departmentVacationDto.approved = departmentVacation.isApproved();
        departmentVacationDto.idHREmpoloyee = departmentVacation.getIdHREmpoloyee();
        for (Vacation vacation:departmentVacation.getVacationList()) {
            departmentVacationDto.vacationList.add(createVacationDto(vacation));
        }
        return departmentVacationDto;
    }


    public TotalVacationGraphDto createTotalVacationGraphDto (TotalVacationGraph totalVacationGraph) {
        TotalVacationGraphDto totalVacationGraphDto = new TotalVacationGraphDto();
        totalVacationGraphDto.year = totalVacationGraph.getYear();
        totalVacationGraphDto.vacations = new ArrayList<>();
        for (DepartmentVacation departmentVacation:totalVacationGraph.getVacations()) {
            totalVacationGraphDto.vacations.add(createDepartmentVacationDto(departmentVacation));
        }
        return totalVacationGraphDto;
    }

    public ShiftDto createShiftDto(Shift shift) {
        ShiftDto shiftDto = new ShiftDto();
        shiftDto.date = shift.getDate();
        shiftDto.limitOfShift = shift.getLimitOfShift();
        shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
        return shiftDto;
    }


    public Vacation createVacation (VacationDto vacationDto) {
        Vacation vacation = new Vacation(vacationDto.startDay,vacationDto.endDay,dao.getPersonById(vacationDto.employee));
        return vacation;
    }

    public DepartmentVacation createDepartmentVacation(DepartmentVacationDto departmentVacationDto){
        List<Vacation> vacationList = new ArrayList<>();
        for (VacationDto vacationDto:departmentVacationDto.vacationList) {
            vacationList.add(createVacation(vacationDto));
        }
        DepartmentVacation departmentVacation = new DepartmentVacation(departmentVacationDto.departament,
                vacationList,departmentVacationDto.year,
                departmentVacationDto.approved,departmentVacationDto.idHREmpoloyee);

        return departmentVacation;
    }
    public PersonDto createPersonDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.firstName = person.getFirstName();
        personDto.lastName = person.getLastName();
        personDto.middleName = person.getMiddleName();
        personDto.birthday = person.getBirthday();
        personDto.idPerson = person.getIdPerson();
        personDto.INN = person.getINN();
        personDto.title = person.getTitle();
        personDto.shiftType = person.getShiftType();
        personDto.rentDate = person.getRentDate();
        personDto.fireDate = person.getFireDate();
        List<OrderDto> orders = new ArrayList<>();
        for (Order order : person.getOrders()) {
            OrderDto orderDto = new OrderDto();
            orderDto.date = order.getDate();
            orderDto.id = order.getId();
            orderDto.type = order.getType();
            orders.add(orderDto);
        }
        personDto.orders = orders;
        if (person.getShifts() != null) {
            List<ShiftDto> shifts = new ArrayList<>();
            for (Shift shift : person.getShifts()) {
                ShiftDto shiftDto = new ShiftDto();
                shiftDto.date = shift.getDate();
                shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
                shiftDto.limitOfShift = shift.getLimitOfShift();
                shifts.add(shiftDto);
            }
            personDto.shifts = shifts;
        } else {
            personDto.shifts = null;
        }
        return personDto;
    }


    public Person createPerson(PersonDto personDto) {
        List<Shift> shifts = new ArrayList<>();
        for (ShiftDto shiftDto:personDto.shifts) {
            shifts.add(createShift(shiftDto));
        }
        List<Order> orders = new ArrayList<>();
        for (OrderDto orderDto:personDto.orders) {
            orders.add(createOrder(orderDto));
        }
        Person person = new Person(personDto.firstName,personDto.lastName,personDto.middleName,
                personDto.birthday,personDto.idPerson,personDto.INN,personDto.title,
                personDto.rentDate,orders,personDto.shiftType,personDto.fireDate,shifts);
        return person;
    }

    public OrderDto createOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.date = order.getDate();
        orderDto.id = order.getId();
        orderDto.type = order.getType();
        return orderDto;
    }

    public Order createOrder(OrderDto orderDto){
        Order order = new Order(orderDto.id, orderDto.type, orderDto.date);
        return order;
    }

    public Shift createShift (ShiftDto shiftDto) {
        Shift shift = new Shift(shiftDto.date,shiftDto.limitOfShift,shiftDto.actualAmountEmployee);
        return shift;
    }
}
