package integration_tests;

import PersonnelDao.User;
import dao.Dao;
import dao.DaoStub;
import dto.*;
import exceptions.*;
import models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.Service;
import service.ServiceImpl;
import service.classes.Role;
import service.mock.PositionService;
import service.mock.WorkCalendarService;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShiftAPITest {

    private Service service;

    private User managerHRUuid = new User();
    private User secondHRManagerUuid = new User();
    private User chiefUuid = new User();
    private User userUuid = new User();
    private User adminUuid = new User();

    @BeforeEach
    public void init() throws InvalidCredentialsException, AccessDeniedException, InvalidDataException, InvalidSessionException, EntitySavingException {
        Dao dao = new DaoStub();
        this.service = new ServiceImpl(dao);

        String managerHRLogin = "firstHRManager";
        //  String secondHRManagerLogin = "secondHRManager";
        String chiefLogin = "chief";
        String userLogin = "user";
        String adminLogin = "admin";
        String password = "password";

        this.managerHRUuid.login = managerHRLogin;
        this.managerHRUuid.role = Role.HR;

        this.userUuid.login = userLogin;
        this.userUuid.role = Role.USER;

        this.chiefUuid.login = chiefLogin;
        this.chiefUuid.role = Role.CHIEF;

        this.adminUuid.login = adminLogin;
        this.adminUuid.role = Role.ADMIN;

        String firstName = "Bondarev";
        String lastName = "Alexander";
        String middleName = "Vladimirovich";
        LocalDate birthday = LocalDate.of(1999, 5, 6);
        String INN = "123456123451";
        Integer title = 1;
        LocalDate rentDate = LocalDate.of(2017, 10, 10);
        Order order = Order.createHireOrder(1, rentDate);
        OrderDto hireOrder1 = createOrderDto(order);
        ShiftType type = ShiftType.NORMAL;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN, title,
                rentDate, hireOrder1, type);

        firstName = "Bondarev1";
        lastName = "Alexander1";
        middleName = "Vladimirovich1";
        birthday = LocalDate.of(1999, 5, 6);
        String INN1 = "123456123456";
        title = 4;

        order = Order.createHireOrder(2, rentDate);
        OrderDto hireOrder2 = createOrderDto(order);
        type = ShiftType.IN_SHIFTS;
        this.service.rentPerson(managerHRUuid, firstName, lastName, middleName, birthday, INN1, title,
                rentDate, hireOrder2, type);

    }


    @Test
    public void testPositive_addShiftHRNormal() throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        Shift shift = new Shift(LocalDate.of(2020, 1, 15), 1);
        ShiftDto shiftDto = createShiftDto(shift);
        this.service.addHrShift(managerHRUuid, 0, shiftDto);
        assertNotNull(this.service.getAllPersonShift(managerHRUuid, 0));
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 0).get(0).date, shiftDto.date);
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 0).get(0).actualAmountEmployee,
                shiftDto.actualAmountEmployee + 1);
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 0).get(0).limitOfShift,
                shiftDto.limitOfShift);
    }

    @Test
    public void testPositive_addShiftChiefNormal() throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        Shift shift = new Shift(LocalDate.of(2020, 1, 15), 1);
        ShiftDto shiftDto = createShiftDto(shift);
        this.service.addChiefShift(chiefUuid, 0, shiftDto, 1);
        assertNotNull(this.service.getAllPersonShift(managerHRUuid, 0));
        assertEquals(this.service.getAllPersonShift(managerHRUuid,
                0).get(0).date, shiftDto.date);
    }

    @Test
    public void testPositive_addShiftChiefInShifts() throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        Shift shift1 = new Shift(LocalDate.of(2020, 1, 15), 1);
        Shift shift2 = new Shift(LocalDate.of(2020, 1, 16), 1);
        Shift shift3 = new Shift(LocalDate.of(2020, 1, 17), 1);
        Shift shift4 = new Shift(LocalDate.of(2020, 1, 21), 1);

        ShiftDto shiftDto1 = createShiftDto(shift1);
        ShiftDto shiftDto2 = createShiftDto(shift2);
        ShiftDto shiftDto3 = createShiftDto(shift3);
        ShiftDto shiftDto4 = createShiftDto(shift4);

        this.service.addChiefShift(chiefUuid, 1, shiftDto1, 2);
        this.service.addChiefShift(chiefUuid, 1, shiftDto2, 2);
        this.service.addChiefShift(chiefUuid, 1, shiftDto3, 2);
        this.service.addChiefShift(chiefUuid, 1, shiftDto4, 2);


        assertNotNull(this.service.getAllPersonShift(managerHRUuid, 1));
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 1).get(0).date, shift1.getDate());
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 1).get(3).date, shift4.getDate());
    }

    @Test
    public void testPositive_addShiftHRInShifts() throws AccessDeniedException, InvalidSessionException, WrongShiftException, EntitySavingException {
        Shift shift1 = new Shift(LocalDate.of(2020, 1, 15), 1);
        Shift shift2 = new Shift(LocalDate.of(2020, 1, 16), 1);
        Shift shift3 = new Shift(LocalDate.of(2020, 1, 17), 1);
        Shift shift4 = new Shift(LocalDate.of(2020, 1, 21), 1);

        ShiftDto shiftDto1 = createShiftDto(shift1);
        ShiftDto shiftDto2 = createShiftDto(shift2);
        ShiftDto shiftDto3 = createShiftDto(shift3);
        ShiftDto shiftDto4 = createShiftDto(shift4);

        this.service.addHrShift(managerHRUuid, 1, shiftDto1);
        this.service.addHrShift(managerHRUuid, 1, shiftDto2);
        this.service.addHrShift(managerHRUuid, 1, shiftDto3);
        this.service.addHrShift(managerHRUuid, 1, shiftDto4);
        assertNotNull(this.service.getAllPersonShift(managerHRUuid, 1));
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 1).get(0).date, shift1.getDate());
        assertEquals(this.service.getAllPersonShift(managerHRUuid, 1).get(3).date, shift4.getDate());
    }

    @Test
    public void testPositive_ErrorAddShiftHRNormalAtVacation() throws AccessDeniedException, InvalidSessionException, WrongShiftException {
        assertThrows( WrongShiftException.class , ()-> {
            Shift shift1 = new Shift(LocalDate.of(2020, 1, 15), 1);
            ShiftDto shiftDto = createShiftDto(shift1);
            Vacation vacation = new Vacation(LocalDate.of(2020, 1, 10),
                    LocalDate.of(2020, 1, 20),
                    createPerson(this.service.getPerson(managerHRUuid, "123456123451")));
            List<Vacation> depVac = new ArrayList<>();
            depVac.add(vacation);
            DepartmentVacation departmentVacation = new DepartmentVacation(
                    PositionService.findAll().get(this.service.getPerson(chiefUuid, "123456123451").title),
                    depVac, 2020);
            DepartmentVacationDto departmentVacationDto = createDepartmentVacationDto(departmentVacation);
            this.service.addDepartmentVacation(chiefUuid, departmentVacationDto);

            this.service.addHrShift(managerHRUuid,0,shiftDto);

        });
    }


    public VacationDto createVacationDto(Vacation vacation) {
        VacationDto vacationDto = new VacationDto();
        vacationDto.startDay = vacation.getStartDay();
        vacationDto.endDay = vacation.getEndDay();
        vacationDto.employee = vacation.getEmployee().getIdPerson();
        return vacationDto;
    }

    public ShiftDto createShiftDto(Shift shift) {
        ShiftDto shiftDto = new ShiftDto();
        shiftDto.date = shift.getDate();
        shiftDto.limitOfShift = shift.getLimitOfShift();
        shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
        return shiftDto;
    }

    public DepartmentVacationDto createDepartmentVacationDto(DepartmentVacation departmentVacation){
        DepartmentVacationDto departmentVacationDto = new DepartmentVacationDto();
        departmentVacationDto.departament = departmentVacation.getDepartament();
        departmentVacationDto.vacationList = new ArrayList<>();
        departmentVacationDto.year = departmentVacation.getYear();
        departmentVacationDto.approved = departmentVacation.isApproved();
        departmentVacationDto.idHREmpoloyee = departmentVacation.getIdHREmpoloyee();
        for (Vacation vacation:departmentVacation.getVacationList()) {
            departmentVacationDto.vacationList.add(createVacationDto(vacation));
        }
        return departmentVacationDto;
    }

    public PersonDto createPersonDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.firstName = person.getFirstName();
        personDto.lastName = person.getLastName();
        personDto.middleName = person.getMiddleName();
        personDto.birthday = person.getBirthday();
        personDto.idPerson = person.getIdPerson();
        personDto.INN = person.getINN();
        personDto.title = person.getTitle();
        personDto.shiftType = person.getShiftType();
        personDto.rentDate = person.getRentDate();
        personDto.fireDate = person.getFireDate();
        List<OrderDto> orders = new ArrayList<>();
        for (Order order : person.getOrders()) {
            OrderDto orderDto = new OrderDto();
            orderDto.date = order.getDate();
            orderDto.id = order.getId();
            orderDto.type = order.getType();
            orders.add(orderDto);
        }
        personDto.orders = orders;
        if (person.getShifts() != null) {
            List<ShiftDto> shifts = new ArrayList<>();
            for (Shift shift : person.getShifts()) {
                ShiftDto shiftDto = new ShiftDto();
                shiftDto.date = shift.getDate();
                shiftDto.actualAmountEmployee = shift.getActualAmountEmployee();
                shiftDto.limitOfShift = shift.getLimitOfShift();
                shifts.add(shiftDto);
            }
            personDto.shifts = shifts;
        } else {
            personDto.shifts = null;
        }
        return personDto;
    }


    public Person createPerson(PersonDto personDto) {
        List<Shift> shifts = new ArrayList<>();
        for (ShiftDto shiftDto:personDto.shifts) {
            shifts.add(createShift(shiftDto));
        }
        List<Order> orders = new ArrayList<>();
        for (OrderDto orderDto:personDto.orders) {
            orders.add(createOrder(orderDto));
        }
        Person person = new Person(personDto.firstName,personDto.lastName,personDto.middleName,
                personDto.birthday,personDto.idPerson,personDto.INN,personDto.title,
                personDto.rentDate,orders,personDto.shiftType,personDto.fireDate,shifts);
        return person;
    }

    public OrderDto createOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.date = order.getDate();
        orderDto.id = order.getId();
        orderDto.type = order.getType();
        return orderDto;
    }

    public Order createOrder(OrderDto orderDto){
        Order order = new Order(orderDto.id, orderDto.type, orderDto.date);
        return order;
    }

    public Shift createShift (ShiftDto shiftDto) {
        Shift shift = new Shift(shiftDto.date,shiftDto.limitOfShift,shiftDto.actualAmountEmployee);
        return shift;
    }

}
