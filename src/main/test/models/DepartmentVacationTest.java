package models;


import exceptions.InvalidSessionException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DepartmentVacationTest {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private LocalDate startDate1 = LocalDate.parse("01.10.2019",formatter);
    private LocalDate endDate1 = LocalDate.parse("21.10.2019",formatter);
    private LocalDate startDate2 = LocalDate.parse("01.05.2019",formatter);
    private LocalDate endDate2 = LocalDate.parse("09.05.2019",formatter);
    private LocalDate endDate21 = LocalDate.parse("29.05.2019",formatter);
    private LocalDate wrongStartDate = LocalDate.parse("27.12.2019",formatter);
    private LocalDate wrongEndDate = LocalDate.parse("01.01.2020",formatter);
    private Order order1 = Order.createHireOrder(1, startDate1);
    private String fam = "Iuanova";
    private String im = "Inna";
    private String otch = "Ivanovna";
    private Integer idPerson = 1;
    private String INN = "123456123456";
    private Person person = new Person(im, fam, otch, startDate1, 1,INN, 1,startDate1, order1, ShiftType.NORMAL);
    private  Person person2 = new Person(im, fam, otch, startDate1,2, INN, 1,startDate1, order1, ShiftType.NORMAL);
    private  Person person3 = new Person(im, fam, otch, startDate1,3, INN, 1,startDate1, order1, ShiftType.NORMAL);
    private Vacation vacation1 = new Vacation(startDate1,endDate1,person);
    private Vacation vacation12 = new Vacation(startDate1,endDate1,person2);
    private Vacation vacation13 = new Vacation(startDate1,endDate1,person3);
    private Vacation vacation2 = new Vacation(startDate2,endDate2,person);
    private Vacation vacation22 = new Vacation(startDate2,endDate2,person2);
    private Vacation vacation23 = new Vacation(startDate2,endDate2,person3);
    private Vacation vacation21 = new Vacation(startDate2,endDate21,person);
    private Vacation vacation222 = new Vacation(startDate1,startDate1.plusDays(30),person);
    private Vacation vacation223 = new Vacation(startDate2,startDate2.plusDays(30),person);

    private Vacation vacation2222 = new Vacation(startDate1,startDate1.plusDays(30),person2);
    private Vacation vacation2232 = new Vacation(startDate2,startDate2.plusDays(30),person2);
    private Vacation wrongVacation = new Vacation(wrongStartDate,wrongEndDate,person);
    private List<Vacation> vacationList = new ArrayList<Vacation>();
    private Integer iDept = 1;
    private Integer year = 2019;
    private List<Integer> idPersons = new ArrayList<Integer>();

    @Test
    public void TestPositive_DepartmentVacationConstructor() {
        vacationList.add(vacation1);
        vacationList.add(vacation2);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);

        assertEquals(departmentVacation.getDepartament(),iDept);
        assertEquals(departmentVacation.getYear(), year);
        for (int i = 0; i < departmentVacation.getVacationList().size(); i++) {
            assertEquals(departmentVacation.getVacationList().get(i), vacationList.get(i));
        }
        assertEquals(departmentVacation.isApproved(),false);
    }

    @Test
    public void TestNegative_DepartmentVacationConstructor() {
        assertThrows(IllegalArgumentException.class, () -> {
            vacationList.add(vacation1);
            vacationList.add(vacation2);
            vacationList.add(wrongVacation);
            DepartmentVacation departmentVacation = new DepartmentVacation(iDept, vacationList, year);
        });
    }

    @Test
    public void TestPositive_ApproveDeptVacation() {
        vacationList.add(vacation1);
        vacationList.add(vacation2);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        departmentVacation.approve(person.getIdPerson());

        assertEquals(departmentVacation.getIdHREmpoloyee(),idPerson);
        assertEquals(departmentVacation.isApproved(),true);
    }

    @Test
    public void TestPositive_checkVacationDaysWrightAmount() {
        vacationList.add(vacation1);
        vacationList.add(vacation2);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        assertEquals(departmentVacation.checkVacationDays(idPerson),true);
    }

    @Test
    public void TestPositive_checkVacationDaysWrongAmount() {
        vacationList.add(vacation222);
        vacationList.add(vacation223);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        assertEquals(departmentVacation.checkVacationDays(idPerson),false);
    }

    @Test
    public void TestPositive_checkDepartmentVacationDaysEnouphEmployee() {
        vacationList.add(vacation1);
        vacationList.add(vacation12);
        vacationList.add(vacation13);
        vacationList.add(vacation2);
        vacationList.add(vacation22);
        vacationList.add(vacation23);
        idPersons.add(1);
        idPersons.add(2);
        idPersons.add(3);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        assertEquals(departmentVacation.checkDepartmentVacationDays(idPersons),true);
    }

    @Test
    public void TestPositive_checkDepartmentVacationDaysNotEnouphEmployee() {
        vacationList.add(vacation1);
        vacationList.add(vacation12);
        vacationList.add(vacation2);
        vacationList.add(vacation22);
        idPersons.add(1);
        idPersons.add(2);
        idPersons.add(3);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        assertEquals(departmentVacation.checkDepartmentVacationDays(idPersons),false);
    }

    @Test
    public void TestPositive_checkDepartmentVacationDaysWrong() {
        vacationList.add(vacation222);
        vacationList.add(vacation2222);
        vacationList.add(vacation223);
        vacationList.add(vacation2232);
        idPersons.add(1);
        idPersons.add(2);
        DepartmentVacation departmentVacation = new DepartmentVacation(iDept,vacationList,year);
        assertEquals(departmentVacation.checkDepartmentVacationDays(idPersons),false);
    }
}
