package models;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VacationTest {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private LocalDate startDate = LocalDate.parse("01.10.2019",formatter);
    private LocalDate wrightEndDate = LocalDate.parse("21.10.2019",formatter);
    private LocalDate wrongEndDate = LocalDate.parse("30.12.2019",formatter);
    private Order order = Order.createHireOrder(1, startDate);
    private String fam = "Iuanova";
    private String im = "Inna";
    private String otch = "Ivanovna";
    private Integer idPerson = 1;
    private String INN = "123456123456";
    private  Person person = new Person(im, fam, otch, startDate,  INN, 12,startDate, order, ShiftType.NORMAL);


    @Test
    public void TestPositive_VacationConstructor() {
        Vacation vacation = new Vacation(startDate,wrightEndDate,person);
        assertEquals(vacation.getStartDay(),startDate);
        assertEquals(vacation.getEndDay(),wrightEndDate);
     //   Assert.assertEquals(vacation.getEmployee().getIdPerson(),idPerson);
    }

    @Test
    public void TestNegative_VacationConstructor() {

        assertThrows(IllegalArgumentException.class, () -> {
            Vacation vacation = new Vacation(startDate,wrongEndDate,person);
        });
    }

    @Test
    public void TestPositive_getAmountDays() {
        Vacation vacation = new Vacation(startDate,wrightEndDate,person);
        Long actual = 21L;
        assertEquals(vacation.getAmountDays(),actual);
    }
}
