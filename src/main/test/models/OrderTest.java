package models;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static models.Type.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderTest {

    private Integer id = 1;
    private LocalDate date = LocalDate.of(2019,11,12);
    private Double wage = 60000.00;
    private Integer title = 3;
    private LocalDate fireDate = LocalDate.of(2020,12,3);
    private LocalDate rentDate = LocalDate.of(2020,1,3);

    @Test
    public void TestPositive_OrderRentConstructor() {
        Order ord = Order.createHireOrder(id,date);

        assertTrue(ord.getId().equals(id) && ord.getDate().equals(date) &&
                            ord.getType()==RENT);
    }

    @Test
    public void TestPositive_OrderFireConstructor() {
        Order ord = Order.createFireOrder(id,date);

        assertTrue(ord.getId().equals(id) && ord.getDate().equals(date) &&
                ord.getType()==FIRE);
    }

    @Test
    public void TestPositive_OrderTransferConstructor() {
        Order ord = Order.createTransferOrder(id,date);

        assertTrue(ord.getId().equals(id) && ord.getDate().equals(date) &&
                ord.getType()==TRANSFER);
    }
}
