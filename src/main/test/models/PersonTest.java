package models;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static models.Order.createFireOrder;
import static models.Order.createHireOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersonTest {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private LocalDate startDate = LocalDate.parse("01.10.2019", formatter);
    private LocalDate rentDate = LocalDate.parse("02.10.2019", formatter);
    private LocalDate fireDate = LocalDate.parse("02.10.2020", formatter);
    private Order order = createHireOrder(1, startDate);
    private String fam = "Iuanova";
    private String im = "Inna";
    private String otch = "Ivanovna";
 //   private Integer idPerson = 1;
    private Integer title = 12;
    private String INN = "123456123456";
    private Double wage = 13000.00;
    private Order fireOrder = createFireOrder(2, startDate);


    @Test
    public void TestPositive_PersonConstructor() {
        Person person = new Person(im, fam, otch, startDate, INN, title,rentDate, order, ShiftType.NORMAL);
        assertEquals(person.getFirstName(), im);
        assertEquals(person.getLastName(), fam);
        assertEquals(person.getMiddleName(), otch);
        assertEquals(person.getTitle(), title);
        assertEquals(person.getINN(), INN);
       // Assert.assertEquals(person.getIdPerson(), idPerson);
        assertEquals(person.getOrders().get(0).getId(), order.getId());
        assertEquals(person.getRentDate(), rentDate);
        assertEquals(person.getBirthday(), startDate);
        assertEquals(person.getShiftType(), ShiftType.NORMAL);
    }

    @Test
    public void TestPositive_PersonFire() throws Exception {

            Person person = new Person(im, fam, otch, startDate, INN, title, rentDate, order, ShiftType.NORMAL);

            person.firePerson(fireOrder, fireDate);
            assertEquals(person.getFireDate(), fireDate);

    }

    @Test
    public void TestNegative_PersonFire() throws Exception {
        assertThrows(Exception.class, () -> {
                    Person person = new Person(im, fam, otch, startDate, INN, title, rentDate, order, ShiftType.NORMAL);
                    person.firePerson(order, fireDate);
                });
        // Assert.assertEquals(person.getFireDate(), fireDate);
    }

    @Test
    public void TestPositive_AddShiftNormal() throws Exception {
        Shift shift1 = new Shift(LocalDate.of(2019, 12, 10), 2);
        Shift shift2 = new Shift(LocalDate.of(2019, 12, 11), 1);
        Shift shift3 = new Shift(LocalDate.of(2019, 12, 12), 2);
        Shift shift4 = new Shift(LocalDate.of(2019, 12, 13), 2);
        Shift shift5 = new Shift(LocalDate.of(2019, 12, 14), 2);
        Shift shift6 = new Shift(LocalDate.of(2019, 12, 15), 2);
        Shift shift7 = new Shift(LocalDate.of(2019, 12, 16), 2);
        Person person = new Person(im, fam, otch, startDate,  INN, title,rentDate, order, ShiftType.NORMAL);
        Person person1 = new Person(im, fam, otch, startDate, INN, title,rentDate, order, ShiftType.NORMAL);
        assertEquals(person.addShift(shift1),true);
        assertEquals(person.getShifts().get(0), shift1);
        assertEquals(person.addShift(shift4),true);
        assertEquals(person.addShift(shift2),true);
        assertEquals(person1.addShift(shift2),false);
        assertEquals(person1.addShift(shift1),true);
    }

    @Test
    public void TestPositive_AddShiftInShifts() throws Exception {
        Shift shift1 = new Shift(LocalDate.of(2019, 12, 10), 1);
        Shift shift2 = new Shift(LocalDate.of(2019, 12, 11), 1);
        Shift shift3 = new Shift(LocalDate.of(2019, 12, 12), 1);
        Shift shift4 = new Shift(LocalDate.of(2019, 12, 13), 1);
        Shift shift5 = new Shift(LocalDate.of(2019, 12, 14), 1);
        Shift shift6 = new Shift(LocalDate.of(2019, 12, 15), 1);
        Shift shift7 = new Shift(LocalDate.of(2019, 12, 16), 1);
        Shift shift8 = new Shift(LocalDate.of(2019, 12, 17), 1);
        Shift shift9 = new Shift(LocalDate.of(2019, 12, 18), 1);
        Person person = new Person(im, fam, otch, startDate,  INN, title,rentDate, order, ShiftType.IN_SHIFTS);
        Person person1 = new Person(im, fam, otch, startDate,  INN, title,rentDate, order, ShiftType.IN_SHIFTS);
        assertEquals(person.addShift(shift1),true);
        assertEquals(person.addShift(shift2), true);
        assertEquals(person.addShift(shift3),true);
        assertEquals(person.addShift(shift4),false);
        assertEquals(person.addShift(shift5),false);
        assertEquals(person.addShift(shift6),false);
        assertEquals(person.addShift(shift7),true);
        assertEquals(person.addShift(shift8),true);
        assertEquals(person.addShift(shift9),true);
        assertEquals(person1.addShift(shift1),false);
        assertEquals(person1.addShift(shift2), false);
        assertEquals(person1.addShift(shift3),false);
        assertEquals(person1.addShift(shift4),true);
        assertEquals(person1.addShift(shift5),true);
        assertEquals(person1.addShift(shift6),true);
        assertEquals(person1.addShift(shift7),false);
        assertEquals(person1.addShift(shift8),false);
        assertEquals(person1.addShift(shift9),false);
        assertEquals(person.getShifts().get(0), shift1);
        assertEquals(person.getShifts().get(1), shift2);
        assertEquals(person.getShifts().get(2), shift3);
        assertEquals(person.getShifts().get(3), shift7);
        assertEquals(person.getShifts().get(1).isFull(),true);
    }
}