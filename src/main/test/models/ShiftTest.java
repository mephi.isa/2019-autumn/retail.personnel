package models;



import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ShiftTest {

    private LocalDate date = LocalDate.of(2020,1,3);
    private Integer limitShift = 5;

    @Test
    public void TestPositive_ShiftConstructor() {

        Shift shift = new Shift(date,limitShift);
        assertEquals(shift.getDate(),date);
        assertEquals(shift.getLimitOfShift(),limitShift);
        assertEquals(shift.isFull(),false);
    }

}
