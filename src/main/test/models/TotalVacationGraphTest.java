package models;



import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TotalVacationGraphTest {
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private LocalDate startDate1 = LocalDate.parse("01.10.2019",formatter);
    private LocalDate endDate1 = LocalDate.parse("21.10.2019",formatter);
    private LocalDate startDate2 = LocalDate.parse("01.05.2019",formatter);
    private LocalDate endDate2 = LocalDate.parse("09.05.2019",formatter);
    private LocalDate endDate21 = LocalDate.parse("29.05.2019",formatter);
    private LocalDate wrongStartDate = LocalDate.parse("27.12.2019",formatter);
    private LocalDate wrongEndDate = LocalDate.parse("01.01.2020",formatter);
    private Order order1 = Order.createFireOrder(1,startDate1);
    private String fam = "Iuanova";
    private String im = "Inna";
    private String otch = "Ivanovna";
    private Integer idPerson = 1;
    private String INN = "123456123456";
    private Person person = new Person(im, fam, otch, startDate1,  INN, 1,startDate1, order1, ShiftType.NORMAL);
    private  Person person2 = new Person(im, fam, otch, startDate1,  INN, 1,startDate1, order1, ShiftType.NORMAL);
    private  Person person3 = new Person(im, fam, otch, startDate1,  INN, 1,startDate1, order1, ShiftType.NORMAL);
    private Vacation vacation1 = new Vacation(startDate1,endDate1,person);
    private Vacation vacation12 = new Vacation(startDate1,endDate1,person2);
    private Vacation vacation13 = new Vacation(startDate1,endDate1,person3);
    private Vacation vacation2 = new Vacation(startDate2,endDate2,person);
    private Vacation vacation22 = new Vacation(startDate2,endDate2,person2);
    private Vacation vacation23 = new Vacation(startDate2,endDate2,person3);
    private List<Vacation> vacationList = new ArrayList<Vacation>();
    private Integer iDept = 1;
    private Integer iDept2 = 2;
    private Integer year = 2019;
    private List<DepartmentVacation> vacations = new ArrayList<DepartmentVacation>();

    @Test
    public void TestPositive_DepartmentVacationConstructor() {
        vacationList.add(vacation1);
        vacationList.add(vacation12);
        vacationList.add(vacation2);
        vacationList.add(vacation22);
        DepartmentVacation departmentVacation1 = new DepartmentVacation(iDept,vacationList,year);
        departmentVacation1.approve(1);
        vacationList.add(vacation23);
        vacationList.add(vacation13);
        DepartmentVacation departmentVacation2 = new DepartmentVacation(iDept2,vacationList,year);
        departmentVacation2.approve(1);
        vacations.add(departmentVacation1);
        vacations.add(departmentVacation2);
        TotalVacationGraph totalVacationGraph = new TotalVacationGraph(year,vacations);
        assertEquals(totalVacationGraph.getYear(),year);
        assertEquals(totalVacationGraph.getVacations(),vacations);
    }

    @Test
    public void TestPositive_DepartmentVacationConstructorWrong() {
        assertThrows(IllegalArgumentException.class, () -> {
            vacationList.add(vacation1);
            vacationList.add(vacation12);
            vacationList.add(vacation2);
            vacationList.add(vacation22);
            DepartmentVacation departmentVacation1 = new DepartmentVacation(iDept, vacationList, year);
            vacationList.add(vacation23);
            vacationList.add(vacation13);
            DepartmentVacation departmentVacation2 = new DepartmentVacation(iDept2, vacationList, year);
            departmentVacation2.approve(1);
            vacations.add(departmentVacation1);
            vacations.add(departmentVacation2);
            TotalVacationGraph totalVacationGraph = new TotalVacationGraph(year, vacations);
        });
    }
}
