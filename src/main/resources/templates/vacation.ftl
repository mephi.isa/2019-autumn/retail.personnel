<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>personal</title>
</head>
<body>

<#if user??> <#if user == "ADMIN">
    <form name="searchForm" action="/createdepvac" method="GET">
        <input type="text" name="year" placeholder="Год"/>
        <input type="text"  name="idDept" placeholder="Департамент"/>
        <input type="submit" value="Создать/просмотреть график отпусков департамента" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if user??> <#if user == "CHIEF" >
    <form name="searchForm" action="/createdepvac" method="GET">
        <input type="text" name="year" placeholder="Год"/>
        <input type="hidden"  name="idDept" value="${idDept}"/>
        <input type="submit" value="Создать/просмотреть график отпусков департамента" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if user??> <#if user == "HR">
    <form name="searchForm" action="/createdepvac" method="GET">
        <input type="text" name="year" placeholder="Год">
        <input type="text" name="idDept" placeholder="Департамент">
        <input type="submit" value="Просмотреть график отпусков департамента" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if user??><#if user == "HR" || user == "ADMIN">
    <form name="searchForm" action="/createtotalgraph" method="GET">
        <input type="text" name="year" placeholder="Год"/>
        <input type="submit" value="Создать итоговый график отпусков за год" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<form name="searchForm" action="/gettotalgraph" method="GET">
    <input type="text" name="year" placeholder="Год"/>
    <input type="submit" value="Посмотреть итоговый график отпусков за год" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<#if user??> <#if user == "HR">
    <form name="searchForm" action="/hr" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "CHIEF">
    <form name="searchForm" action="/chief" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "USER">
    <form name="searchForm" action="/user" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if user??> <#if user == "ADMIN">
    <form name="searchForm" action="/admin" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
