<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>hr</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>Личный кабинет сотрудника ${userID} </h1>

<form name="searchForm" action="/person" method="GET">
    <input type="submit" value="Просмотр списка сотрудников" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/vacation" method="GET">
    <input type="submit" value="Просмотр графа отпусков на год" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/logout" method="GET">
    <input type="submit" value="Выход" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>


</body>
</html>