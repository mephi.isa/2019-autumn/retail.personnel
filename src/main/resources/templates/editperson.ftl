<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>personal</title>
</head>
<body>


<#if persons??>
    <div> Информация о работнике: </div>
    <form>
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>День рождения</th><th>ИНН</th><th>Должность</th><th>График работы</th><th>Дата наема</th><th>Дата увольнения</th>
            </tr>
            <#list persons as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>

                </tr>
            </#list>
        </table>
    </form>
</#if>


<#if user??> <#if user == "ADMIN" || user == "HR">

    <form name="searchForm" action="/editfirstname" method="GET" >
        <input type="hidden"  name="idPerson" value="${idPerson}"/>
        <input type="text" name="firstName" placeholder=" Имя">
        <input type="submit" value="Изменить имя" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

    <form name="searchForm" action="/editlastname" method="GET">
        <input type="hidden"  name="idPerson" value="${idPerson}"/>
            <input type="text" name="lastName" placeholder="Фамилия">
            <input type="submit" value="Изменить фамилию" />
            <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

<form name="searchForm" action="/editmiddlename" method="GET">
    <input type="hidden"  name="idPerson" value="${idPerson}"/>
       <input type="text" name="middleName" placeholder="Отчество">
        <input type="submit" value="Изменить отчество" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/edititle" method="GET">
    <input type="hidden"  name="idPerson" value="${idPerson}"/>
    <input type="text" name="title" placeholder="Должность">
    <input type="text" name="orderid" placeholder="Номер приказа">
    <input type="text" name="o_day" placeholder="День приказа">
    <input type="text" name="o_month" placeholder="Месяц приказа">
    <input type="text" name="o_year" placeholder="Год приказа">
    <input type="submit" value="Изменить должность" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/editshifttype" method="GET">
    <input type="hidden"  name="idPerson" value="${idPerson}"/>
    <input type="text" name="shifttype" placeholder="График работы (NORMAL/INSHIFTS)">
    <input type="text" name="orderid" placeholder="Номер приказа">
    <input type="text" name="o_day" placeholder="День приказа">
    <input type="text" name="o_month" placeholder="Месяц приказа">
    <input type="text" name="o_year" placeholder="Год приказа">
    <input type="submit" value="Изменить тип графика работы" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/fireperson" method="GET">
    <input type="hidden"  name="idPerson" value="${idPerson}"/>
    <input type="text" name="r_day" placeholder="День увольнения">
    <input type="text" name="r_month" placeholder="Месяц увольнения">
    <input type="text" name="r_year" placeholder="Год увольнения">
    <input type="text" name="orderid" placeholder="Номер приказа">
    <input type="text" name="o_day" placeholder="День приказа">
    <input type="text" name="o_month" placeholder="Месяц приказа">
    <input type="text" name="o_year" placeholder="Год приказа">
    <input type="submit" value="Уволить сотрудника" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

    <form name="searchForm" action="/person" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>


<#if exception??><div>${exception}</div> </#if>

</body>
</html>
