<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>personal</title>
</head>
<body>

<#if user??> <#if user == "ADMIN" || user == "CHIEF">

    <form name="searchForm" action="/addingvacation" method="GET">
        <input type="hidden"  name="idPerson" value="${idPerson}"/>
        <input type="text" name="s_day" placeholder="День начала отпуска">
        <input type="text" name="s_month" placeholder="Месяц начала отпуска">
        <input type="text" name="e_day" placeholder="День конца отпуска">
        <input type="text" name="e_month" placeholder="Месяц конца отпуска">
        <input type="submit" value="Добавить отпуск" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

</#if></#if>

<#if vacations??>
    <div>  Список отпусков: </div>
    <form>
        <table id="table vacations" border="1px" cellspacing="1">
            <tr>
                <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>День начала отпуска</th><th>День окончания</th>
            </tr>
            <#list vacations as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                </tr>
            </#list>
        </table>
    </form>
</#if>

<form name="searchForm" action="/createdepvac" method="GET">
    <input type="hidden"  name="year" value="${year}"/>
    <input type="hidden"  name="idDept" value="${idDept}"/>
    <input type="submit" value="Вернуться назад" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>


<#if exception??><div>${exception}</div> </#if>

</body>
</html>
