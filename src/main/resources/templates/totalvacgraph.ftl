<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>hr</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>График отпусков за ${year} </h1>


<#if totalgraph??>
    <div> График отпусков за года: </div>
    <form>
        <table id="table vacation graph" border="1px" cellspacing="1">
            <tr>
                <th>#</th><th>Год</th><th>Департамент</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>ID сотрудника</th><th>Начало отпуска</th><th>Конец отпуска</th>
            </tr>
            <#list totalgraph as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>

                </tr>
            </#list>
        </table>
    </form>
</#if>

<form name="searchForm" action="/vacation" method="GET">
    <input type="submit" value="Назад" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>


</body>
</html>