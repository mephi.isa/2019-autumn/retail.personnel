<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>createdepvac</title>
</head>
<body>

<#if user??> <#if user == "ADMIN"  || user == "CHIEF" || user == "HR">

    <#if persons??>
        <div> Персонал: </div>
        <form>
            <table id="table product" border="1px" cellspacing="1">
                <tr>
                    <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Должность</th><th>Тип смен</th>
                </tr>
                <#list persons as row>
                    <tr>
                        <td>${row?index + 1}</td>
                        <#list row as field>
                            <td>${field}</td>
                        </#list>
                        <td><button formaction="/addshift" method="GET" value="${persons[row?index][0]}"
                                    name="idPerson" type="submit"
                                    style="width: 100%; height: 100%">Редактировать смены</button>
                    </tr>
                </#list>
            </table>
        </form>
    </#if>
</#if></#if>


<#if user??> <#if user == "HR">
    <form name="searchForm" action="/hr" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "CHIEF">
    <form name="searchForm" action="/chief" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "USER">
    <form name="searchForm" action="/user" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if user??> <#if user == "ADMIN">
    <form name="searchForm" action="/admin" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
