<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>personal</title>
</head>
<body>

<#if user??> <#if user == "ADMIN" || user == "HR">

<form name="searchForm" action="/addperson" method="GET">
    <input type="text" name="firstName" placeholder="Имя">
    <input type="text" name="secondName" placeholder="Фамилия">
    <input type="text" name="middleName" placeholder="Отчество">
    <input type="text" name="INN" placeholder="ИНН">
    <input type="text" name="b_day" placeholder="День рождения">
    <input type="text" name="b_month" placeholder="Месяц рождения">
    <input type="text" name="b_year" placeholder="Год рождения">
    <input type="text" name="title" placeholder="Должность">
    <input type="text" name="r_day" placeholder="День наема">
    <input type="text" name="r_month" placeholder="Месяц наема">
    <input type="text" name="r_year" placeholder="Год наема">
    <input type="text" name="orderid" placeholder="Номер приказа">
    <input type="text" name="o_day" placeholder="День приказа">
    <input type="text" name="o_month" placeholder="Месяц приказа">
    <input type="text" name="o_year" placeholder="Год приказа">
    <input type="text" name="shifttype" placeholder="График работы (NORMAL/INSHIFTS)">
    <input type="submit" value="Нанять работника" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

</#if></#if>


<#if user??> <#if user == "ADMIN" || user == "HR">

<#if persons??>
<div> Персонал: </div>
<form>
    <table id="table product" border="1px" cellspacing="1">
        <tr>
            <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>День рождения</th><th>ИНН</th><th>Должность</th><th>График работы</th><th>Дата наема</th><th>Дата увольнения</th>
        </tr>
        <#list persons as row>
            <tr>
                <td>${row?index + 1}</td>
                <#list row as field>
                    <td>${field}</td>
                </#list>
                <#if user??> <#if user == "ADMIN" || user == "HR">
                <td><button formaction="/editperson" method="GET" value="${persons[row?index][0]}"
                            name="idPerson" type="submit"
                            style="width: 100%; height: 100%">Редактировать</button>
                    </#if></#if>
            </tr>
        </#list>
    </table>
</form>
</#if>
</#if></#if>



<#if user??> <#if user == "CHIEF">

    <#if subpersons??>
        <div> Данные о подчиненных: </div>
        <form>
            <table id="table product" border="1px" cellspacing="1">
                <tr>
                    <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Должность</th><th>График работы</th>
                </tr>
                <#list subpersons as row>
                    <tr>
                        <td>${row?index + 1}</td>
                        <#list row as field>
                            <td>${field}</td>
                        </#list>

                    </tr>
                </#list>
            </table>
        </form>
    </#if>
</#if></#if>

<#if user??> <#if user == "CHIEF" || user == "USER">
<#if shortpersons??>
    <div> Персонал: </div>
    <form>
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Должность</th>
            </tr>
            <#list shortpersons as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td> ${field} </td>
                    </#list>
                </tr>
            </#list>
        </table>
    </form>
</#if>
</#if></#if>


<#if user??> <#if user == "HR">
    <form name="searchForm" action="/hr" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "CHIEF">
    <form name="searchForm" action="/chief" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "USER">
    <form name="searchForm" action="/user" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>
</#if></#if>
<#if user??> <#if user == "ADMIN">
<form name="searchForm" action="/admin" method="GET">
    <input type="submit" value="Вернуться назад" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>
</#if></#if>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
