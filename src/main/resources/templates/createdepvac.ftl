<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>createdepvac</title>
</head>
<body>

<#if user??> <#if user == "ADMIN"  || user == "CHIEF">

    <#if persons??>
        <div> Персонал: </div>
        <form>
            <table id="table product" border="1px" cellspacing="1">
                <tr>
                    <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Должность</th>
                </tr>
                <#list persons as row>
                    <tr>
                        <td>${row?index + 1}</td>
                        <#list row as field>
                            <td>${field}</td>
                        </#list>
                        <#if depvacex??>
                            <#if isapproved == true>

                            <#else>

                            </#if>
                        <#else>
                        <td><button formaction="/addvacation" method="GET" value="${persons[row?index][0]}"
                                    name="idPerson" type="submit"
                                    style="width: 100%; height: 100%">Добавить отпуск</button>
                        </#if>


                    </tr>
                </#list>
            </table>
        </form>
    </#if>
</#if></#if>

<#if user??> <#if user == "ADMIN" || user == "CHIEF" || user == "HR">

    <#if personsvac??>
        <div> Список отпусков департамента: </div>
        <form>
            <table id="table product" border="1px" cellspacing="1">
                <tr>
                    <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Дата начала отпуска</th><th>Дата конца отпуска</th>
                </tr>
                <#list personsvac as row>
                    <tr>
                        <td>${row?index + 1}</td>
                        <#list row as field>
                            <td>${field}</td>
                        </#list>
                    </tr>
                </#list>
            </table>
        </form>
    </#if>
</#if></#if>


<#if depvacex??>
    <#if user??><#if user == "HR" || user == "ADMIN" >
    <#if isapproved == true>
        <div>График утвержден.</div>
    <#else>
        <div> График отпусков департамента создан успешно. </div>
        <form name="searchForm" action="/approve" method="GET">
            <input type="hidden"  name="year" value="${year}"/>
            <input type="hidden"  name="idDept" value="${idDept}"/>
            <input type="submit" value="Утвердить" />
            <#if add_product_status??><div>${add_product_status}</div></#if>
        </form>
    </#if>
    </#if></#if>
<#else>
    <#if user??><#if user == "CHIEF" || user == "ADMIN" >
<form name="searchForm" action="/adddepvac" method="GET">
    <input type="hidden"  name="year" value="${year}"/>
    <input type="hidden"  name="idDept" value="${idDept}"/>
    <input type="submit" value="Создать график департамента" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>
    </#if></#if>
</#if>


<form name="searchForm" action="/vacation" method="GET">
        <input type="submit" value="Вернуться назад" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
