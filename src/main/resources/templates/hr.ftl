<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>hr</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>Личный кабинет сотрудника отела кадров ${hrID} </h1>

<form name="searchForm" action="/person" method="GET">
    <input type="submit" value="Работа с персоналом" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/vacation" method="GET">
    <input type="submit" value="Утверждение графиков отпусков департаментов" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/shifts" method="GET">
    <input type="submit" value="Работа со сменами" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/logout" method="GET">
    <input type="submit" value="Выход" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>



</body>
</html>