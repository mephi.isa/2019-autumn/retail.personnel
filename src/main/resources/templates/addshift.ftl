<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>personal</title>
</head>
<body>

<#if user??> <#if user == "ADMIN" || user == "CHIEF" || user == "HR">

    <form name="searchForm" action="/addingshift" method="GET">
        <input type="hidden"  name="idPerson" value="${idPerson}"/>
        <input type="text" name="day" placeholder="День смены">
        <input type="text" name="month" placeholder="Месяц смены">
        <input type="text" name="year" placeholder="Год смены">
        <input type="submit" value="Добавить смену" />
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

</#if></#if>

<#if user??> <#if user == "ADMIN" || user == "CHIEF" || user == "HR">
<#if shifts??>
    <div>  Список смен: </div>
    <form>
        <table id="table vacations" border="1px" cellspacing="1">
            <tr>
                <th>#</th><th>ID Сотрудника</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Смена</th>
            </tr>
            <#list shifts as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                </tr>
            </#list>
        </table>
    </form>
</#if>
</#if></#if>

<form name="searchForm" action="/shifts" method="GET">
    <input type="submit" value="Вернуться назад" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
